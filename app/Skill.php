<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    //
    public $table = 'skill';
    protected $fillable = [
        'JenisSkill',
    ];

    public function pesertas()
    {
        return $this->belongsToMany(Peserta::class, 'skill_peserta');
    }
}
