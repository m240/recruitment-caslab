<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pilihan extends Model
{
    protected $table = 'pilihan';

    protected $fillable = [
        'isi', 'status', 'pertanyaan_id'
    ];

    public function pertanyaan()
    {
        return $this->belongsTo('App\Pertanyaan');
    }
}
