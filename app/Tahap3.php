<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahap3 extends Model
{
    //
    protected $table = "tahap_3";
    protected $fillable = [
        "wawancara_2", "magang",
        "project", "oprec_id", "score_total_wawancara_2",
        "score_total_magang", "score_total_project",
        "peserta_id", "score_total_tahap_3"
    ];
    public function peserta()
    {
        return $this->belongsTo('App\Peserta', 'peserta_id');
    }
}
