<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	protected $table = 'role';
    protected $fillable = ['hak_akses'];
    
    public function admin()
    {
        return $this->hasMany('App\Admin');
    }
}
