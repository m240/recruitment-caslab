<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oprec extends Model
{
    protected $table = 'oprec';
    protected $fillable = [
        'nama_oprec', 'status_dibuka_oprec',
        'status_pendaftaran', 'status_tahap1', 'status_tahap2', 'status_tahap3'
    ];

    public function pesertas()
    {
        return $this->belongsToMany('Peserta', 'status', 'id_sesi', 'id_peserta')
            ->withTimeStamps();
    }

    public function microteaching()
    {
        return $this->hasMany('App\Microteaching');
    }
    public function totalTahap2()
    {
        return $this->hasMany('App\TotalTahap2');
    }
    public function wawancara1()
    {
        return $this->hasMany('App\Wawancara1');
    }

    // Eloquent Many To One
    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }

    // One To Many Eloquen Tahap 3

    public function tahap1()
    {
        return $this->hasMany('App\Tahap1');
    }

    public function tahap3()
    {
        return $this->hasMany('App\Tahap3');
    }

    public function status()
    {
        return $this->hasMany('App\Status');
    }

    public function magang()
    {
        return $this->hasMany('App\Magang');
    }

    public function project()
    {
        return $this->hasMany('App\Project');
    }

    public function wawancara2()
    {
        return $this->hasMany('App\Wawancara2');
    }

    public function persemtaseTahap3()
    {
        return $this->hasMany('App\PersemtaseTahap3');
    }
}
