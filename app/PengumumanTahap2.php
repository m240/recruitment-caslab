<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengumumanTahap2 extends Model
{
    protected $table = "pengumumantahap2";
    protected $fillable = [
        "oprec_id",
        "namapengumumantahap2",
    ];


    public function oprec()
    {
        return $this->belongsTo('App\Oprec', 'oprec_id');
    }
}
