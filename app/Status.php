<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    //
    public $timestamps = false;
    protected $table = "status";
    protected $fillable = [
        "status_pendaftaran",
        "status_tahap1",
        "status_tahap2",
        "status_tahap3",
        "oprec_id",
        "peserta_id"
    ];

    public function peserta()
    {
        return $this->belongsTo('App\Peserta');
    }
    public function oprec()
    {
        return $this->belongsTo('App\Oprec');
    }
}
