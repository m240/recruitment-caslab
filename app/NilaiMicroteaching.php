<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiMicroteaching extends Model
{
    //
    protected $table = "nilai_microteaching";
    protected $fillable = [
        "microteaching_id", "nilai", "peserta_id"
    ];

    public function kriteriaMicroteaching()
    {
        return $this->belongsTo('App\Microteaching', 'microteaching_id');
    }

    public function peserta()
    {
        return $this->belongsTo('App\Peserta', 'peserta_id');
    }
    public $timestamps = false;
}
