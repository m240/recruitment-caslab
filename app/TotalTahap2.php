<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalTahap2 extends Model
{
    //
    protected $table = "kriteria_total_tahap_2";
    protected $fillable = [
        "prosentase_wawancara_1", "prosentase_microteaching", "oprec_id"
    ];
    public function oprec()
    {
        return $this->belongsTo('App\Oprec', 'oprec_id');
    }
    public $timestamps = false;
}
