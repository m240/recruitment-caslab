<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahap1 extends Model
{
    protected $table = 'tahap_1';
    protected $fillable = [
        "score_total"
    ];

    public function oprec()
    {
        return $this->belongsTo('App\Oprec');
    }

    public function peserta()
    {
        return $this->belongsTo('App\Peserta');
    }

    public function jawaban()
    {
        return $this->hasMany('App\Jawaban', 'tahap_1_id', 'id');
    }
}
