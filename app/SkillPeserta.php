<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkillPeserta extends Model
{
    //
    public $timestamps = false;
    protected $primaryKey = 'peserta_id';
    public $incrementing = false;

    protected $fillable = ['peserta_id','skill_id'];
    public $table = 'skill_peserta';
}
