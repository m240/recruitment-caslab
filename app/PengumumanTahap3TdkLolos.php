<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengumumanTahap3TdkLolos extends Model
{
    protected $table = "pengumuman_tahap3_tdk_lolos";
    protected $fillable = [
        "oprec_id",
        "namapengumumantahap3tdklolos",
    ];


    public function oprec()
    {
        return $this->belongsTo('App\Oprec', 'oprec_id');
    }
}
