<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiWawancara1 extends Model
{
    //
    protected $table = "nilai_wawancara_1";
    protected $fillable = [
        "wawancara1_id", "nilai", "peserta_id"
    ];

    public function kriteriaWawancara1()
    {
        return $this->belongsTo('App\Wawancara1', 'wawancara1_id');
    }

    public function peserta()
    {
        return $this->belongsTo('App\Peserta', 'peserta_id');
    }
    public $timestamps = false;
}
