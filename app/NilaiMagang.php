<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiMagang extends Model

{
    //
    protected $table = "nilai_magang";
    protected $fillable = [
        "magang_id", "nilai", "peserta_id"
    ];

    public function kriteriaMagang()
    {
        return $this->belongsTo('App\Magang', 'magang_id');
    }

    public function peserta()
    {
        return $this->belongsTo('App\Peserta', 'peserta_id');
    }
    public $timestamps = false;
}
