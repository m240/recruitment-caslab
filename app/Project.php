<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $table = "kriteria_project";
    protected $fillable = [
        "nama_kriteria", "prosentase", "oprec_id"
    ];
    public function oprec()
    {
        return $this->belongsTo('App\Oprec', 'oprec_id');
    }
    public function nilaiProject()
    {
        return $this->hasMany('App\NilaiProject');
    }
    public $timestamps = false;
}
