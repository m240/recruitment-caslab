<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wawancara1 extends Model
{
    //
    protected $table = "kriteria_wawancara_1";
    protected $fillable = [
        "nama_kriteria", "prosentase", "oprec_id"
    ];
    public function oprec()
    {
        return $this->belongsTo('App\Oprec', 'oprec_id');
    }
    public function nilaiWawancara1()
    {
        return $this->hasMany('App\NilaiWawancara1');
    }
    public $timestamps = false;
}
