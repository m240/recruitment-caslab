<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $fillable = [
        'soal', 'code_snippet', 'score', 'kategoris_id'
    ];

    public function pilihan()
    {
        return $this->hasMany('App\Pilihan');
    }

    public function category()
    {
        return $this->belongsTo('App\Kategori', 'kategoris_id');
    }

    public function jawaban()
    {
        return $this->hasMany('App\Jawaban', 'pertanyaan_id');
    }
}
