<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Microteaching extends Model
{
    //
    protected $table = "kriteria_microteaching";
    protected $fillable = [
        "nama_kriteria", "prosentase", "oprec_id"
    ];
    public function oprec()
    {
        return $this->belongsTo('App\Oprec', 'oprec_id');
    }
    public function nilaiMicroteaching()
    {
        return $this->hasMany('App\NilaiMicroteaching');
    }
    public $timestamps = false;
}
