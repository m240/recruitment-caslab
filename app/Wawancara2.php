<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wawancara2 extends Model
{
    //
    protected $table = "kriteria_wawancara_2";
    protected $fillable = [
        "nama_kriteria", "prosentase", "oprec_id"
    ];
    public function oprec()
    {
        return $this->belongsTo('App\Oprec', 'oprec_id');
    }
    public function nilaiWawancara2()
    {
        return $this->hasMany('App\NilaiWawancara2');
    }
    public $timestamps = false;
}
