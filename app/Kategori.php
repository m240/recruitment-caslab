<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $fillable = ['judul', 'time', 'persentase'];

    public function questions()
    {
        return $this->hasMany('App\Pertanyaan', 'kategoris_id');
    }
}
