<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sesi extends Model
{
    protected $table = 'sesi_oprec';
    protected $fillable = ['nama_oprec', 'status_dibuka_oprec',];

    public function pesertas()
    {
        return $this->belongsToMany('Peserta','intersection','id_sesi','id_peserta')
        ->withTimeStamps();
    }

    public function tahap1()
    {
        return $this->hasMany('App\Tahap1', 'sesi_oprec_id', 'id');
    }

}
