<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiWawancara2 extends Model
{
    //
    protected $table = "nilai_wawancara_2";
    protected $fillable = [
        "wawancara2_id", "nilai", "peserta_id"
    ];

    public function kriteriaWawancara2()
    {
        return $this->belongsTo('App\Wawancara2', 'wawancara2_id');
    }

    public function peserta()
    {
        return $this->belongsTo('App\Peserta', 'peserta_id');
    }
    public $timestamps = false;
}

