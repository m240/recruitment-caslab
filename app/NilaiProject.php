<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiProject extends Model
{
    //
    protected $table = "nilai_project";
    protected $fillable = [
        "project_id", "nilai", "peserta_id"
    ];

    public function kriteriaProject()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }

    public function peserta()
    {
        return $this->belongsTo('App\Peserta', 'peserta_id');
    }
    public $timestamps = false;
}

