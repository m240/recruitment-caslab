<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Response;
use App\Peserta;
use App\Skill;

class KelolaDataController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'phone' => 'required',
            'email' => 'required|email',
            'motivasi' => 'required|max:500',
            'ide_kreatif' => 'required|max:500',
        ]);
    }

    protected function validatorfile(array $data)
    {
        return Validator::make($data, [
            'lampiran' => 'required|mimes:pdf|max:1024',
        ]);
    }

    public function index()
    {
      $skills = Skill::all();
      $peserta = Session::get('users');
      $alreadyuser = Peserta::where('nim',$peserta->user_name)->first();
      $user = Peserta::where('nim', $peserta->user_name)->first();
      if (!$user) {
        return redirect('peserta/daftar')->with('warning', 'Oops: Silahkan registrasi terlebih dahulu !');
    }
    return view('frontend.kelola-data', compact('user','alreadyuser','skills'))->with('status', 'open');
}

public function getLampiran(Request $request)
{
    $file = public_path() . '/' . $request->input('lampiran');
    $headers = array('Content-Type: application/pdf');
    return Response::download($file, 'lampiran.pdf', $headers);
}

public function updateInformasi(Request $request)
{
    $this->validator($request->all())->validate();
    $update = Peserta::where('nim', $request->nim)->first();
    $update->phone = $request->phone;
    $update->email = $request->email;
    $update->motivasi = $request->motivasi;
    $update->ide_kreatif = $request->ide_kreatif;
    if ($request->has('lampiran')) {
        $this->updateLampiran($request);
    }
    $update->save();
    return redirect('peserta/kelola-data')->with('success', 'Sukses memperbarui data diri !');
}

public function updateLampiran(Request $request)
{
    $this->validatorfile($request->all())->validate();
    $update = Peserta::where('nim', $request->nim)->first();
    $lampiran = $request->file('lampiran');
    $name_lampiran = $request->input('nim') . "-lampiran." . $lampiran->getClientOriginalExtension();
    $path_lampiran = 'files/lampiran/' . $name_lampiran;
    $lampiran->move((public_path() . "/files/lampiran/"), $name_lampiran);
    $update->lampiran = $path_lampiran;
    $update->save();
}

public function deleteRecord(Request $request)
{
    $destroy = Peserta::where('id', $request->nim)->first();
    unlink($destroy->lampiran);
    $destroy->delete();
    session()->forget('daftar');
    return redirect('peserta/dashboard')->with('success', 'Berhasil membatalkan keikutsertaan !');
}

public function update(Request $request)
{   
    $skillId = []; 
    for($x = 1; $x<=Skill::all()->count() ;$x++){
        $getskill = "skill_".$x;
        if($request->$getskill=="on"){
            array_push($skillId, $x);
        }
    }
    $peserta = Peserta::find($request->id); 
    $peserta->skills()->sync($skillId);
    return redirect('peserta/daftar')->with('success', 'Success updated peserta !');
}
}