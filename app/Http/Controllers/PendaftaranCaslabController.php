<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Peserta;
use App\Oprec;
use App\Status;
use Session;
use App\Skill;
use Response;

class PendaftaranCaslabController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:web')->except('logout');
    }

    public function index()
    {
        $sesi = Oprec::where('status_dibuka_oprec','dibuka')->get();
        
        $peserta = Session::get('users');
        $sudahDaftarSesiSekarang = null;

        if(count($sesi) == 1){
            $skills = Skill::all();
            $alreadyuser = Peserta::where('nim',$peserta->user_name)->get();

            if($alreadyuser){
                $i = 0;
                foreach ($alreadyuser as $ulang) {
                    $sudahDaftarSesiSekarang = Status::where('oprec_id',$sesi[0]->id)->where('peserta_id',$ulang->id)->first();
                    if($sudahDaftarSesiSekarang != null || count($alreadyuser) == 1 ){
                        break;
                    }
                    $i++;
                }
                if($sudahDaftarSesiSekarang){
                    $alreadyuser = $alreadyuser[$i];
                    return view('frontend.kelola-data',compact('skills','alreadyuser'))->with('warning', 'Oops: Anda sudah mengisi data registrasi, jika memerlukan update silahkan menuju dashboard Kelola Data !')->with('status','open');
                }
            }
            return view('frontend.register-peserta', compact('peserta'))->with('status', 'open');
        } //kalau lebih dari 1 sesi yang dibuka ya tidak boleh
        else if (count($sesi) > 1) {
            return view('frontend.register-peserta',  compact('peserta'))->with('status', 'adminError');
        }
         else {
            return view('frontend.register-peserta',  compact('peserta'))->with('status', 'closed');
        }

    //  $skills = Skill::all();
    //  $peserta = Session::get('users');
    //  $alreadyuser = Peserta::where('nim',$peserta->user_name)->first();
    //  $user = Peserta::where('nim',$peserta->user_name)->first();

    //  if($alreadyuser){
    //     return view('frontend.kelola-data',compact('user','skills','alreadyuser'))->with('warning', 'Oops: Anda sudah mengisi data registrasi, jika memerlukan update silahkan menuju dashboard Kelola Data !')->with('status','open');
    // }
    

    // try {
    //     $sesi = $sesi->status_pendaftaran;
    // } catch (\Throwable $th) {

    // }

}

protected function validator(array $data)
{
    return Validator::make($data, [
        'nama' => 'required',
        // 'nim' => 'required|unique:peserta|size:15',
        'gender' => 'required|numeric',
        'phone' => 'required',
        'angkatan' => 'required',
        'email' => 'required|email',
        'motivasi' => 'required|max:500',
        'ide_kreatif' => 'required|max:500',
        'lampiran' => 'required|mimes:pdf|max:1024',
    ]);
}

protected function create(Request $request)
{
    $sesi = Oprec::where('status_dibuka_oprec','dibuka')->first();
    $lampiran = $request->file('lampiran');
    $name_lampiran = $sesi->nama_oprec."-".$request->input('nim')."-lampiran.".$lampiran->getClientOriginalExtension();
    $path_lampiran = "files/lampiran/".$name_lampiran;
    $lampiran->move((public_path()."/files/lampiran/"),$name_lampiran);
    $new = new Peserta();
    $new->nama =$request->input('nama');
    $new->nim = $request->input('nim');
    $new->gender = $request->input('gender');
    $new->phone = $request->input('phone');
    $new->angkatan = $request->input('angkatan');
    $new->email = $request->input('email');
    $new->motivasi = $request->input('motivasi');
    $new->ide_kreatif = $request->input('ide_kreatif');
    $new->lampiran = $path_lampiran;
    $new->save();


    $id = $new->id;
    $status = new Status();
    $status->peserta_id = $id;
    $status->oprec_id = $sesi->id;
    $status->save();
}

public function register(Request $request)
{
    $this->validator($request->all())->validate();
    $this->create($request);
    return redirect('peserta/kelola-data')->with('success', ('Data registrasi berhasil ditambahkan !'));
}

public function contohBerkas(){
  $file = public_path().'/files/lampiran/TEMPLATE-CV.docx';
  $headers = array('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
  return Response::download($file, 'contoh-berkas.docx',$headers);
}
    
    public function progres(){
        $peserta = Session::get('users');

        $cek = Oprec::where('status_dibuka_oprec','dibuka')->get();
        if (count($cek) == 1) {
            $alreadyuser = Peserta::where('nim',$peserta->user_name)->get();
            $sudahDaftarSesiSekarang = null;
            $psrta = null;
            if($alreadyuser){
                $i = 0;
                foreach ($alreadyuser as $ulang) {
                    $sudahDaftarSesiSekarang = Status::where('oprec_id',$cek[0]->id)->where('peserta_id',$ulang->id)->first();
                    if($sudahDaftarSesiSekarang != null || count($alreadyuser) == 1 ){
                        $psrta = $sudahDaftarSesiSekarang;
                        break;
                    }
                    $i++;
                }
                if($sudahDaftarSesiSekarang){
                    if ($sudahDaftarSesiSekarang->status_pendaftaran == 'lolos') {
                        return view('frontend.progres', compact('cek','psrta'))->with('status','buka')->with('daftar','sudah');
                    }else{
                        return view('frontend.progres', compact('cek','psrta'))->with('status','buka')->with('daftar','tdklolos');
                    }
                }
                else{
                return view('frontend.progres', compact('cek'))->with('status','buka')->with('daftar','belum');
                }
            }
            
        }else if(count($cek) > 1){
            return view('frontend.progres')->with('status','kelebihan'); //Lebih dari 1 sesi yang terbuka
        }else{
            return view('frontend.progres')->with('status','tutup');    //tidak ada sesi yg terbuka
        }
        
    }

}
