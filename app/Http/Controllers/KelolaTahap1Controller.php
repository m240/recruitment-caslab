<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Pertanyaan;
use App\Jawaban;
use App\Pilihan;
use App\Tahap1;
use App\Peserta;
use App\Kategori;
use App\Status;
use Illuminate\Support\Str;

class KelolaTahap1Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($id)
    {
        $pesertas = Tahap1::all()->where('oprec_id', $id);

        // return $pesertas;

        return view('backend.sessions.tahap1.index', compact('pesertas'));
    }

    public function pertanyaan()
    {
        $questions = Pertanyaan::all()->sortBy('id');
        $count_category = Kategori::count();
        $categories = Kategori::all()->sortBy('id');

        $correct_collections = [
            'option1' => 'Option #1',
            'option2' => 'Option #2',
            'option3' => 'Option #3',
            'option4' => 'Option #4',
        ];
        // return $correct_collections;
        return view('backend.sessions.tahap1.pertanyaan', compact('correct_collections', 'questions', 'categories', 'count_category'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'soal' => 'required',
            'option1' => 'required',
            'option2' => 'required',
            'option3' => 'required',
            'option4' => 'required',
        ],[
            'soal.required' => 'Questions must be filled',
            'option1.required' => 'Option #1 must be filled',
            'option2.required' => 'Option #2 must be filled',
            'option3.required' => 'Option #3 must be filled',
            'option4.required' => 'Option #4 must be filled',
        ]);
        // return $request->soal;
        $question = Pertanyaan::create($request->all());

        if ($request->file('gambar')) {
            # code...
            $file = $request->file('gambar')->store('images_questions', 'public');
            $question->gambar = $file;
            $question->save();
        }


        foreach ($request->input() as $key => $value) {
            if (strpos($key, 'option') !== false && $value != '' ) {
                $correct =  $request->input('correct') == $key ? 1 : 0;
                Pilihan::create([
                    'pertanyaan_id' => $question->id,
                    'isi'           => $value,
                    'status'        => $correct
                ]);
            }
        }

        return redirect()->route('tahap1.pertanyaan')->with('success', 'Successfully created a question !');
    }

    public function detail($id)
    {
        $question = Pertanyaan::where('id', $id)->first();
        $category = Kategori::where('id', $question->kategoris_id)->first();
        $option = Pilihan::where('pertanyaan_id', $question->id)->get();

        $json = json_encode(array(
            'category' => $category,
            'question' => $question,
            'option' => $option,
        ));

        return $json;
    }

    public function detailPeserta($id)
    {
        $detail = Status::where('peserta_id', $id)->get()->first();
        return $detail;
    }

    public function update(Request $request)
    {
        $question = Pertanyaan::firstOrNew(['id' => $request->id]);
        $option = [];

        $question->fill($request->all());

        if ($request->file('gambar')) {
            # code...
            if ($question->gambar && file_exists(storage_path('app/public/'.$question->gambar))) {
                # code...
                \Storage::delete(['public/'.$question->gambar]);
            }
            
            $file = $request->file('gambar')->store('images_questions', 'public');
            $question->gambar = $file;
        }



        $question->save();

        foreach ($request->input() as $key => $value) {
            if (strpos($key, 'option') !== false && $value != '' ) {
                $correct =  $request->input('correct') == $key ? 1 : 0;
                $option->update([
                    'isi'           => $value,
                    'status'        => $correct
                ]);
            }elseif (strpos($key, 'id_opt') !== false) {
                $option = Pilihan::firstOrNew(['id' => $value]);
            }
        }

        return redirect()->route('tahap1.pertanyaan')->with('success', 'Successfully edit questions !');
    }

    public function detailDelete($id)
    {
        $delete = Pertanyaan::find($id);

        return $delete;
    }

    public function destroy(Request $request)
    {
        $destroy = Pertanyaan::find($request->id);

        $destroy->delete();

        return redirect()->route('tahap1.pertanyaan')->with('success', 'Successfully deleted the question !');
    }

    public function anyData($id)
    {
        $model = Tahap1::select(['score_total', 'peserta_id'])->where('sesi_oprec_id', $id)->get();

        // $model = Tahap1::with('sesi', 'peserta')->select('tahap_1.*');
        return Datatables::of($model)->addIndexColumn()->addColumn('peserta', function($model){
            $nama_peserta = Peserta::select(['nama'])->where('id', $model->peserta_id)->first();

            return $nama_peserta;
        })->addColumn('action', function($model){
            return '<a class="btn btn-sm btn-info" onclick="detail('."'".$model->id."'".')"><i class="fa fa-search"></i> Detail</a>';
        })->make(true);

        // return $model;
        // return Datatables::of($model)->addIndexColumn()->addColumn('')->make(true);
    }

    public function store_category(Request $request)
    {
        $new_category = new Kategori();
        $new_category->fill($request->all());

        $new_category->save();

        return redirect()->route('tahap1.pertanyaan')->with('success', 'Successfully created a category !');
    }

    public function detail_category($id)
    {
        $json = Kategori::find($id);

        return $json;   
    }

    public function update_category(Request $request)
    {
        $update_category = Kategori::find($request->id);

        $update_category->fill($request->all());
        $update_category->save();

        return redirect()->route('tahap1.pertanyaan')->with('success', 'Successfully updated a category !');
    }

    public function destroy_category(Request $request)
    {
        $destroy_category = Kategori::find($request->id);

        $destroy_category->delete();

        return redirect()->route('tahap1.pertanyaan')->with('success', 'Successfully deleted a category !');
    }

    public function updateStatusPeserta(Request $request)
    {
        $update = Status::find($request->id);
        $update->status_tahap1 = $request->status_tahap1;
        $update->save();

        return redirect()->route('tahap1.index', $update->oprec_id);
    }
}
