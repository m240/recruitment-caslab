<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Status;
use App\Oprec;

class PendaftaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index($id){
        $status = Status::where('oprec_id',$id)->where('status_pendaftaran','lolos')->get();
        $sesi = Oprec::where('id', $id)->get()->first();

        
        $male = 0;
        $female = 0;
        $total = 0;
        foreach($status as $peserta){
            if($peserta->peserta->gender==1){
                $male++;
            }else{
                $female++;
            }
            $total++;
        }
        $gender=[$female,$male];
        return view('backend/sessions/pendaftaran/lolos-pendaftaran',compact('status','sesi','gender','total'));
    }
    
    public function aktif($id){
        $sesi = Oprec::findOrfail($id);
        $sesi->status_pendaftaran = "dibuka";
        $sesi->save();
        return  redirect()->route('timeline.index', $id)->with('success', 'Successfully change status!');
    }

    public function nonaktif($id){
        $sesi = Oprec::findOrfail($id);
        $sesi->status_pendaftaran = "ditutup";
        $sesi->save();
        return redirect()->route('timeline.index', $id)->with('success', 'Successfully change status!');
    }

    public function detail($id)
    {
        $status = Status::findOrfail($id);
        return $status;
    }

    public function update(Request $request)
    {
        $status = Status::findOrfail($request->id);
       $status->status_pendaftaran = $request->status_pendaftaran;
       $status->save();
        return redirect()->route('lolos.pendaftaran', $status->oprec_id)->with('success', 'Successfully change status!');
   }



}
