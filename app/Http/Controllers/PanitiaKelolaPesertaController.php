<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Peserta;
use App\User;
use App\Season;

class PanitiaKelolaPesertaController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:panitia');
    }

    public function index()
    {
        $sessions = Season::all();
        $pesertas = Peserta::all();
        return view('backend.peserta.forpanitia', compact('sessions', 'pesertas'));
    }

    public function detail($id)
    {
        $peserta = Peserta::where('id', $id)->get();

        return $peserta;
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'nim' => 'required|numeric',
                'angkatan' => 'required|numeric',
                'motivasi' => 'required',
            ],
            [
                'nim.required' => 'NIM of peserta must be required',
                'nim.numeric' => 'NIM of peserta must be a number',
                'nim.size' => 'Digits of NIM must be 15',
                'angkatan.required' => 'Angkatan of peserta must be required',
                'angkatan.numeric' => 'Angkatan of peserta must be a number',
                'angkatan.size' => 'Digits of Angkatan must be 4',
                'motivasi.required' => 'Motivasi of peserta must be required',
            ]);

        $user = User::where('user_name', $request->nim)->first();
        $store = new Peserta();
        $store->nama = $user->full_name;
        $store->nim = $request->nim;
        $store->angkatan = $request->angkatan;
        $store->email = $user->email;
        $store->phone = $user->phone;
        $store->gender = $user->gender;
        $store->motivasi = $request->motivasi;
        $store->season_id = $request->session_id;

        if ($request->file('lampiran')) {
            $file = $request->nim.".".$request->file('lampiran')->getClientOriginalExtension();
            $request->file('lampiran')->storeAs('public', $file);
            $store->lampiran = $file;
        }

        $store->save();
        return redirect('panitia/peserta')->with('success', 'Success added peserta !');
    }

    public function update(Request $request)
    {
        $update = Peserta::findOrFail($request->id);
        if ($request->file('lampiran')) {
            if ($update->lampiran != 'NULL') {
                unlink(storage_path('app/public/'.$update->lampiran));
                $file = $update->nim.".".$request->file('lampiran')->getClientOriginalExtension();
                $request->file('lampiran')->storeAs('public', $file);
                $update->lampiran = $file;
            }else {
                $file = $update->nim.".".$request->file('lampiran')->getClientOriginalExtension();
                $request->file('lampiran')->storeAs('public', $file);
                $update->lampiran = $file;
            }
        }
        $update->fill($request->all());
        $update->save();
        return redirect('panitia/peserta')->with('success', 'Success updated peserta !');

    }

    public function delete(Request $request)
    {
        $destroy = Peserta::findOrFail($request->id);
        unlink(storage_path('app/public/'.$destroy->lampiran));
        $destroy->delete();

        return redirect('panitia/peserta')->with('success', 'Success deleted peserta !');
    }

    public function download_csv()
    {
        $pesertas = \App\Peserta::all();
        $csvExporter = new \Laracsv\Export();
        return $csvExporter->build($pesertas, [
            'id',
            'nama', 
            'nim',
            'gender',
            'phone',
            'angkatan', 
            'email', 
            'motivasi',
            'ide_kreatif',
            'tahap1',
            'tahap2',
            'tahap3',
            'status',
            'lampiran',
            'season_id',
        ])->download();
    
    }
}
