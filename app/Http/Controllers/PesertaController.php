<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use App\Oprec;
use App\Pertanyaan;
use App\Pilihan;
use App\Kategori;
use App\Tahap1;
use App\Peserta;
use App\Jawaban;
use App\Status;

class PesertaController extends Controller
{
    // protected $score;
    // protected $id_tahap1 = 0;
    protected $total_final = 0;
    protected $total_jawab_kategori = 0;

    public function index()
    {
        return view('frontend.dashboard');
    }

    public function detail($id)
    {
        $detail = Season::where('id', $id)->get();
        return $detail;
    }

    public function tahapSatu(Request $request)
    {
        $time1 = 0;
        $index = 0;
        $count_tahap1 = Tahap1::whereHas('peserta.tahap1', function ($query) {
            $query->where('nim', Session::get('users')->user_name);
        })->count();

        if ($count_tahap1 == 0) {
            $store = new Tahap1();
            $store->oprec_id = Oprec::where('status_dibuka_oprec', 'dibuka')->value('id');
            $store->peserta_id = Peserta::where('nim', Session::get('users')->user_name)->value('id');
            $store->save();
        } else {
            return redirect()->route('peserta.progres');
        }

        $id_tahap1 = Tahap1::whereHas('peserta.tahap1', function ($query) {
            $query->where('nim', Session::get('users')->user_name);
        })->value('id');
        // $id_tahap1 = $this->id_tahap1;

        // return $id_tahap1;
        $categories = Kategori::orderBy('id', 'asc')->get();
        foreach ($categories as $category) {
            $time1 += $category->time;
        }

        $questions = Pertanyaan::inRandomOrder()->get();
        $count = Pertanyaan::count();
        foreach ($questions as $question) {
            $question->options = Pilihan::where('pertanyaan_id', $question->id)->inRandomOrder()->get();
        }

        return view('frontend.tahap1', compact('categories', 'questions', 'time1', 'count', 'index', 'id_tahap1'));
    }

    public function storeJawaban(Request $request)
    {
        $id_jawaban = $request->choices;
        $id_pertanyaan = $request->pertanyaan_id;
        // $this->id_tahap1 = $request->id_tahap1;

        $store = new Jawaban();
        $store->tahap_1_id = $request->id_tahap1;
        $store->pilihan_id = $id_jawaban;
        $store->pertanyaan_id = $id_pertanyaan;

        if (Pilihan::where('id', $id_jawaban)->value('status') == 1) {
            $store->jawab = Pertanyaan::where('id', $id_pertanyaan)->value('score');
        } else {
            $store->jawab = 0;
        }

        $respons = $store->save();
    }

    public function finalScore(Request $request)
    {
        // $id_tahap1 = $this->id_tahap1;

        $total_final = 0;
        // $total = 0;

        $categories = Kategori::all();
        foreach ($categories as $category) {
            $total_jawab_kategori = 0;
            $pertanyaans = $category->questions;
            foreach ($pertanyaans as $pertanyaan) {
                $total_jawab_kategori += $pertanyaan->jawaban->where('tahap_1_id', $request->id_tahap1)->sum('jawab');

                # code...
            }
            // return $total_pertanyaan_kategori;
            $total = $total_jawab_kategori * ($category->persentase / 100);
            $total_final += $total;
            // echo $total;
        }

        // return $total_final;
        $update = Tahap1::find($request->id_tahap1);
        $update->score_total = $total_final;
        $update->save();

        return redirect()->route('peserta.progres');
        // ((100*jumlah_soal_benar)/jumlah_soal)*(persentase/100)
    }
}
