<?php

namespace App\Http\Controllers;

use App\PengumumanTahap2;

use App\Status;
use App\Oprec;
use App\PengumumanTahap3;
use App\PengumumanTahap3TdkLolos;

use Illuminate\Http\Request;

class PengumumanController extends Controller
{
    public function addPengumumanTahap2(Request $request)
    {
        $request->validate(
            [
                'oprec_id' => 'required|string',
                'namapengumumantahap2' => 'required',
            ],
            [
                'oprec_id.required' => 'id oprec must be required',
                'namapengumumantahap2.required' => 'pengumuman tahap 2 kriteria must be required',

            ]
        );
        $oprecId = $request->oprec_id;
        $namaPengumuman = $request->name;
        $pengumumantahap2 = new PengumumanTahap2();
        $pengumumantahap2->namapengumumantahap2 = $request->namapengumumantahap2;
        $pengumumantahap2->oprec_id = $oprecId;
        $pengumumantahap2->save();
        return redirect()->back()->with('success', 'Successfully added kriteria Pengumuman Tahap 2');
    }

    public function showPengumumantahap2($id)
    {
        $sesi = Oprec::findOrfail($id);
        $pesertas = Status::where('oprec_id', '=', $id)
            ->where('status_tahap2', '=', 'menunggu')
            ->get();
        $pengumuman = PengumumanTahap2::all();
        return view('frontend.showpengumumantahap2', ['liat' => $pengumuman]);
    }
    public function showPengumumantahap3($id)
    {
        $sesi = Oprec::findOrfail($id);
        $pesertas = Status::where('oprec_id', '=', $id)
            ->where('status_tahap3', '=', 'lolos')
            ->get();
        $pengumuman3 = pengumumantahap3::all();
        return view('frontend.showpengumumantahap3', ['liat3' => $pengumuman3]);
    }

    public function showPengumumantahap3TdkLolos($id)
    {
        $sesi = Oprec::findOrfail($id);
        $pesertas = Status::where('oprec_id', '=', $id)
            ->where('status_tahap3', '=', 'tidak lolos')
            ->get();
        $pengumumantdklolos = pengumumantahap3tdklolos::all();
        return view('frontend.showpengumumantahap3tdklolos', ['liattdklolos' => $pengumumantdklolos]);
    }
    public function addPengumumanTahap3(Request $request)
    {
        $request->validate(
            [
                'oprec_id' => 'required|string',
                'namapengumumantahap3' => 'required',
            ],
            [
                'oprec_id.required' => 'id oprec must be required',
                'namapengumumantahap3.required' => 'pengumuman tahap 3 kriteria must be required',

            ]
        );
        $oprecId = $request->oprec_id;
        $namaPengumuman3 = $request->name;
        $pengumumantahap3 = new PengumumanTahap3();
        $pengumumantahap3->namapengumumantahap3 = $request->namapengumumantahap3;
        $pengumumantahap3->oprec_id = $oprecId;
        $pengumumantahap3->save();
        return redirect()->back()->with('success', 'Successfully added kriteria Pengumuman Tahap 3');
    }

    public function addPengumumanTahap3TdkLolos(Request $request)
    {
        $request->validate(
            [
                'oprec_id' => 'required|string',
                'namapengumumantahap3tdklolos' => 'required',
            ],
            [
                'oprec_id.required' => 'id oprec must be required',
                'namapengumumantahap3tdklolos.required' => 'pengumuman tahap 3 tdk lolos kriteria must be required',

            ]
        );
        $oprecId = $request->oprec_id;
        $namaPengumuman3tdklolos = $request->name;
        $pengumumantahap3tdklolos = new PengumumanTahap3TdkLolos();
        $pengumumantahap3tdklolos->namapengumumantahap3tdklolos = $request->namapengumumantahap3tdklolos;
        $pengumumantahap3tdklolos->oprec_id = $oprecId;
        $pengumumantahap3tdklolos->save();
        return redirect()->back()->with('success', 'Successfully added kriteria Pengumuman Tahap 3 tdk lolos');
    }
}
