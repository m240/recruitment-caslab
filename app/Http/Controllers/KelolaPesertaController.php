<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Peserta;
use App\User;
use App\Skill;
use App\SkillPeserta;

class KelolaPesertaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $pesertas = Peserta::all();
        $skills = Skill::all();
        $male = 0;
        $female = 0;
        $total = 0;
        foreach($pesertas as $peserta){
            if($peserta->gender==1){
                $male++;
            }else{
                $female++;
            }
            $total++;
        }
        $gender=[$female,$male];
        return view('backend.peserta.index', compact('pesertas','gender','total','skills'));
    }

    public function detail($id)
    {
        $peserta = Peserta::find($id);
        return $peserta;
    }

    public function tambah_skill(Request $request){
        $request->validate(
            [
                'nama_skill' => 'required|string',
            ],
            [
                'nama_skill.required' => 'Name of skill must be required',
                'nama_skill.string' => 'Name of skill must be string'
            ]);
        $check_skill = Skill::where('JenisSkill', strtolower($request->nama_skill))->first();
        if($check_skill){
            return redirect('adm1n/peserta')->with('failed', 'Jenis skill already exist !');
        }
        $skill = new Skill();
        $skill->JenisSkill = strtolower($request->nama_skill);
        $skill->save();
        return redirect('adm1n/peserta')->with('success', 'Success added new skill !');
    }

    public function skillPeserta($id)
    {
        $check_peserta = Peserta::where('id', $id)->first();
        if(!$check_peserta){
            return null;
        }
        $skill_peserta = Peserta::where('id', '=', $id)
            ->with('skills')
            ->first();
        return $skill_peserta->skills;
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'nim' => 'required|numeric',
                'motivasi' => 'required',
            ],
            [
                'nim.required' => 'NIM of peserta must be required',
                'nim.numeric' => 'NIM of peserta must be a number',
                'nim.size' => 'Digits of NIM must be 15',
                'motivasi.required' => 'Motivasi of peserta must be required',
            ]);

        $user = User::where('user_name', $request->nim)->first();
        if($user){
            if(Peserta::where('nim', $request->nim)->first()){
                return redirect('adm1n/peserta')->with('failed', 'Peserta already exist !');
            }
            $store = new Peserta();
            $store->nama = $user->full_name;
            $store->nim = $request->nim;
            $store->angkatan = substr($user->user_name,0,4);
            $store->email = $user->email;
            $store->phone = $user->phone;
            $store->gender = $user->gender;
            $store->motivasi = $request->motivasi;
            if ($request->file('lampiran')) {
                $lampiran = $request->file('lampiran');
                $name_lampiran = $request->input('nim')."-lampiran.".$lampiran->getClientOriginalExtension();
                $path_lampiran = "/files/lampiran/".$name_lampiran;
                $lampiran->move((public_path()."/files/lampiran/"),$name_lampiran);
                $store->lampiran = $path_lampiran;
            }
            $store->save();

            // create status
            return redirect('adm1n/peserta')->with('success', 'Success added peserta !');
        }else{
            return redirect('adm1n/peserta')->with('failed', 'Failed to search peserta !');
        }
        
    }

    public function update(Request $request)
    {
        $update = Peserta::findOrFail($request->id);
        if ($request->file('lampiran')) {
            if ($update->lampiran != NULL) {
                $lampiran = $request->file('lampiran');
                $name_lampiran = $update->nim."-lampiran.".$lampiran->getClientOriginalExtension();
                $path_lampiran = "/files/lampiran/".$name_lampiran;
                unlink(public_path().$update->lampiran);
                $lampiran->move((public_path()."/files/lampiran/"),$name_lampiran);
                $update->lampiran = $path_lampiran;
            }else {
                $lampiran = $request->file('lampiran');
                $name_lampiran = $update->nim."-lampiran.".$lampiran->getClientOriginalExtension();
                $path_lampiran = "/files/lampiran/".$name_lampiran;
                $lampiran->move((public_path()."/files/lampiran/"),$name_lampiran);
                $update->lampiran = $path_lampiran;
            }
        }
        $update->email = $request->email;
        $update->phone = $request->phone;
        $update->motivasi = $request->motivasi;
        $update->save();
        
        $skillId = []; 
        for($x = 1; $x<=Skill::all()->count() ;$x++){
            $getskill = "skill_".$x;
            if($request->$getskill=="on"){
                array_push($skillId, $x);
            }
        }
        $peserta = Peserta::find($request->id);	
        $peserta->skills()->sync($skillId);
        return redirect('adm1n/peserta')->with('success', 'Success updated peserta !');
    }

    public function delete(Request $request)
    {
      $destroy = Peserta::findOrFail($request->id);
    //   $jawaban = 
      try{
        if($destroy->lampiran != NULL){
            unlink(public_path()."/".$destroy->lampiran);
        }
      }catch(Exception $e){

      }
      $destroy->delete();
      return redirect('adm1n/peserta')->with('success', 'Success deleted peserta !');
    }
}
