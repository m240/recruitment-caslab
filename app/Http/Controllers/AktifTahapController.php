<?php

namespace App\Http\Controllers;

use App\Tahap2;
use App\Status;
use App\Oprec;
use App\Wawancara1;
use App\Microteaching;
use App\TotalTahap2;
use App\PengumumanTahap2;
use DB;
use Illuminate\Http\Request;
use Mockery\Exception;
use App\NilaiWawancara1;
use App\NilaiMicroteaching;

class AktifTahapController extends Controller
{
    public function aktif_tahap2($id)
    {
        $sesi = Oprec::findOrfail($id);
        $sesi->status_tahap2 = "dibuka";
        $sesi->save();
        return  redirect()->route('timeline.index', $id)->with('success', 'Successfully change status!');
    }

    public function nonaktif_tahap2($id)
    {
        $sesi = Oprec::findOrfail($id);
        $sesi->status_tahap2 = "ditutup";
        $sesi->save();
        return redirect()->route('timeline.index', $id)->with('success', 'Successfully change status!');
    }

    public function aktif_tahap3($id)
    {
        $sesi = Oprec::findOrfail($id);
        $sesi->status_tahap3 = "dibuka";
        $sesi->save();
        return  redirect()->route('timeline.index', $id)->with('success', 'Successfully change status!');
    }

    public function nonaktif_tahap3($id)
    {
        $sesi = Oprec::findOrfail($id);
        $sesi->status_tahap3 = "ditutup";
        $sesi->save();
        return redirect()->route('timeline.index', $id)->with('success', 'Successfully change status!');
    }
}
