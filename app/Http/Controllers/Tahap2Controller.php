<?php

namespace App\Http\Controllers;

use App\Tahap2;
use App\Status;
use App\Oprec;
use App\Wawancara1;
use App\Microteaching;
use App\TotalTahap2;
use DB;
use Illuminate\Http\Request;
use Mockery\Exception;
use App\NilaiWawancara1;
use App\NilaiMicroteaching;

class Tahap2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($id)
    {
        $sesi = Oprec::where('id', $id)->get()->first();
        $pesertas = Status::where('oprec_id', '=', $id)
            ->where('status_tahap1', '=', 'lolos')
            ->get();
        return view('backend.sessions.tahap2.index', compact('pesertas', 'sesi'));
    }

    public function detailWawancara1($oprec_id, $peserta_id)
    {
        $sesi = Oprec::where('id', $oprec_id)->get()->first();
        $peserta = Status::where('oprec_id', '=', $oprec_id)
            ->where('peserta_id', '=', $peserta_id)
            ->get();
        $kt_wawancara1 = $sesi->wawancara1;
        return view('backend.sessions.tahap2.wawancara1', compact('sesi', 'peserta', 'kt_wawancara1'));
    }

    public function detailMicroteaching($oprec_id, $peserta_id)
    {
        $sesi = Oprec::where('id', $oprec_id)->get()->first();
        $peserta = Status::where('oprec_id', '=', $oprec_id)
            ->where('peserta_id', '=', $peserta_id)
            ->get();
        $tahap = Tahap2::where('oprec_id', '=', $oprec_id)
            ->where('peserta_id', '=', $peserta_id)
            ->first();
        if (isset($tahap->tema)) {
            $tema = $tahap->tema;
        } else {
            $tema = "Belum mengisi tema";
        }
        $kt_microteaching = $sesi->microteaching;
        return view('backend.sessions.tahap2.microteaching', compact('sesi', 'tema', 'peserta', 'kt_microteaching'));
    }

    public function updateKelulusan(Request $request)
    {
        $statusKelulusan = $request->status_kelulusan;
        $oprec_id = $request->oprec_id;
        $ids = $request->id;
        $message = 'Successfully update data!';
        $statusMessage = "success";
        if ($ids != null) {
            DB::table('status')
                ->whereIn('id', $ids)
                ->where('oprec_id', '=', $oprec_id)
                ->update(['status_tahap2' => $statusKelulusan]);
        } else {
            $message = 'Failed update data nothing checked!';
            $statusMessage = "failed";
        }
        return redirect()->back()->with($statusMessage, $message);
    }

    public function addKriteriaWawancara1(Request $request)
    {
        $request->validate(
            [
                'oprec_id' => 'required|string',
                'name' => 'required',
                'prosentase' => 'required|numeric'
            ],
            [
                'oprec_id.required' => 'id oprec must be required',
                'name.required' => 'nama kriteria must be required',
                'prosentase.required' => 'prosentase kriteria must be required',
                'prosentase.numeric' => 'prosentase kriteria must be number',
            ]
        );
        $oprecId = $request->oprec_id;
        $namaKriteria = $request->name;
        $prosentase = $request->prosentase;
        $wawancara1 = new Wawancara1();
        $wawancara1->nama_kriteria = $namaKriteria;
        $wawancara1->prosentase = $prosentase;
        $wawancara1->oprec_id = $oprecId;
        $wawancara1->save();
        return redirect()->back()->with('success', 'Successfully added kriteria wawancara 1!');
    }

    public function updateNilaiTotal($oprec_id)
    {
        $updatePeserta = Tahap2::where('oprec_id', '=', $oprec_id)->get();
        $kriteria = TotalTahap2::where('oprec_id', '=', $oprec_id)->get()->first();
        foreach ($updatePeserta as $peserta) {
            if (isset($peserta->score_total_wawancara_1) && isset($peserta->score_total_microteaching)) {
                $scoreWawancara = $kriteria->prosentase_wawancara_1 / 100 * $peserta->score_total_wawancara_1;
                $scoreMicroteaching =  $kriteria->prosentase_microteaching / 100 * $peserta->score_total_wawancara_1;
                $peserta->score_total_tahap_2 = $scoreWawancara + $scoreMicroteaching;
                $peserta->save();
            }
        }
    }

    public function addMicroteaching(Request $request)
    {
        $request->validate(
            [
                'oprec_id' => 'required|string',
                'name' => 'required',
                'prosentase' => 'required|numeric'
            ],
            [
                'oprec_id.required' => 'id oprec must be required',
                'name.required' => 'nama kriteria must be required',
                'prosentase.required' => 'prosentase kriteria must be required',
                'prosentase.numeric' => 'prosentase kriteria must be number',
            ]
        );
        $oprecId = $request->oprec_id;
        $namaKriteria = $request->name;
        $prosentase = $request->prosentase;
        $microteaching = new Microteaching();
        $microteaching->nama_kriteria = $namaKriteria;
        $microteaching->prosentase = $prosentase;
        $microteaching->oprec_id = $oprecId;
        $microteaching->save();
        return redirect()->back()->with('success', 'Successfully added kriteria microteaching!');
    }

    public function deleteKriteriaWawancara1(Request $request)
    {
        $destroy = Wawancara1::findOrFail($request->id);
        $destroy->delete();
        return redirect()->back()->with('success', 'Successfully delete data !');
    }


    public function deleteKriteriaMicroteaching(Request $request)
    {
        $destroy = Microteaching::findOrFail($request->id);
        $destroy->delete();
        return redirect()->back()->with('success', 'Successfully delete data !');
    }


    public function setTotalTahap2(Request $request)
    {
        $oprec_id = $request->oprec_id;
        $total = TotalTahap2::where('oprec_id', '=', $oprec_id)->get()->first();
        if ($total == NULL) {
            $totalTahap2 = new TotalTahap2();
            $totalTahap2->prosentase_wawancara_1 = $request->wawancara1;
            $totalTahap2->prosentase_microteaching = $request->microteaching;
            $totalTahap2->oprec_id = $oprec_id;
            $totalTahap2->save();
        } else {
            $total->prosentase_wawancara_1 = $request->wawancara1;
            $total->prosentase_microteaching = $request->microteaching;
            $total->save();
        }
        $this->updateNilaiTotal($oprec_id);
        return redirect()->back()->with('success', 'Successfully update persentase tahap 2 !');
    }

    public function updateNilaiWawancara1(Request $request)
    {
        $oprec_id = $request->oprec_id;
        $peserta_id = $request->peserta_id;
        $data_peserta = Tahap2::firstOrNew([
            'peserta_id' => $peserta_id,
            'oprec_id' => $oprec_id
        ]);
        $total_nilai = 0;
        foreach (array_combine($request->id_kt_wawancara1, $request->kt_wawancara1) as $id => $nilai) {
            print($id . ":" . $nilai . "\n");
            $data_nilai = NilaiWawancara1::firstOrNew([
                'wawancara1_id' => $id,
                'peserta_id' => $peserta_id
            ]);
            $data_nilai->wawancara1_id = $id;
            $data_nilai->nilai = $nilai;
            $data_nilai->peserta_id = $peserta_id;
            $data_nilai->save();
            $kriteria = Wawancara1::where('id', '=', $id)->get()->first();
            $persentase = $kriteria->prosentase;
            $total_nilai += ($persentase / 100) * $nilai;
        }
        $data_peserta->catatan_wawancara_1 = $request->catatan_wawancara;
        $data_peserta->oprec_id = $oprec_id;
        $data_peserta->peserta_id = $peserta_id;
        $data_peserta->score_total_wawancara_1 = $total_nilai;
        $data_peserta->save();
        $this->updateNilaiTotal($oprec_id);
        return redirect()->back()->with('success', 'Successfully update nilai peserta !');
    }


    public function updateNilaiMicroteaching(Request $request)
    {
        $oprec_id = $request->oprec_id;
        $peserta_id = $request->peserta_id;
        $data_peserta = Tahap2::firstOrNew([
            'peserta_id' => $peserta_id,
            'oprec_id' => $oprec_id
        ]);
        $total_nilai = 0;
        foreach (array_combine($request->id_kt_microteaching, $request->kt_microteaching) as $id => $nilai) {
            $data_nilai = NilaiMicroteaching::firstOrNew([
                'microteaching_id' => $id,
                'peserta_id' => $peserta_id
            ]);
            $data_nilai->microteaching_id = $id;
            $data_nilai->nilai = $nilai;
            $data_nilai->peserta_id = $peserta_id;
            $data_nilai->save();
            $kriteria = Microteaching::where('id', '=', $id)->get()->first();
            $persentase = $kriteria->prosentase;
            $total_nilai += ($persentase / 100) * $nilai;
        }
        $data_peserta->catatan_microteaching = $request->catatan_microteaching;
        $data_peserta->oprec_id = $oprec_id;
        $data_peserta->peserta_id = $peserta_id;
        $data_peserta->score_total_microteaching = $total_nilai;
        $data_peserta->save();
        $this->updateNilaiTotal($oprec_id);
        return redirect()->back()->with('success', 'Successfully update nilai peserta !');
    }

    public function addTema(Request $request)
    {
        $oprec_id = $request->oprec_id;
        $peserta_id = $request->peserta_id;
        $data_peserta = Tahap2::firstOrNew([
            'peserta_id' => $peserta_id,
            'oprec_id' => $oprec_id
        ]);
        $data_peserta->tema = $request->judul_tema;
        $data_peserta->oprec_id = $oprec_id;
        $data_peserta->peserta_id = $peserta_id;
        $data_peserta->save();
        return redirect()->back()->with('success', 'Successfully update tema peserta !');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
