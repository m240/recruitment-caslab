<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class KelolaPanitiaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('backend.manage-panitia.index');
    }

   	public function store(Request $request)
    {
        $request->validate([
            'nim' => 'required|unique:panitia',
            'job_title' => 'required',
        ]);
        $nim = $request->nim;
        $ilab = User::where('user_name', $nim)->first();
        $user_ilab = User::where('user_name', $nim)->first();
        try {
            $user_ilab = $ilab->user_name;
        } catch (\Throwable $th) {

        }
        if($nim === $user_ilab){
            $role = Role::where('peran',$request->job_title)->first();
            $new = new Panitia();
            try{
                $new->name = $ilab->full_name;
                $new->nim = $ilab->user_name;
                $new->password = bcrypt($ilab->password);
                $new->email = $ilab->email;
                $new->job_title = $role->peran;
                $new->role_id = $role->id;
                $new->save();
            }catch (\Throwable $th) {

            }
            return redirect('adm1n/manage-panitia')->with('success', 'Sukses menambah data !');
        }else{
            return redirect('adm1n/manage-panitia')->with('failed', ('Gagal menambahkan data, nim tidak ditemukan!'));
        }
    }

    public function detail($id){
        $detail = Panitia::where('id', $id)->get();
        return $detail;
    }

    public function update(Request $request)
    {
        $update = Panitia::findOrFail($request->id);
        $role = Role::where('peran',$request->job_title)->first();
        $update->name = $request->name;
        $update->job_title = $role->peran;
        $update->role_id = $role->id;
        $update->save();
        return redirect('adm1n/manage-panitia')->with('success', 'Sukses mengupdate data !');
    }

    public function delete(Request $request)
    {
        $destroy = Panitia::findOrFail($request->id);
        $destroy->delete();
        return redirect('adm1n/manage-panitia')->with('success', 'Sukses menghapus data !');
    }
}
