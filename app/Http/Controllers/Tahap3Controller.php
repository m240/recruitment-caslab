<?php

namespace App\Http\Controllers;

use App\Tahap3;
use App\Status;
use App\Oprec;
use App\Wawancara2;
use App\Magang;
use App\Project;
use App\TotalTahap3;
use DB;
use Illuminate\Http\Request;
use Mockery\Exception;
use App\NilaiWawancara2;
use App\NilaiMagang;
use App\NilaiProject;

class Tahap3Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($id)
    {
        $sesi = Oprec::where('id', $id)->get()->first();
        $pesertas = Status::where('oprec_id', '=', $id)
            ->where('status_tahap2', '=', 'lolos')
            ->get();
        return view('backend.sessions.tahap3.index', compact('pesertas', 'sesi'));
    }

    public function detailWawancara2($oprec_id, $peserta_id)
    {
        $sesi = Oprec::where('id', $oprec_id)->get()->first();
        $peserta = Status::where('oprec_id', '=', $oprec_id)
            ->where('peserta_id', '=', $peserta_id)
            ->get();
        $kt_wawancara2 = $sesi->wawancara2;
        return view('backend.sessions.tahap3.wawancara2', compact('sesi', 'peserta', 'kt_wawancara2'));
    }

    public function detailMagang($oprec_id, $peserta_id)
    {
        $sesi = Oprec::where('id', $oprec_id)->get()->first();
        $peserta = Status::where('oprec_id', '=', $oprec_id)
            ->where('peserta_id', '=', $peserta_id)
            ->get();
        $kt_magang = $sesi->magang;
        return view('backend.sessions.tahap3.magang', compact('sesi', 'peserta', 'kt_magang'));
    }

    public function detailProject($oprec_id, $peserta_id)
    {
        $sesi = Oprec::where('id', $oprec_id)->get()->first();
        $peserta = Status::where('oprec_id', '=', $oprec_id)
            ->where('peserta_id', '=', $peserta_id)
            ->get();
        $kt_project = $sesi->project;
        return view('backend.sessions.tahap3.project', compact('sesi', 'peserta', 'kt_project'));
    }

    public function updateKelulusan(Request $request)
    {
        $statusKelulusan = $request->status_kelulusan;
        $oprec_id = $request->oprec_id;
        $ids = $request->id;
        $message = 'Successfully update data!';
        $statusMessage = "success";
        if ($ids != null) {
            DB::table('status')
                ->whereIn('id', $ids)
                ->where('oprec_id', '=', $oprec_id)
                ->update(['status_tahap3' => $statusKelulusan]);
        } else {
            $message = 'Failed update data nothing checked!';
            $statusMessage = "failed";
        }
        return redirect()->back()->with($statusMessage, $message);
    }

    public function addKriteriaWawancara2(Request $request)
    {
        $request->validate(
            [
                'oprec_id' => 'required|string',
                'name' => 'required',
                'prosentase' => 'required|numeric'
            ],
            [
                'oprec_id.required' => 'id oprec must be required',
                'name.required' => 'nama kriteria must be required',
                'prosentase.required' => 'prosentase kriteria must be required',
                'prosentase.numeric' => 'prosentase kriteria must be number',
            ]
        );
        $oprecId = $request->oprec_id;
        $namaKriteria = $request->name;
        $prosentase = $request->prosentase;
        $wawancara2 = new Wawancara2();
        $wawancara2->nama_kriteria = $namaKriteria;
        $wawancara2->prosentase = $prosentase;
        $wawancara2->oprec_id = $oprecId;
        $wawancara2->save();
        return redirect()->back()->with('success', 'Successfully added kriteria wawancara 2!');
    }

    public function addKriteriaMagang(Request $request)
    {
        $request->validate(
            [
                'oprec_id' => 'required|string',
                'name' => 'required',
                'prosentase' => 'required|numeric'
            ],
            [
                'oprec_id.required' => 'id oprec must be required',
                'name.required' => 'nama kriteria must be required',
                'prosentase.required' => 'prosentase kriteria must be required',
                'prosentase.numeric' => 'prosentase kriteria must be number',
            ]
        );
        $oprecId = $request->oprec_id;
        $namaKriteria = $request->name;
        $prosentase = $request->prosentase;
        $magang = new Magang();
        $magang->nama_kriteria = $namaKriteria;
        $magang->prosentase = $prosentase;
        $magang->oprec_id = $oprecId;
        $magang->save();
        return redirect()->back()->with('success', 'Successfully added kriteria magang!');
    }

    public function addKriteriaProject(Request $request)
    {
        $request->validate(
            [
                'oprec_id' => 'required|string',
                'name' => 'required',
                'prosentase' => 'required|numeric'
            ],
            [
                'oprec_id.required' => 'id oprec must be required',
                'name.required' => 'nama kriteria must be required',
                'prosentase.required' => 'prosentase kriteria must be required',
                'prosentase.numeric' => 'prosentase kriteria must be number',
            ]
        );
        $oprecId = $request->oprec_id;
        $namaKriteria = $request->name;
        $prosentase = $request->prosentase;
        $project = new Project();
        $project->nama_kriteria = $namaKriteria;
        $project->prosentase = $prosentase;
        $project->oprec_id = $oprecId;
        $project->save();
        return redirect()->back()->with('success', 'Successfully added kriteria project!');
    }

    public function updateNilaiTotal($oprec_id)
    {
        $updatePeserta = Tahap3::where('oprec_id', '=', $oprec_id)->get();
        $kriteria = TotalTahap3::where('oprec_id', '=', $oprec_id)->get()->first();
        foreach ($updatePeserta as $peserta) {
            if (isset($peserta->score_total_wawancara_2) && isset($peserta->score_total_magang) && isset($peserta->score_total_project)) {
                $scoreWawancara = $kriteria->prosentase_wawancara_2 / 100 * $peserta->score_total_wawancara_2;
                $scoreMagang =  $kriteria->prosentase_magang / 100 * $peserta->score_total_magang;
                $scoreProject =  $kriteria->prosentase_project / 100 * $peserta->score_total_project;
                $peserta->score_total_tahap_3 = $scoreWawancara + $scoreMagang + $scoreProject;
                $peserta->save();
            }
        }
    }

    public function deleteKriteriaWawancara2(Request $request)
    {
        $destroy = Wawancara2::findOrFail($request->id);
        $destroy->delete();
        return redirect()->back()->with('success', 'Successfully delete data !');
    }


    public function deleteKriteriaMagang(Request $request)
    {
        $destroy = Magang::findOrFail($request->id);
        $destroy->delete();
        return redirect()->back()->with('success', 'Successfully delete data !');
    }

    public function deleteKriteriaProject(Request $request)
    {
        $destroy = Project::findOrFail($request->id);
        $destroy->delete();
        return redirect()->back()->with('success', 'Successfully delete data !');
    }


    public function setTotalTahap3(Request $request)
    {
        $oprec_id = $request->oprec_id;
        $total = TotalTahap3::where('oprec_id', '=', $oprec_id)->get()->first();
        if ($total == NULL) {
            $totalTahap3 = new TotalTahap3();
            $totalTahap3->prosentase_wawancara_2 = $request->wawancara2;
            $totalTahap3->prosentase_magang = $request->magang;
            $totalTahap3->prosentase_project = $request->project;
            $totalTahap3->oprec_id = $oprec_id;
            $totalTahap3->save();
        } else {
            $total->prosentase_wawancara_2 = $request->wawancara2;
            $total->prosentase_magang = $request->magang;
            $total->prosentase_project = $request->project;
            $total->save();
        }
        $this->updateNilaiTotal($oprec_id);
        return redirect()->back()->with('success', 'Successfully update persentase tahap 3 !');
    }

    public function updateNilaiWawancara2(Request $request)
    {
        $oprec_id = $request->oprec_id;
        $peserta_id = $request->peserta_id;
        $data_peserta = Tahap3::firstOrNew([
            'peserta_id' => $peserta_id,
            'oprec_id' => $oprec_id
        ]);
        $total_nilai = 0;
        foreach (array_combine($request->id_kt_wawancara2, $request->kt_wawancara2) as $id => $nilai) {
            print($id . ":" . $nilai . "\n");
            $data_nilai = NilaiWawancara2::firstOrNew([
                'wawancara2_id' => $id,
                'peserta_id' => $peserta_id
            ]);
            $data_nilai->wawancara2_id = $id;
            $data_nilai->nilai = $nilai;
            $data_nilai->peserta_id = $peserta_id;
            $data_nilai->save();
            $kriteria = Wawancara2::where('id', '=', $id)->get()->first();
            $persentase = $kriteria->prosentase;
            $total_nilai += ($persentase / 100) * $nilai;
        }
        $data_peserta->catatan_wawancara_2 = $request->catatan_wawancara;
        $data_peserta->oprec_id = $oprec_id;
        $data_peserta->peserta_id = $peserta_id;
        $data_peserta->score_total_wawancara_2 = $total_nilai;
        $data_peserta->save();
        $this->updateNilaiTotal($oprec_id);
        return redirect()->back()->with('success', 'Successfully update nilai peserta !');
    }


    public function updateNilaiMagang(Request $request)
    {
        $oprec_id = $request->oprec_id;
        $peserta_id = $request->peserta_id;
        $data_peserta = Tahap3::firstOrNew([
            'peserta_id' => $peserta_id,
            'oprec_id' => $oprec_id
        ]);
        $total_nilai = 0;
        foreach (array_combine($request->id_kt_magang, $request->kt_magang) as $id => $nilai) {
            print($id . ":" . $nilai . "\n");
            $data_nilai = NilaiMagang::firstOrNew([
                'magang_id' => $id,
                'peserta_id' => $peserta_id
            ]);
            $data_nilai->magang_id = $id;
            $data_nilai->nilai = $nilai;
            $data_nilai->peserta_id = $peserta_id;
            $data_nilai->save();
            $kriteria = Magang::where('id', '=', $id)->get()->first();
            $persentase = $kriteria->prosentase;
            $total_nilai += ($persentase / 100) * $nilai;
        }
        $data_peserta->catatan_magang = $request->catatan_magang;
        $data_peserta->oprec_id = $oprec_id;
        $data_peserta->peserta_id = $peserta_id;
        $data_peserta->score_total_magang = $total_nilai;
        $data_peserta->save();
        $this->updateNilaiTotal($oprec_id);
        return redirect()->back()->with('success', 'Successfully update nilai peserta !');
    }

    public function updateNilaiProject(Request $request)
    {
        $oprec_id = $request->oprec_id;
        $peserta_id = $request->peserta_id;
        $data_peserta = Tahap3::firstOrNew([
            'peserta_id' => $peserta_id,
            'oprec_id' => $oprec_id
        ]);
        $total_nilai = 0;
        foreach (array_combine($request->id_kt_project, $request->kt_project) as $id => $nilai) {
            print($id . ":" . $nilai . "\n");
            $data_nilai = NilaiProject::firstOrNew([
                'project_id' => $id,
                'peserta_id' => $peserta_id
            ]);
            $data_nilai->project_id = $id;
            $data_nilai->nilai = $nilai;
            $data_nilai->peserta_id = $peserta_id;
            $data_nilai->save();
            $kriteria = Project::where('id', '=', $id)->get()->first();
            $persentase = $kriteria->prosentase;
            $total_nilai += ($persentase / 100) * $nilai;
        }
        $data_peserta->catatan_project = $request->catatan_project;
        $data_peserta->oprec_id = $oprec_id;
        $data_peserta->peserta_id = $peserta_id;
        $data_peserta->score_total_project = $total_nilai;
        $data_peserta->save();
        $this->updateNilaiTotal($oprec_id);
        return redirect()->back()->with('success', 'Successfully update nilai peserta !');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
