<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Oprec;
use Auth;

class KelolaSesiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $admin_id = Auth::id();
        $sessions = Oprec::all();
        return view('backend.sessions.index', compact('sessions', 'admin_id'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_oprec' => 'required',
            'status_dibuka_oprec' => 'required'
        ],[
            'nama_oprec.required' => 'Name of session must be required'
        ]);

        $new = new Oprec();
        $new->fill($request->all());
        $new->admin_id = Auth::id();
        $new->save();
        return redirect()->route('sesi.index')->with('success', 'Successfully created session !');
    }

    public function detail($id)
    {
        $detail = Oprec::where('id', $id)->get()->first();
        return $detail;
    }

    public function update(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'status_dibuka_oprec' => 'required'
            ],
            [
                'name.required' => 'Name of session must be required',
            ]
        );

        $update = Oprec::findOrFail($request->id);
        $update->nama_oprec = $request->name;
        $update->status_dibuka_oprec = $request->status_dibuka_oprec;
        $update->save();

        return redirect('adm1n/sesi')->with('success', 'Successfully edit session !');
    }

    public function delete(Request $request)
    {
        $destroy = Oprec::findOrFail($request->id);
        $destroy->delete();

        return redirect('adm1n/sesi')->with('success', 'Successfully deleted session !');
    }

    public function addPendaftaran($id)
    {
        $update = Oprec::findOrFail($id);
        if ($update->status_dibuka_oprec == "dibuka") {
            if ($update->tgl_pendaftaran) {
                return redirect('adm1n/sesi/timeline/' . $update->id)->with('failed', 'Pendaftaran already exist!');
            }
            $update->status_pendaftaran = "dibuka";
            $update->tgl_pendaftaran = date('Y-m-d H:i:s');
            $update->save();
            return redirect('adm1n/sesi/timeline/' . $update->id)->with('success', 'Successfully add pendaftaran !');
        }
        return redirect('adm1n/sesi/timeline/' . $update->id)->with('failed', 'Failed open session first !');
    }

    public function updateTahap1(Request $request, $id)
    {
        $update = Oprec::findOrFail($id);

        $update->status_tahap1 = $request->status_tahap1;

        $update->save();

        return redirect()->route('timeline.index', $id)->with('success', 'Successfully edit status Tahap 1 !');
    }
}
