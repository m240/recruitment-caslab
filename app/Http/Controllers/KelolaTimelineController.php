<?php

namespace App\Http\Controllers;

use App\Oprec;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Tahap1;
use App\peserta;
//dika

use App\Status;

//dika
class KelolaTimelineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($id)
    {
        //dika
        $sesi = Oprec::where('id', $id)->get()->first();
        $tampil = status::all()->where('oprec_id',$id);
        $male = 0;
        $female = 0;
        $total = 0;
        foreach($tampil as $peserta){
            if($peserta->peserta->gender==1){
                $male++;
            }else{
                $female++;
            }
            $total++;
        }
        $gender=[$female,$male];
        //dika

        return view('backend.sessions.timeline', compact('sesi','tampil','gender','total'));
    }

    public function store_tahap1($id)
    {
        $pendaftaran = Oprec::findOrFail($id);
        $tahap1 = Tahap1::where('sesi_oprec_id', $id)->count();

        if ($tahap1->tgl_pendaftaran != NULL) {
            $store = new Tahap1();
            $store->sesi_oprec_id = $id;
            $store->save();
            return redirect()->route('timeline.index', $id)->with('success', 'Successfully created tahap 1 !');
        } else {
            return redirect()->route('timeline.index', $id)->with('failed', 'Tahap 1 already exist !');
        }
    }

        public function detail($id)
    {
        $status = Status::findOrfail($id);
        return $status;
    }

    public function update(Request $request)
    {
        $status = Status::findOrfail($request->id);
        $status->status_pendaftaran = $request->status_pendaftaran;
        $status->status_tahap1 = $request->status_tahap1;
        $status->status_tahap2 = $request->status_tahap2;
        $status->status_tahap3 = $request->status_tahap3;
        $status->save();
        return redirect()->route('timeline.index', $status->oprec_id)->with('success', 'Successfully update status');
    }
}
