<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
class AdminLoginController extends Controller
{
  public function __construct()
  {
    $this->middleware('guest:admin', ['except' => ['logout']]);
  }

  public function showLoginForm()
  {
    return view('auth.admin-login');
  }

  public function login(Request $request)
  {
    // Validate the form data
    $this->validate($request, [
      'username'   => 'required',
      'password' => 'required|min:6'
    ]);
    
    $password = $request->password;
    $passwordHashed = sha1(md5($password."asjkdfh")."shsadfgkj");
    // Attempt to log the user in
    if (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $passwordHashed], $request->remember)) {
      // if successful, then redirect to their intended location
      return redirect()->route('admin.dashboard');
    }
    
    // if unsuccessful, then redirect back to the login with the form data
    Session::flash('message', "Username atau password salah");
    return redirect()->back()->withInput($request->only('username', 'remember'));
  }

  public function logout()
  {
    Auth::guard('admin')->logout();
    return redirect()->route('admin.login');
  }
}
