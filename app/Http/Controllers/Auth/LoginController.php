<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/peserta/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:web')->except('logout');
    }

    public function ShowLoginForm()
    {
      if(!Session::has('users')){
        return view('auth.peserta-login');
      }else{
        return redirect('/peserta/dashboard');
      }
    }

    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required|size:15',
            'password' => 'required',
        ]);
        $username = $request->username;
        $password = $request->password;
        $users = User::where('user_name', $username)->first();
        try {
            $user = $users->user_name;
        } catch (\Throwable $th) {
            return redirect('/');
        }
        $pass = sha1(md5($password."asjkdfh")."shsadfgkj");
		if($username === $user && $pass === $users->password){
            $this->clearLoginAttempts($request);
            $request->session()->put('users', $users);
            return redirect('peserta/dashboard');
        }else{
            $this->incrementLoginAttempts($request);
            return redirect('/')->with('failed',' Username atau password salah !');
        }
    }

    public function logout(){
        Session::flush();
    	return redirect('/');
    }
}
