<?php

namespace App\Http\Controllers\Auth;

use App\Peserta;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Session;

class RegisterPesertaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:panitia');
    }
    public function index()
    {
        return view('auth.register-peserta');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'nim' => 'required|numeric|min:15|unique:pesertas',
            'password' => 'required|min:6|confirmed',
            'email' => 'required|email|max:255|unique:pesertas',
            'motivasi' => 'required|max:255',
            'ide_kreatif' => 'required|max:255',
            'address' => 'required|max:255',
            'tempat_lahir' => 'required|min:4',
            'tanggal_lahir' => 'required',            
        ]);

        
    }

    protected function create(array $data)
    {
        return Peserta::create([
            'name' => $data['name'],
            'nim' => $data['nim'],
            'password' => bcrypt($data['password']),
            'email' => $data['email'],
            'motivasi' => $data['motivasi'],
            'ide_kreatif' => $data['ide_kreatif'],
            'address' => $data['address'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
        ]);
        $id = $this->create($data)->id;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $this->create($request->all());
        Session::flash('message', "Sukses registrasi peserta");
        return redirect()->back();
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }
    
    // public function list()
    // {
    //     //
        
    //     //::all();
    //     // return view ('auth.cek-peserta', ['auth' => $pesertas]);
    // }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
