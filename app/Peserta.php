<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    protected $connection = 'mysql';
    public $table = 'peserta';
    protected $fillable = [
        'nama',
        'nim',
        'angkatan',
        'email',
        'gender',
        'phone',
        'motivasi',
        'lampiran',
    ];

    public function skills()
    {
        return $this->belongsToMany(Skill::class, 'skill_peserta');
    }

    public function sesi_oprecs()
    {
        return $this->belongsToMany('Peserta', 'intersection', 'id_peserta', 'id_sesi')
            ->withTimeStamps();
    }

    public function tahap2()
    {
        return $this->hasMany('App\Tahap2');
    }

    public function tahap1()
    {
        return $this->hasMany('App\Tahap1');
    }
    public function nilaiWawancara1()
    {
        return $this->hasMany('App\NilaiWawancara1', 'id');
    }
    public function nilaiMicroteaching()
    {
        return $this->hasMany('App\NilaiMicroteaching', 'id');
    }

    // Tahap 3

    public function tahap3()
    {
        return $this->hasMany('App\Tahap3');
    }
    public function scoreWawancara2()
    {
        return $this->hasMany('App\ScoreWawancara2');
    }
    public function scoreProject()
    {
        return $this->hasMany('App\ScoreProject');
    }
    public function scoreMagang()
    {
        return $this->hasMany('App\ScoreMagang');
    }
    public function status()
    {
        return $this->hasMany('App\Status');
    }
}
