<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalTahap3 extends Model
{
    //
    protected $table = "kriteria_total_tahap_3";
    protected $fillable = [
        "prosentase_wawancara_2", "prosentase_magang", "prosentase_project", "oprec_id"
    ];
    public function oprec()
    {
        return $this->belongsTo('App\Oprec', 'oprec_id');
    }
    public $timestamps = false;
}
