<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengumumanTahap3 extends Model
{
    protected $table = "pengumumantahap3";
    protected $fillable = [
        "oprec_id",
        "namapengumumantahap3",
    ];


    public function oprec()
    {
        return $this->belongsTo('App\Oprec', 'oprec_id');
    }
}
