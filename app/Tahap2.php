<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahap2 extends Model
{
    //
    protected $table = "tahap_2";
    protected $fillable = [
        "wawancara_1", "microteaching",
        "tema", "oprec_id", "score_total_microteaching",
        "score_total_wawancara_1", "peserta_id", "score_total_tahap_2"
    ];
    public function peserta()
    {
        return $this->belongsTo('App\Peserta', 'peserta_id');
    }
}
