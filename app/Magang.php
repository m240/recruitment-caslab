<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Magang extends Model
{
    //
    protected $table = "kriteria_magang";
    protected $fillable = [
        "nama_kriteria", "prosentase", "oprec_id"
    ];
    public function oprec()
    {
        return $this->belongsTo('App\Oprec', 'oprec_id');
    }
    public function nilaiMagang()
    {
        return $this->hasMany('App\NilaiMagang');
    }
    public $timestamps = false;
}
