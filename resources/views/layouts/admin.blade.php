<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- start: HEAD -->

<head>
	<title>SIRENTA - Sistem Recruitment Asisten Lab.</title>
	<!-- start: META -->
	<meta charset="utf-8" />
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
	<meta name="viewport"
		content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- end: META -->
	<!-- start: MAIN CSS -->
	<link href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('fonts/style.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/main-responsive.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('plugins/iCheck/skins/all.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css')}}" rel="stylesheet"
		type="text/css">
	<link href="{{asset('plugins/perfect-scrollbar/src/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/theme_light.css')}}" rel="stylesheet" type="text/css" id="skin_color">
	<link href="{{asset('css/print.css')}}" rel="stylesheet" type="text/css" media="print" />
	<!--[if IE 7]>
		<link rel="stylesheet" href="ssets/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
	<!-- end: MAIN CSS -->
	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	@yield('css')
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	<link rel="shortcut icon" href="{{asset('favicon.ico')}}" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->

<body>
	<!-- start: HEADER -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<!-- start: TOP NAVIGATION CONTAINER -->
		<div class="container">
			<div class="navbar-header">
				<!-- start: RESPONSIVE MENU TOGGLER -->
				<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
					<span class="clip-list-2"></span>
				</button>
				<!-- end: RESPONSIVE MENU TOGGLER -->
				<!-- start: LOGO -->
				<a class="navbar-brand" href="{{route('admin.dashboard')}}">
					<img src="{{asset('images/logo-sirenta.png')}}" alt="logo" height="25px"> Sistem Recruitment Asisten
					Lab.
				</a>
				<!-- end: LOGO -->
			</div>
			<div class="navbar-tools">
				<!-- start: TOP NAVIGATION MENU -->
				<ul class="nav navbar-right">
					<!-- start: USER DROPDOWN -->
					<li class="dropdown current-user">
						<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true"
							href="#">
							<i class="clip-user-5"></i>
							<span class="username">{{ Auth::user()->name }}</span>
							<i class="clip-chevron-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="{{route('admin.logout')}}">
									<i class="clip-exit"></i>
									&nbsp;Log Out
								</a>
							</li>
						</ul>
					</li>
					<!-- end: USER DROPDOWN -->
				</ul>
				<!-- end: TOP NAVIGATION MENU -->
			</div>
		</div>
		<!-- end: TOP NAVIGATION CONTAINER -->
	</div>
	<!-- end: HEADER -->
	<!-- start: MAIN CONTAINER -->
	<div class="main-container">
		<div class="navbar-content">
			<!-- start: SIDEBAR -->
			<div class="main-navigation navbar-collapse collapse">
				<!-- start: MAIN MENU TOGGLER BUTTON -->
				<div class="navigation-toggler">
					<i class="clip-chevron-left"></i>
					<i class="clip-chevron-right"></i>
				</div>
				<!-- end: MAIN MENU TOGGLER BUTTON -->
				<!-- start: MAIN NAVIGATION MENU -->
				<ul class="main-navigation-menu">
					<li class="{{request()->is('adm1n/index') ? 'active open' : ''}}">
						<a href="{{route('admin.dashboard')}}"><i class="clip-home-3"></i>
							<span class="title"> Dashboard ADMIN</span><span class="selected"></span>
						</a>
					</li>
					<li class="{{request()->is('adm1n/manage-panitia') ? 'active open' : ''}}">
						<a href="{{route('manage-panitia.index')}}"><i class="clip-pencil"></i>
							<span class="title"> Kelola Panitia</span><span class="selected"></span>
						</a>
					</li>
					<li class="{{request()->is('adm1n/sesi/*') || request()->is('adm1n/sesi') ? 'active open' : ''}}">
						<a href="{{route('sesi.index')}}"><i class="clip-clock"></i>
							<span class="title"> Kelola Sesi</span><span class="selected"></span>
						</a>
					</li>
					<li class="{{request()->is('adm1n/peserta') ? 'active open' : ''}}">
						<a href="{{route('kelolapeserta.index')}}"><i class="fa fa-group"></i>
							<span class="title"> Kelola Peserta</span><span class="selected"></span>
						</a>
					</li>
				</ul>
					<!-- end: MAIN NAVIGATION MENU -->
			</div>
			<!-- end: SIDEBAR -->
		</div>

		<!-- start: PAGE -->
		<div class="main-content">
			<!-- start: PANEL CONFIGURATION MODAL FORM -->
			<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
				nama<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								&times;
							</button>
							<h4 class="modal-title">Panel Configuration</h4>
						</div>
						<div class="modal-body">
							Here will be a configuration form
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">
								Close
							</button>
							<button type="button" class="btn btn-primary">
								Save changes
							</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- end: SPANEL CONFIGURATION MODAL FORM -->
			<div class="container">
				<!-- start: PAGE HEADER -->
				<div class="row">
					<div class="col-sm-12">
						<!-- start: PAGE TITLE & BREADCRUMB -->
						<ol class="breadcrumb">
							<?php $segments = ''; ?>
							@foreach(Request::segments() as $segment)

							<?php $segments .= '/'.$segment; ?>
							@if($loop->last)
							<li class="active">
								{{ucwords(str_replace("_", " ", $segment))}}
							</li>
							@else
							<li>
								@if(is_numeric($segment))
								@yield('id_name')
								@else
								<a href="{{ $segments }}">{{ucwords(str_replace("_", " ", $segment))}}</a>
								@endif
							</li>
							@endif
							@endforeach
						</ol>
						<div class="page-header">
							<h1>@yield('title') <small>@yield('description')</small></h1>
						</div>
						<!-- end: PAGE TITLE & BREADCRUMB -->
					</div>
				</div>
				<!-- end: PAGE HEADER -->
				<!-- start: PAGE CONTENT -->
				@yield('content')
				<!-- end: PAGE CONTENT-->
			</div>
		</div>
		<!-- end: PAGE -->

</div>

	<!-- end: MAIN CONTAINER -->
	<!-- start: FOOTER -->
	<div class="footer clearfix">
		<div class="footer-inner">
			&copy; Copyright 2019.
			Made with <span style="color: #e25555;">&#9829;</span> by Tim Riset Infotech UMM
		</div>
		<div class="footer-items">
			<span class="go-top"><i class="clip-chevron-up"></i></span>
		</div>
	</div>
	<!-- end: FOOTER -->
	<div id="event-management" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
					<h4 class="modal-title">Event Management</h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn btn-light-grey">
						Close
					</button>
					<button type="button" class="btn btn-danger remove-event no-display">
						<i class='fa fa-trash-o'></i> Delete Event
					</button>
					<button type='submit' class='btn btn-success save-event'>
						<i class='fa fa-check'></i> Save
					</button>
				</div>
			</div>
		</div>
	</div>
	<!-- start: MAIN JAVASCRIPTS -->
	<!--[if lt IE 9]>
		<script src="plugins/respond.min.js"></script>
		<script src="plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<![endif]-->
	<!--[if gte IE 9]><!-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<!--<![endif]-->
	<script src="{{asset('plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript">
	</script>
	<script src="{{asset('plugins/blockUI/jquery.blockUI.js')}}" type="text/javascript"></script>
	<script src="{{asset('plugins/iCheck/jquery.icheck.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('plugins/perfect-scrollbar/src/jquery.mousewheel.js')}}" type="text/javascript"></script>
	<script src="{{asset('plugins/perfect-scrollbar/src/perfect-scrollbar.js')}}" type="text/javascript"></script>
	<script src="{{asset('plugins/less/less-1.5.0.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('plugins/jquery-cookie/jquery.cookie.js')}}" type="text/javascript"></script>
	<script src="{{asset('plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js')}}" type="text/javascript">
	</script>
	<script src="{{asset('js/main.js')}}" type="text/javascript"></script>
	<!-- end: MAIN JAVASCRIPTS -->
	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	@yield('js')
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
		jQuery(document).ready(function () {
			Main.init();
			// Index.init();
		});
	</script>
</body>
<!-- end: BODY -->

</html>