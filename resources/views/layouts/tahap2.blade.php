<div class="container profile-page">
    <br>
    <button href="#pengumuman_tahap2_modal" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i> Add Pengumuman Tahap 2</button>
    <table class="table table-striped table-bordered table-full-width">
        <thead>
            <tr>
                <th style="width: 10%">
                    Kriteria Wawancara 1:
                </th>
                <th style="width: 60%">
                    <div class="panel-body">
                        @foreach($sesi->wawancara1 as $wawancara1)
                        <a onclick="hapusWawancara1('{{$wawancara1->id}}','{{$wawancara1->nama_kriteria}}')" style="margin: 3px" class="btn btn-primary" role="button">
                            {{$wawancara1->nama_kriteria}} : {{$wawancara1->prosentase}} %
                        </a>
                        @endforeach
                    </div>
                </th style="width: 30%">
                <th>
                    <button href="#wawancara_modal" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i> Add Kriteria Wawancara</button>
                </th>
            </tr>
        </thead>
    </table>
    <table class="table table-striped table-bordered table-full-width">
        <thead>
            <tr>
                <th style="width: 10%">
                    Kriteria Microteaching:
                </th>
                <th style="width: 60%">
                    @foreach($sesi->microteaching as $microteaching)
                    <a onclick="hapusMicroteaching('{{$microteaching->id}}','{{$microteaching->nama_kriteria}}')" style="margin: 3px" class="btn btn-purple" role="button">
                        {{$microteaching->nama_kriteria}} : {{$microteaching->prosentase}} %
                    </a>
                    @endforeach
                </th>
                <th style="width: 30%">
                    <button href="#microteaching_modal" data-toggle="modal" class="btn btn-purple"><i class="fa fa-plus"></i> Add Kriteria Microteaching</button>
                </th>
            </tr>
        </thead>
    </table>
    <table class="table table-striped table-bordered table-full-width">
        <form action="{{route('tahap2.set.total.tahap2')}}" method="POST">
            @csrf
            <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
            <thead>
                <tr>
                    <th style="width: 10%">
                        Kriteria Total Tahap 2:
                    </th>
                    <th style="width: 30%">
                        Persentase Wawancara 1 :
                        @if(isset($sesi->totalTahap2[0]))
                        <input name="wawancara1" value="{{$sesi->totalTahap2[0]->prosentase_wawancara_1}}" type="number" min="0" placeholder="persentase % nilai" id="form-field-18" class="form-control">
                        @else
                        <input name="wawancara1" type="number" min="0" placeholder="nilai belum di set" id="form-field-18" class="form-control">
                        @endif
                    </th>
                    <th style="width: 30%">
                        Persentase Microteaching
                        @if(isset($sesi->totalTahap2[0]))
                        <input name="microteaching" value="{{$sesi->totalTahap2[0]->prosentase_microteaching}}" type="number" min="0" placeholder="persentase % nilai" id="form-field-18" class="form-control">
                        @else
                        <input name="microteaching" type="number" min="0" placeholder="nilai belum di set" id="form-field-18" class="form-control">
                        @endif
                    </th>
                    <th style="width: 30%">
                        <button class="btn btn-info"><i class="clip-settings"></i> Set Kriteria Tahap 2</button>
                    </th>
                </tr>
            </thead>
        </form>
    </table>