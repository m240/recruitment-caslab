<!DOCTYPE html>
<!-- Template Name: Clip-One - Frontend | Build with Twitter Bootstrap 3 | Version: 1.0 | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->

<head>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>SIRENTA - Sistem Recruitment Asisten Lab.</title>
	<!-- start: META -->
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->

	<meta charset="utf-8" />
	<meta name="viewport"
		content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta content="" name="description" />
	<meta content="" name="author" />

	<!-- end: META -->
	<!-- start: MAIN CSS -->
	<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/fonts/style.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/animate.css/animate.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/main-responsive.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/theme_blue.css')}}" type="text/css" id="skin_color">


	<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
	<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet"
		type="text/css" />
	<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('plugins/iCheck/skins/all.css')}}">
	<!-- end: MAIN CSS -->
	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	<link rel="stylesheet" href="{{asset('assets/plugins/revolution_slider/rs-plugin/css/settings.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/flex-slider/flexslider.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/colorbox/example2/colorbox.css')}}">
	<link rel="stylesheet" href="{{asset('plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}">
	<link rel="icon" href="{{asset('images/logo-sirenta.jpg')}}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	<!-- start: HTML5SHIV FOR IE8 -->
	<!--[if lt IE 9]>
		<script src="{{asset('assets/plugins/html5shiv.js')}}"></script>
	<![endif]-->
	<!-- end: HTML5SHIV FOR IE8 -->
	@yield('css')
</head>
<!-- end: HEAD -->

<body>
	<!-- start: HEADER -->
	<header>
		<!-- start: SLIDING BAR (SB) -->
		<div id="slidingbar-area">
			<div id="slidingbar">
				<!-- start: SLIDING BAR THIRD COLUMN -->
				<div class="col-md-4 col-sm-4">
					<h2>Contact Us</h2>
					<address class="margin-bottom-40">
						Laboratorium Informatika UMM
						<br>
						Jl. Raya Tlogomas No.246,
						Jawa Timur 65144,
						Indonesia
						<br>
						Telpon: (0341) 464318, ext 252
						<br>
						<br>
						WA/Telegram (Quick Response): +62 896-896-01317
						<br>
						Email:
						<a href="mailto:lab.informatika@umm.ac.id ">
							lab.informatika@umm.ac.id
						</a>
					</address>
				</div>
				<!-- end: SLIDING BAR THIRD COLUMN -->
			</div>
			<!-- start: SLIDING BAR TOGGLE BUTTON -->
			<a href="#" class="sb_toggle">
			</a>
			<!-- end: SLIDING BAR TOGGLE BUTTON -->
		</div>
		<!-- end: SLIDING BAR -->
		<!-- start: TOP BAR -->
		<div class="clearfix " id="topbar">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<!-- start: TOP BAR CALL US -->
						<div class="callus">
							Call Us: SMS/WA/Telegram (Quick Response) +62 896-896-01317 - (0341)464318, ext 252
						</div>
						<!-- end: TOP BAR CALL US -->
					</div>
				</div>
			</div>
		</div>
		</div>
		<!-- end: TOP BAR -->
		<div role="navigation" class="navbar navbar-default navbar-fixed-top space-top">
			<!-- start: TOP NAVIGATION CONTAINER -->
			<div class="container">
				<div class="navbar-header">
					<!-- start: RESPONSIVE MENU TOGGLER -->
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- end: RESPONSIVE MENU TOGGLER -->
					<!-- start: LOGO -->
					<a class="navbar-brand navbar-expand-lg" href="{{route('peserta.index')}}">
						<img src="{{asset('images/logo-sirenta.png')}}" alt="logo" height="57px">
					</a>
					<!-- end: LOGO -->
				</div>
				<div class="navbar-tools">
					<ul class="nav navbar-nav navbar-right">
						<li class="{{request()->is('peserta/dashboard') ? 'active open' : ''}}">
							<a href="{{route('peserta.index')}}"><i class="clip-home-3"></i>
								<span class="title">Home</span><span class="selected"></span>
							</a>
						</li>
						<li
							class="{{request()->is('peserta/daftar') ? 'active open' : ''}}{{request()->is('peserta/kelola-data') ? 'active open' : ''}}">
							<a href="{{route('peserta.daftar')}}"><i class="clip-pencil"></i>
								<span class="title">Kelola Data</span><span class="selected"></span>
							</a>
						</li>
						<!-- class="{{request()->is('peserta/daftar') ? 'active open' : ''}}{{request()->is('peserta/kelola-data') ? 'active open' : ''}}" -->
						<li class="{{request()->is('peserta/progres') ? 'active open' : ''}}">
							<a href="{{route('peserta.progres')}}"><i class="clip-stats"></i>
								<span class="title">Progres</span><span class="selected"></span>
							</a>
						</li>
						<li class="dropdown">
							<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle"
								data-close-others="false" href="#">
								<img src="https://krs.umm.ac.id/Poto/{{substr(Session::get('users')->user_name,0,4)}}/{{Session::get('users')->user_name}}.JPG"
									class="circle-img" alt="" style="height: 40px">
								<i class="clip-chevron-down"></i>
							</a>
							<ul class="dropdown-menu"
								style="width: 200px!important; right: 0px;!important; left: auto!important;">
								<li>
									<a href="#">
										<i class="clip-user-2"></i>
										&nbsp;{{Session::get('users')->full_name}}
									</a>
								</li>
								<li>
									<a href="#">
										<i class="clip-barcode"></i>
										&nbsp;{{Session::get('users')->user_name}}
									</a>
								</li>
								<li>
									<a href="{{route('logout')}}">
										<i class="clip-exit"></i>
										&nbsp;Log Out
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<!-- end: TOP NAVIGATION CONTAINER -->
		</div>
	</header>

	@yield('content')

	<!-- start: FOOTER -->
	<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="newsletter">
						<style>
							.support-logo {
								padding: 3px;
								border-radius: 5px;
								display: inline-block;
								margin-right: 5px
							}
						</style>
						<h4>Supported by :</h4>
						<div class="center support-logo">
							<img src="{{asset('images/logo-sirenta.png')}}" height="50px">
						</div>
						<div class="center support-logo">
							<img src="https://infotech.umm.ac.id/assets/img/logo_lab_it.png" height="50px">
						</div>
						<div class="center support-logo">
							<img src="https://infotech.umm.ac.id/assets/img/logo_umm.png" height="50px">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="contact-details">
						<h4>Contact Us</h4>
						<ul class="contact">
							<li>
								<p>
									<i class="fa fa-map-marker"></i><strong>Address:</strong> Jl. Raya Tlogomas No.246,
									Jawa Timur 65144,
									Indonesia
								</p>
							</li>
							<li>
								<p>
									<i class="fa fa-phone"></i><strong>Phone:</strong> (0341) 464318, ext 252 -
									SMS/WA/Telegram (Quick Response) +62 896-896-01317
								</p>
							</li>
							<li>
								<p>
									<i class="fa fa-envelope"></i><strong>Email 1 :</strong>
									<a href="mailto:infotechumm@umm.ac.id">
										lab.informatika@umm.ac.id
									</a>
								</p>
							</li>
							<li>
								<p>
									<i class="fa fa-envelope"></i><strong>Email 2 :</strong>
									<a href="mailto:lab.informatika.umm@gmail.com">
										lab.informatika.umm@gmail.com
									</a>
								</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-2">
					<h4>Follow Us</h4>
					<div class="social-icons">
						<ul>
							<li class="social-twitter tooltips" data-original-title="Twitter" data-placement="bottom">
								<a target="_blank" href="https://twitter.com/infotechUMM">
									Twitter
								</a>
							</li>
							<li class="social-facebook tooltips" data-original-title="Facebook" data-placement="bottom">
								<a target="_blank" href="https://www.facebook.com/infotechUMM/"
									data-original-title="Facebook">
									Facebook
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-copyright">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<p style="color: #FFFFFF !important">
							&copy; Copyright 2019.
							Made with <span style="color: #e25555;">&#9829;</span> by Tim Riset Infotech UMM
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<a id="scroll-top" href="#"><i class="fa fa-angle-up"></i></a>
	<!-- end: FOOTER -->

	<!-- start: MAIN JAVASCRIPTS -->
	<!--[if lt IE 9]>
		<script src="{{asset('assets/plugins/respond.min.js')}}"></script>
		<script src="{{asset('assets/plugins/excanvas.min.js')}}"></script>
		<script src="{{asset('assets/plugins/html5shiv.js')}}"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<![endif]-->
	<!--[if gte IE 9]><!-->

	<!--<![endif]-->
	<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/plugins/jquery.transit/jquery.transit.js')}}"></script>
	<script src="{{asset('assets/plugins/hover-dropdown/twitter-bootstrap-hover-dropdown.min.js')}}"></script>
	<script src="{{asset('assets/plugins/jquery.appear/jquery.appear.js')}}"></script>
	<script src="{{asset('assets/plugins/blockUI/jquery.blockUI.js')}}"></script>
	<script src="{{asset('assets/plugins/jquery-cookie/jquery.cookie.js')}}"></script>
	<script src="{{asset('assets/js/main.js')}}"></script>
	<!-- end: MAIN JAVASCRIPTS -->
	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	@yield('js')
	<script src="{{asset('assets/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
	<script src="{{asset('assets/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js')}}">
	</script>
	<script src="{{asset('assets/plugins/flex-slider/jquery.flexslider.js')}}"></script>
	<script src="{{asset('assets/plugins/stellar.js/jquery.stellar.min.js')}}"></script>
	<script src="{{asset('assets/plugins/colorbox/jquery.colorbox-min.js')}}"></script>
	<script src="{{asset('assets/js/index.js')}}"></script>
	<script src="{{asset('plugins/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
	<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
	<script src="{{asset('js/ui-modals.js')}}"></script>
	<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
	<script src="{{asset('js/table-data.js')}}"></script>
	<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
	<script src="{{asset('js/form-elements.js')}}"></script>
	<script>
		jQuery(document).ready(function() {
			Main.init();
			Index.init();
			$.stellar();
		});
	</script>
</body>

</html>