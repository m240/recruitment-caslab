<div class="container profile-page">
    <br>
    <button href="#pengumuman_tahap3_modal" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i> Add Pengumuman Tahap 3</button>
    <button href="#pengumuman_tahap3tdklolos_modal" data-toggle="modal" class="btn btn-danger"><i class="fa fa-plus"></i> Add Pengumuman Tdk Lolos Tahap 3</button>
    <table class="table table-striped table-bordered table-full-width">
        <thead>
            <tr>
                <th style="width: 12%">
                    Wawancara 2 :
                </th>
                <th style="width: 70%">
                    @foreach($sesi->wawancara2 as $wawancara2s)
                    <a onclick="hapusWawancara2('{{$wawancara2s->id}}','{{$wawancara2s->nama_kriteria}}')" style="margin: 3px" class="btn btn-info" role="button">
                        {{$wawancara2s->nama_kriteria}} : {{$wawancara2s->prosentase}} %
                    </a>
                    @endforeach
                </th>
                <th style="width: 18%">
                    <button href="#wawancara_modal" data-toggle="modal" class="btn btn-info"><i class="fa fa-plus"></i> Add Kriteria Wawancara 2</button>
                </th>
            </tr>
        </thead>
    </table>
    <table class="table table-striped table-bordered table-full-width">
        <thead>
            <tr>
                <th style="width: 12%">
                    Project :
                </th>
                <th style="width: 70%">
                    @foreach($sesi->project as $projects)
                    <a onclick="hapusProject('{{$projects->id}}','{{$projects->nama_kriteria}}')" style="margin: 3px" class="btn btn-yellow" role="button">
                        {{$projects->nama_kriteria}} : {{$projects->prosentase}} %
                    </a>
                    @endforeach
                </th>
                <th style="width: 18%">
                    <button href="#project_modal" data-toggle="modal" class="btn btn-yellow"><i class="fa fa-plus"></i> Add Kriteria Project</button>
                </th>
            </tr>
        </thead>
    </table>
    <table class="table table-striped table-bordered table-full-width">
        <thead>
            <tr>
                <th style="width: 12%">
                    Magang :
                </th>
                <th style="width: 70%">
                    @foreach($sesi->magang as $magangs)
                    <a onclick="hapusMagang('{{$magangs->id}}','{{$magangs->nama_kriteria}}')" style="margin: 3px" class="btn btn-green" role="button">
                        {{$magangs->nama_kriteria}} : {{$magangs->prosentase}} %
                    </a>
                    @endforeach
                </th>
                <th style="width: 18%">
                    <button href="#magang_modal" data-toggle="modal" class="btn btn-green"><i class="fa fa-plus"></i> Add Kriteria Magang</button>
                </th>
            </tr>
        </thead>
    </table>

    <table class="table table-striped table-bordered table-full-width">
        <form action="{{route('tahap3.set.total.tahap3')}}" method="POST">
            @csrf
            <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
            <thead>
                <tr>
                    <th style="width: 12%">
                        Kriteria Final:
                    </th>
                    <th style="width: 23%">
                        % Wawancara 2 :
                        @if(isset($sesi->totalTahap3[0]))
                        <input name="wawancara2" value="{{$sesi->totalTahap3[0]->prosentase_wawancara_2}}" type="number" min="0" placeholder="persentase % nilai" id="form-field-18" class="form-control">
                        @else
                        <input name="wawancara2" type="number" min="0" placeholder="nilai belum di set" id="form-field-18" class="form-control">
                        @endif
                    </th>
                    <th style="width: 24%">
                        % Project
                        @if(isset($sesi->totalTahap3[0]))
                        <input name="project" value="{{$sesi->totalTahap3[0]->prosentase_project}}" type="number" min="0" placeholder="persentase % nilai" id="form-field-18" class="form-control">
                        @else
                        <input name="project" type="number" min="0" placeholder="nilai belum di set" id="form-field-18" class="form-control">
                        @endif
                    </th>
                    <th style="width: 23%">
                        % Magang
                        @if(isset($sesi->totalTahap3[0]))
                        <input name="magang" value="{{$sesi->totalTahap3[0]->prosentase_magang}}" type="number" min="0" placeholder="persentase % nilai" id="form-field-18" class="form-control">
                        @else
                        <input name="magang" type="number" min="0" placeholder="nilai belum di set" id="form-field-18" class="form-control">
                        @endif
                    </th>
                    <th style="width: 18%">
                        <button class="btn btn-primary"><i class="clip-settings"></i> Set Kriteria Final</button>
                    </th>
                </tr>
            </thead>
        </form>
    </table>
