@extends('layouts.user')

@section('css')
<style type="text/css">
.panel-daftar{
    border: 3px solid grey;
    border-radius: 5px;
    margin-top: 20px;
    margin-bottom: 20px;
    margin-right: 170px;
    margin-left: 170px;
}
body, html {
  height: 100%;
}

.bgimg {
  background-image: url("{{asset('images/background.jpg')}}");
  height: 100%;
  background-position: center;
  background-size: cover;
  position: relative;
  color: #dff9fb;
  margin-top: 50px;
  margin-bottom: 20px;
  margin-right: 20px;
  margin-left: 20px;
  border: 3px solid grey;
  border-radius: 5px;
}

.topleft {
  position: absolute;
  top: 0;
  left: 16px;
}

.bottomleft {
  position: absolute;
  bottom: 0;
  left: 16px;
}

.middle {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
  color: #130f40;
  opacity: 0.8;
  font-family: "Courier New", Courier, monospace;
  font-size: 25px;
}

hr {
  margin: auto;
  width: 40%;
}
</style>
<link rel="stylesheet" href="{{asset('plugins/select2/select2.css')}}">
<link rel="stylesheet" href="{{asset('plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/summernote/build/summernote.css')}}">
<link href="{{asset('css/coming_soon.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
@if($status == 'adminError')
<div class="bgimg">
  <div class="middle">
    <div class="well well-lg">
      <h3><p class="text-primary">KESALAHAN</p></h3>
      <p class="text-info">Mohon Maaf Terjadi Kesalahan, Silahkan Coba Kembali Beberapa Saat Lagi &#9786;</p>
      <form target="_blank" action="https://www.instagram.com/labit.umm/">
        <button type="submit" class="btn btn-instagram">
          <i class="fa fa-camera-retro"></i>
          | Connect with Instagram | labit.umm
        </button>
      </form>
      </div>
  </div>
</div>
@elseif($status == 'closed')
<div class="bgimg">
  <div class="middle">
    <div class="well well-lg">
    	<h3><p class="text-primary">COMING SOON</p></h3>
      <p class="text-info">Pengisian data pendaftaran belum dibuka, silahkan tunggu dan ikuti terus perkembangannya di media sosial kami &#9786;</p>
      <form target="_blank" action="https://www.instagram.com/labit.umm/">
        <button type="submit" class="btn btn-instagram">
          <i class="fa fa-camera-retro"></i>
          | Connect with Instagram | labit.umm
        </button>
      </form>
      </div>
  </div>
</div>
@else
<div class="panel-daftar">
    <div class="panel-heading">
        <i class="fa fa-external-link-square"></i><h4><span class="label label-primary">Registrasi Calon Asisten:</span></h4>
    </div>
    <div class="panel-body">
        <form class="form-login" id="form_daftar" action="{{route('peserta.daftar.submit')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('post') }}
            <fieldset>
            @include('layouts.message')
<!--             <div class="alert alert-danger" id="pesan_error" style="display: none;">
              <button data-dismiss="alert" class="close">
                      ×
                  </button>
              <ul>
                    <li id="error_motivasi" style="display: none;">The motivasi field is required.</li>
                    <li id="error_ide" style="display: none;">The ide kreatif field is required.</li>
              </ul>
            </div> -->
            <div class="form-group">
                <p>
                   <i class="clip-user"></i> | <b>Nama Lengkap:</b>
                </p>
                <input type="hidden" name="nama" value="{{$peserta->full_name}}">
                <input type="text" class="form-control" value="{{$peserta->full_name}}" disabled>
                <span for="nama" class="label label-danger">{{$errors->first('nama')}}</span>
            </div>
            <div class="form-group">
                <p>
                    <i class="clip-barcode"></i> | <b>Nomor Induk Mahasiswa:</b>
                </p>
                <input type="hidden" name="nim" value="{{$peserta->user_name}}">
                <input type="text" class="form-control" value="{{$peserta->user_name}}" disabled>
                <span for="nim" class="label label-warning">{{$errors->first('nim')}}</span>
            </div>
            <div class="form-group">
                <p>
                    <i class="fa fa-qrcode"></i> | <b>Angkatan:</b>
                </p>
                <input type="hidden" name="angkatan" value="{{substr($peserta->user_name,0,4)}}">
                <input type="text" class="form-control" value="{{substr($peserta->user_name,0,4)}}" disabled>
                <span for="angkatan" class="label label-danger">{{$errors->first('angkatan')}}</span>
            </div>
            <div class="form-group">
                <p>
                    <i class="fa fa-envelope"></i> | <b>Email (pastikan email yang dimasukkan aktif):<span class="symbol required"></span></b>
                </p>
                <input type="text" class="form-control" id="email" name="email" value="{{$peserta->email}}">
                <span for="email" class="label label-danger">{{$errors->first('email')}}</span>
                <span for="email" id="error_email" class="label label-danger" style="display: none;">The email field is required.</span>
            </div>
            <div class="form-group">
                <p>
                    <i class="clip-phone"></i> | <b>Phone (pastikan nomor yang dimasukkan aktif):<span class="symbol required"></span></b>
                </p>
                <input type="text" class="form-control" id="phone" name="phone" value="{{$peserta->phone}}">
                <span for="phone" class="label label-danger">{{$errors->first('phone')}}</span>
                <span for="phone" id="error_phone" class="label label-danger">{{$errors->first('phone')}}</span>
            </div>
            <div class="form-group">
                <p>
                    <b>Gender:</span></b>
                </p>
                @if($peserta->gender == 1)
                  <input type="hidden" name="gender" value="1">
                  <input type="radio" value="1" checked disabled> <i class="fa fa-male"></i> | Male<br>
                  <input type="radio" value="0" disabled> <i class="fa fa-female"></i> | Female<br>
                @else
                    <input type="hidden" name="gender" value="0">
                    <input type="radio" value="1" disabled> <i class="fa fa-male"></i> | Male<br>
                    <input type="radio" value="0" checked disabled> <i class="fa fa-female"></i> | Female<br>
                @endif
                <span for="gender" class="label label-danger">{{$errors->first('gender')}}</span>
            </div>
            <div class="form-group">
                <p>
                    <i class="fa fa-align-left"></i> | <b>Masukkan motivasi (max: 500 karakter):<span class="symbol required"></span></b>
                </p>
                <textarea type="text" id="motivasi" maxlength="500" class="form-control limited" name="motivasi" placeholder="Motivasi anda mendaftar sebagai calon asisten lab"></textarea>
                <span for="motivasi" class="label label-danger">{{$errors->first('motivasi')}}</span>
                <span for="motivasi" id="error_motivasi" class="label label-danger" style="display: none;">The motivasi field is required.</span>
            </div>
            <div class="form-group">
                <p>
                    <i class="fa fa-lightbulb-o"></i> | <b>Masukkan ide kreatif (max: 500 karakter):<span class="symbol required"></span></b>
                </p>
                <textarea type="text" id="ide_kreatif" maxlength="500" class="form-control limited"  name="ide_kreatif" placeholder="Ide atau gagasan anda mengenai teknologi untuk kebutuhan saat ini atau di masa yang mendatang"></textarea>
                <span for="ide_kreatif" class="label label-danger">{{$errors->first('ide_kreatif')}}</span>
                <span for="ide_kreatif" id="error_ide" class="label label-danger" style="display: none;">The ide kreatif field is required.</span>
            </div>
            <div class="form-group">
                <p><b><i class="clip-paperplane"></i> |
                    Masukkan berkas lampiran .pdf (max: 1 Megabytes):<span class="symbol required"></span></b>
                </p>
                <button onclick="location.href='{{route('peserta.daftar.contoh-berkas')}}'" class="btn btn-link" type="button"><i class="fa fa-bookmark-o"></i> | Lihat contoh berkas lampiran</a> </button>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="input-group">
                        <div class="form-control uneditable-input">
                            <i class="fa fa-file fileupload-exists"></i>
                            <span class="fileupload-preview"></span>
                        </div>
                        <div class="input-group-btn">
                            <div class="btn btn-light-grey btn-file">
                                <span class="fileupload-new"><i class="fa fa-folder-open-o"></i> Select file</span>
                                <span class="fileupload-exists"><i class="fa fa-folder-open-o"></i> Change</span>
                                <input type="file" id="file" name="lampiran" class="file-input">
                            </div>
                            <a href="#" class="btn btn-light-grey fileupload-exists" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                            </a>
                        </div>
                    </div>
                </div>
                <span for="lampiran" class="label label-danger">{{$errors->first('lampiran')}}</span>
                <span for="lampiran" id="error_lampiran" class="label label-danger" style="display: none;">The lampiran field is required.</span>
            </div>
            <div class="form-actions">
                <button type="button" onclick="cek()" class="btn btn-green pull-right">
                    Daftar <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
            <button onclick="location.href='{{route('peserta.index')}}'" class="btn btn-secondary pull-right" type="button"><i class="fa fa-arrow-circle-left"></i> Batal</a> </button>
            </fieldset>
        </form>
    </div>
</div>
<script>
  function validateEmail(email) {
    var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(email);
  }
  function cek(){
    var motivasi = document.getElementById("motivasi");
    var ide = document.getElementById("ide_kreatif");
    var file = document.getElementById("file");
    var email = $("#email").val();
    var phone = document.getElementById("phone");
    var counter = 0;

    if (email == 0) {
      document.getElementById("error_email").innerHTML = "The email field is required."
      document.getElementById("error_email").style.display = "block";
    }else if(!validateEmail(email)){
      document.getElementById("error_email").innerHTML = "You have entered an invalid email address!";
      document.getElementById("error_email").style.display = "block";
    }
    else{
      document.getElementById("error_email").style.display = "none";
      counter++;
    }

    if (isNaN(phone.value)) {
      document.getElementById("error_phone").innerHTML = "You must input only numbers!"
      document.getElementById("error_phone").style.display = "block";
    }else if (phone.value == 0) {
      document.getElementById("error_phone").innerHTML = "The phone field is required."
      document.getElementById("error_phone").style.display = "block";
    }else {
      document.getElementById("error_phone").style.display = "none";
      counter++;
    }

    if (motivasi.value == 0) {
      document.getElementById("error_motivasi").style.display = "block";
    }else{
      document.getElementById("error_motivasi").style.display = "none";
      counter++;
    }
    if (ide.value == 0) {
      document.getElementById("error_ide").style.display = "block";
    }else{
      document.getElementById("error_ide").style.display = "none";
      counter++;
    }
    if (file.value == 0){
      document.getElementById("error_lampiran").innerHTML = "The lampiran field is required.";
      document.getElementById("error_lampiran").style.display = "block";
    }else if (/\.(pdf)$/i.test(file.files[0].name) === false){
      document.getElementById("error_lampiran").innerHTML = "The lampiran must be a file of type: pdf.";
      document.getElementById("error_lampiran").style.display = "block";
    }else if (file.files[0].size > 1024000 ) {
      document.getElementById("error_lampiran").innerHTML = "The lampiran may not be greater than 1024 kilobytes.";
      document.getElementById("error_lampiran").style.display = "block";
      return;
    }else{
      document.getElementById("error_lampiran").style.display = "none";
      counter++;
    }
    if (counter == 5) {document.getElementById("form_daftar").submit();}
  }
</script>
@endif
@endsection
