@extends('layouts.user')

@section('css')
<link href="{{asset('css/coming_soon.css')}}" rel="stylesheet" type="text/css">
<style type="text/css">
.panel-daftar{
  border: 3px solid grey;
  border-radius: 5px;
  margin-top: 20px;
  margin-bottom: 20px;
  margin-right: 170px;
  margin-left: 170px;
}
</style>
<link rel="stylesheet" href="{{asset('plugins/select2/select2.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datepicker/css/datepicker.css')}}">
<link rel="stylesheet" href="{{asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}">
<link rel="stylesheet" href="{{asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css')}}">
<link rel="stylesheet" href="{{asset('plugins/jQuery-Tags-Input/jquery.tagsinput.css')}}">
<link rel="stylesheet" href="{{asset('plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/summernote/build/summernote.css')}}">


<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css"/>



@endsection


@section('content')
<div class="container" style="width: auto;">
  @if($status == 'open')
  <div class="panel-daftar">
    <div class="panel-heading">
      <i class="fa fa-external-link-square"></i>

    </div>
    @include('layouts.message')
    <div class="panel-body">



     <form id="batalkan" onsubmit="if(!confirm('Batalkan keikutsertaan?')){return false;}" action="{{route('peserta.kelola-data.batal-ikut')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <input type="hidden" name="nim" value="{{$alreadyuser->id}}">
      <button type="submit" class="btn btn-danger pull-right" form="batalkan"><i class="fa fa-trash-o"></i> | Batalkan keikutsertaan</a> </button>
    </form>


    <div style="padding-right: 220px;">
     <a onclick="detail('{{$alreadyuser->id}}')" class="btn btn-yellow pull-right">
      <i class="clip-star-3"></i>
    Tambah Skill</a> 
  </div>

  <h5><span class="label label-info">Update data diri calon asisten:</span></h5>



  <form class="form-login" action="{{route('peserta.kelola-data.submit.informasi')}}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('post') }}
    <div class="form-group">
      <p>
       <i class="clip-user"></i> | <b>Nama Lengkap:</b>
     </p>
     <input type="hidden" name="nama" value="{{$alreadyuser->nama}}">
     <input type="text" class="form-control" value="{{$alreadyuser->nama}}" disabled>
     <span for="nama" class="label label-danger">{{$errors->first('nama')}}</span>
   </div>

   <div class="form-group">
    <p>
      <i class="clip-barcode"></i> | <b>Nomor Induk Mahasiswa:</b>
    </p>
    <input type="hidden" name="nim" value="{{$alreadyuser->nim}}">
    <input type="text" class="form-control" value="{{$alreadyuser->nim}}" disabled>
    <span for="nim" class="label label-warning">{{$errors->first('nim')}}</span>
  </div>

  <div class="form-group">
    <p>
      <i class="fa fa-qrcode"></i> | <b>Angkatan:</b>
    </p>
    <input type="hidden" name="angkatan" value="{{substr($alreadyuser->nim,0,4)}}">
    <input type="text" class="form-control" value="{{substr($alreadyuser->nim,0,4)}}" disabled>
    <span for="angkatan" class="label label-danger">{{$errors->first('angkatan')}}</span>
  </div>

  <div class="form-group">
    <p>
      <i class="fa fa-envelope"></i> | <b>Email (pastikan email yang dimasukkan aktif):<span class="symbol required"></span></b>
    </p>
    <input type="email" class="form-control" name="email" value="{{$alreadyuser->email}}">
    <span for="email" class="label label-danger">{{$errors->first('email')}}</span>
  </div>


  <div class="form-group">
    <p>
      <i class="clip-phone"></i> | <b>Phone (pastikan nomor yang dimasukkan aktif):<span class="symbol required"></span></b>
    </p>
    <input type="text" class="form-control" name="phone" value="{{$alreadyuser->phone}}">
    <span for="phone" class="label label-danger">{{$errors->first('phone')}}</span>
  </div>

  <div class="form-group">
    <p>
      <b>Gender:</span></b>
    </p>
    @if($alreadyuser->gender == 1)
    <input type="hidden" name="gender" value="1">
    <input type="radio" value="1" checked disabled> <i class="fa fa-male"></i> | Male<br>
    <input type="radio" value="0" disabled> <i class="fa fa-female"></i> | Female<br>
    @else
    <input type="hidden" name="gender" value="0">
    <input type="radio" value="1" disabled> <i class="fa fa-male"></i> | Male<br>
    <input type="radio" value="0" checked disabled> <i class="fa fa-female"></i> | Female<br>
    @endif
    <span for="gender" class="label label-danger">{{$errors->first('gender')}}</span>
  </div>

  <div class="form-group">
    <p>
      <i class="fa fa-align-left"></i> | <b>Masukkan motivasi (max: 500 karakter):<span class="symbol required"></span></b>
    </p>
    <textarea type="text" maxlength="500" class="form-control limited" name="motivasi">{{$alreadyuser->motivasi}}</textarea>
    <span for="motivasi" class="label label-danger">{{$errors->first('motivasi')}}</span>
  </div>


  <div class="form-group">
    <p>
      <i class="fa fa-lightbulb-o"></i> | <b>Masukkan ide kreatif (max: 500 karakter):<span class="symbol required"></span></b>
    </p>
    <textarea type="text" maxlength="500" class="form-control limited" name="ide_kreatif">{{$alreadyuser->ide_kreatif}}</textarea>
    <span for="ide_kreatif" class="label label-danger">{{$errors->first('ide_kreatif')}}</span>
  </div>



  <h5><span class="label label-info">Update berkas:</span></h5>
  <div class="form-group">
    <p><b><i class="clip-paperplane"></i> |
      Masukkan berkas lampiran .pdf (max: 1 Megabytes):<span class="symbol required"></span></b>
    </p>
    <button onclick="location.href='{{route('peserta.daftar.contoh-berkas')}}'" class="btn btn-link" type="button"><i class="fa fa-bookmark-o"></i> | Lihat contoh berkas lampiran</a> </button>


    <div class="fileupload fileupload-new" data-provides="fileupload">
      <div class="input-group">


        <div class="form-control uneditable-input">
          <i class="fa fa-file fileupload-exists"></i>
          <span class="fileupload-preview"></span>
        </div>


        <div class="input-group-btn">
          <div class="btn btn-light-grey btn-file">
            <span class="fileupload-new"><i class="fa fa-folder-open-o"></i> Select file</span>
            <span class="fileupload-exists"><i class="fa fa-folder-open-o"></i> Change</span>
            <input type="file" name="lampiran" class="file-input">
          </div>
          <a href="#" class="btn btn-light-grey fileupload-exists" data-dismiss="fileupload">
            <i class="fa fa-times"></i> Remove
          </a>
        </div>


      </div>
    </div>


    <span for="lampiran" class="label label-danger">{{$errors->first('lampiran')}}</span>
  </div>

  <button type="submit" class="btn btn-success pull-right">
    Update data diri <i class="fa fa-arrow-circle-right"></i>
  </button>

  <button onclick="location.href='{{route('peserta.index')}}'" class="btn btn-secondary pull-right" type="button"><i class="fa fa-arrow-circle-left"></i> Batal</a> </button>
</form>


<form id="lampiran" action="{{route('peserta.kelola-data.lampiran')}}" method="POST" enctype="multipart/form-data">
  {{ csrf_field() }}
  <input type="hidden" name="lampiran" value="{{$alreadyuser->lampiran}}">
  <button type="submit" class="btn btn-link"><i class="fa fa-download" form="lampiran"></i> | Lihat hasil upload berkas lampiran</a> </button>
</form>


</div>
</div>



<!-- detail -->

@endif

<!-- tes -->
<div id="form_detail" class="modal fade" tabindex="-1" style="display: none; vertical-align: top;">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
    </button>
    <h4 class="modal-title">Kemampuan Yang Dikuasai</h4>
  </div>

  <div class="modal-body">
    <form enctype="multipart/form-data" action="{{route('inputskill.update')}}" method="POST">
      @method('put')
      @csrf

      <input type="hidden" class="form-control" name="id" id="id">

      <table class="table table-striped table-bordered table-hover table-full-width">

        <thead>
          <tr>
            <th>Skill</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>

          @foreach ($skills as $skill)
          <tr>
            <td>
              {{$skill->JenisSkill}}
            </td>
            <td>    
              <input id="skill_{{$skill->id}}" class="skill_item" name="skill_{{$skill->id}}" type="checkbox" >
            </td>
          </tr>
          @endforeach
        </tbody>

      </table>


      
    </div>


    <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-light-grey">
        Cancel
      </button>
      <button type="submit" class="btn btn-blue" value="submit">
        <i class="fa fa-share"></i> Update
      </button>
    </div>
  </form>
</div>

</div>
@endsection

@section('js')

<script type="text/javascript">
  function detail(id) {
    $.ajax({
      url: "{{url('peserta/daftar/detail/')}}" + "/" + id,
      dataType: "json",
      success: function(peserta) {
        $('#id').val(peserta.id);
        $('.skill_item').prop("checked", false);
      },
    });
    
    $.ajax({
      url: "{{url('peserta/daftar/detail/skill')}}" + "/" + id,
      dataType: "json",
      success: function(skill) {
        if(skill){
          for(var i = 0; i < skill.length; i++) {
            var obj = skill[i];
            $('#skill_'+obj.id).prop('checked', true);
          }
        }
        $('#form_detail').modal('show');
      }
    });
  }
  jQuery(document).ready(function() {
    Main.init();
    TableData.init();
  });
</script>
<script src="{{asset('js/coming_soon.js')}}"></script>
@endsection
