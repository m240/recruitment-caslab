@extends('layouts.user')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
@endsection

@section('content')
@include('layouts.message')
<section class="page-top">
    <div class="container">
        <div class="col-md-4 col-sm-4">
            <h1><i class="fa fa-pencil-square-o"></i> Tahap 1 </h1>
        </div>
        <div class="col-md-8 col-sm-8">
            <ul class="pull-right breadcrumb">
                <li>
                    <b>Your Time : &nbsp;<span id="timer"> 0.00</span></b>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="wrapper">
    <div class="container" id="tag_container">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading" id="test_status">
                    Questions
                </div>
                <div class="panel-body">
                    <form id="test" method="POST">
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-3" style="float: right">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Category
                </div>
                <div class="panel-body">
                    @foreach ($categories as $category)
                    <h4>{{$category->judul}}
                        <span style="float: right" class="badge badge-info">{{$category->persentase}} %</span>
                    </h4>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>

<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-body">
        <p>
            Open Recruitment Stage 1 Completed !!!
        </p>
    </div>
    <div class="modal-footer">
        <form action="{{route('peserta.tahap1.final')}}" method="post">
            @csrf
            <input type="hidden" id="id_delete" name="id_tahap1" value="{{$id_tahap1}}">
            <button type="submit" class="btn btn-success" value="submit">
                Continue
            </button>
        </form>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    var pos = 0, test, test_status, question, choice, choices, chA, chB, chC, correct = 0;
    var questions = {!!$questions!!}
    function _(x){
        return document.getElementById(x);
    }
    function renderQuestion(){
        test = _("test");
        // if(pos >= {{$count}}){
        //     pos = 0;
        //     correct = 0;
        //     setOut();
        //     // alert('You got '+correct+' of '+{{$count}}+' questions correct');
        //     // pos = 0;
        //     // correct = 0;
        //     // window.location.href= '/peserta/progres';
        //     // break;
        //     // test.innerHTML = "<h2>You got "+correct+" of "+{{$count}}+" questions correct</h2>";
        //     // _("test_status").innerHTML = "Test Completed";
            
        // }

        _("test_status").innerHTML = "Question "+(pos+1)+" of "+{{$count}};
        question = questions[pos].soal;
        aset = "{{asset('storage')}}";
        image = questions[pos].gambar;
        chA = questions[pos].options[0].isi;
        chB = questions[pos].options[1].isi;
        chC = questions[pos].options[2].isi;
        chD = questions[pos].options[3].isi;

        test.innerHTML = "<input type='hidden' name='_token' value='{{csrf_token()}}'>";
        test.innerHTML += "<input type='hidden' name='id_tahap1' id='id_tahap1' value='"+{{$id_tahap1}}+"'>";
        test.innerHTML += "<input type='hidden' name='pertanyaan_id' id='pertanyaan_id' value='"+questions[pos].id+"'>";
        test.innerHTML += "<h4>"+question+"</h4>";
        if (image != null) {
            test.innerHTML += "<img src='"+aset+"/"+image+"' alt='Gambar' width='600px'>";
        }
        
        test.innerHTML += "<div class='radio'><input type='radio' name='choices' id='choices' value='"+questions[pos].options[0].id+"'>"+chA+"</div>";
        test.innerHTML += "<div class='radio'><input type='radio' name='choices' id='choices' value='"+questions[pos].options[1].id+"'>"+chB+"</div>";
        test.innerHTML += "<div class='radio'><input type='radio' name='choices' id='choices' value='"+questions[pos].options[2].id+"'>"+chC+"</div>";
        test.innerHTML += "<div class='radio'><input type='radio' name='choices' id='choices' value='"+questions[pos].options[3].id+"'>"+chD+"</div><br>";
        test.innerHTML += "<button type='submit' id='saveBtn' value='submit' style='float: right' class='btn btn-primary'>Submit</button>";
        
    }

    $(document).ready(function () {
        $('#test').submit(function (e) {
            e.preventDefault();
            $.ajax({
                data: $('#test').serialize(),
                url: "{{route('peserta.store')}}",
                type: 'POST',
                success: function(data) {
                    pos++;
                    if(pos >= {{$count}}){
                        pos = 0;
                        $('#static').modal('show');
                        // modal
                    }
                    

                    renderQuestion();
                    
                },error:function(){ 
                    alert("error!!!!");
                },
            });
        });
    });

    window.addEventListener("load", renderQuestion, false);

    // $(document).ready(function(){
    //     $(".tombol-simpan").click(function(e){
    //         e.preventDefault();
    //         $.ajaxSetup({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             }
    //         });
    //         $.ajax({
    //             url: "{{url('/peserta/tahap-1/store')}}",
    //             method: 'POST',
    //             data: {
    //                 tahap_1_id : $('#id_tahap1').val(),
    //                 pertanyaan_id : $('#pertanyaan_id').val(),
    //                 choice : $('#choices').val(),
    //             },
    //             success: function() {
    //                 jQuery('.alert').show();
    //                 // $('.tampildata').load("tampil.php");
    //                 // choices = document.getElementsByName("choices");
    //                 // for(var i=0; i<choices.length; i++){ 
    //                 //     if(choices[i].checked){ 
    //                 //         choice=choices[i].value;
    //                 //     } 
    //                 // } 
    //                 // if(choice==1){ 
    //                 //     correct++; 
    //                 // }
    //                 // pos++;

    //                 // renderQuestion();
    //             }
    //         });
    //     });
    // });

    // function checkAnswer(){
    //     choices = document.getElementsByName("choices");
    //     for(var i=0; i<choices.length; i++){ 
    //         if(choices[i].checked){ 
    //             choice=choices[i].value;
    //         } 
    //     } 
    //     if(choice==1){ 
    //         correct++; 
    //     }
    //     pos++;

    //     renderQuestion(); 
    // } 

    
</script>
<script type="text/javascript">
    var max_time = '<?php echo $time1; ?>';
    var c_seconds = 0;
    var total_seconds =60*max_time;
    max_time = parseInt(total_seconds/60);
    c_seconds = parseInt(total_seconds%60);

    document.getElementById("timer").innerHTML=max_time + ' Minutes ' + c_seconds + ' Seconds';

    function init(){
    document.getElementById("timer").innerHTML=max_time + ' Minutes ' + c_seconds + ' Seconds';
    setTimeout("CheckTime()",999);
    }

    function CheckTime(){
    document.getElementById("timer").innerHTML=max_time + ' Minutes ' + c_seconds + ' Seconds' ;
    if(total_seconds <=0){ setTimeout('document.quiz.submit()',1); } else { total_seconds=total_seconds -1;
        max_time=parseInt(total_seconds/60); c_seconds=parseInt(total_seconds%60); setTimeout("CheckTime()",999); } }
        init(); 
</script> <!-- script for disable url -->
<script type="text/javascript">
    var time= '<?php echo $time1; ?>';
    var realtime = time*60000;
        setTimeout(function () {
            $('#static').modal('show');},
        realtime);

        function setOut() {
            // alert('You got '+correct+' of '+{{$count}}+' questions correct');
            
            window.location.href= '/peserta/progres';
        };
</script>
<script type="text/javascript">

</script>
@endsection