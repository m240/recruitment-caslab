@extends('layouts.user')
@section('css')
<style type="text/css">
    .panel-daftar {
        border: 3px solid grey;
        border-radius: 5px;
        margin-top: 20px;
        margin-bottom: 20px;
        margin-right: 170px;
        margin-left: 170px;
    }

    body,
    html {
        height: 100%;
    }

    .bgimg {
        background-image: url("{{asset('images/background.jpg')}}");
        height: 100%;
        background-position: center;
        background-size: cover;
        position: relative;
        color: #dff9fb;
        margin-top: 50px;
        margin-bottom: 20px;
        margin-right: 20px;
        margin-left: 20px;
        border: 3px solid grey;
        border-radius: 5px;
    }

    .topleft {
        position: absolute;
        top: 0;
        left: 16px;
    }

    .bottomleft {
        position: absolute;
        bottom: 0;
        left: 16px;
    }

    .middle {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
        color: #130f40;
        opacity: 0.8;
        font-family: "Courier New", Courier, monospace;
        font-size: 20px;
    }

    hr {
        margin: auto;
        width: 40%;
    }
</style>
<link rel="stylesheet" href="{{asset('plugins/select2/select2.css')}}">
<link rel="stylesheet" href="{{asset('plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/summernote/build/summernote.css')}}">
<link href="{{asset('css/coming_soon.css')}}" rel="stylesheet" type="text/css">
@endsection

<head>
    <title>Pengumuman Tahap 3</title>
</head>

<body>
    <div class="bgimg">
        <div class="middle">
            <div class="well well-lg">
                <h3>
                    <p class="text-primary">PENGUMUMAN TAHAP 3</p>
                </h3>
                <p class="text-info">
                    @foreach($liat3 as $pengumuman3)
                    {{ $pengumuman3->namapengumumantahap3 }}
                    @endforeach &#9786;
                </p>
                <form target="_blank" action="https://www.instagram.com/labit.umm/">
                    <button type="submit" class="btn btn-instagram">
                        <i class="fa fa-camera-retro"></i>
                        | Connect with Instagram | labit.umm
                    </button>
                </form>
            </div>
        </div>
    </div>

</body>