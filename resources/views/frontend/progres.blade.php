@extends('layouts.user')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('css/progres.css')}}">
@endsection

@section('title')
Progres
@endsection

@section('description')
Halaman Progres
@endsection

@section('content')
@include('layouts.message')

@if($status == 'buka')
<div class="container" style="width:100%;background: #353738;">
    <div id="timeline">
        <!-- Start coloring pendaftaran cirlce -->
        @if($cek[0]->status_pendaftaran == 'dibuka' && $daftar == 'sudah' )
        <div class="dot" id="one" style="background-color: green;">
            <a href="{{route('peserta.daftar')}}">
                <i class="clip-note circle-icon" style="left:7px;top:6px;position: absolute;"></i>
                <date>Pendaftaran</date>
            </a>
        </div>
        @elseif($cek[0]->status_pendaftaran == 'dibuka' && $daftar == 'belum' )
        <div class="dot" id="one" style="background-color: orange;">
            <a href="{{route('peserta.daftar')}}">
                <i class="clip-note circle-icon" style="left:7px;top:6px;position: absolute;"></i>
                <date>Pendaftaran</date>
            </a>
        </div>
        @elseif($cek[0]->status_pendaftaran == 'dibuka' && $daftar == 'tdklolos' )
        <div class="dot" id="one" style="background-color: red;">
            <i class="clip-note circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Pendaftaran</date>
        </div>
        @else
        <div class="dot" id="one" style="background-color: white;">
            <i class="clip-note circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Pendaftaran</date>
        </div>
        @endif
        <!-- End Coloring pendaftaran circle -->
        <!--  -->
        <!-- Start coloring Tahap 1 circle -->
        @if($daftar == 'tdklolos' || $daftar == 'belum')
        <div class="dot" id="two" style="background-color: white;">
            <i class="clip-pencil-3 circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 1</date>
        </div>
        @elseif($cek[0]->status_tahap1 == 'ditutup' && $psrta->status_tahap1 == 'lolos')
        <div class="dot" id="two" style="background-color: green;">
            <i class="clip-pencil-3 circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 1</date>
        </div>
        @elseif($cek[0]->status_tahap1 == 'ditutup' && $psrta->status_tahap1 == 'tidak lolos')
        <div class="dot" id="two" style="background-color: red;">
            <i class="clip-pencil-3 circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 1</date>
        </div>
        @elseif($cek[0]->status_tahap1 == 'ditutup' && $psrta->status_tahap1 == 'menunggu')
        <div class="dot" id="two" style="background-color: white;">
            <i class="clip-pencil-3 circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 1</date>
        </div>
        @elseif($cek[0]->status_tahap1 == 'dibuka' && $psrta->status_tahap1 == 'lolos')
        <div class="dot" id="two" style="background-color: green;">
            <i class="clip-pencil-3 circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 1</date>
        </div>
        @elseif($cek[0]->status_tahap1 == 'dibuka' && $psrta->status_tahap1 == 'menunggu')
        <div class="dot" id="two" style="background-color: orange;">
            <a href="{{route('peserta.tahap1')}}">
                <i class="clip-pencil-3 circle-icon" style="left:7px;top:6px;position: absolute;"></i>
                <date>Tahap 1</date>
            </a>
        </div>
        @elseif($cek[0]->status_tahap1 == 'dibuka' && $psrta->status_tahap1 == 'tidak lolos')
        <div class="dot" id="two" style="background-color: red;">
            <i class="clip-pencil-3 circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 1</date>
        </div>
        @endif
        <!-- End coloring Tahap 1 circle -->
        <!--  -->
        <!-- Start coloring Tahap 2 circle -->
        @if($daftar == 'tdklolos' || $daftar == 'belum' || $psrta->status_tahap1 == 'tidak lolos' ||
        $psrta->status_tahap1 == 'menunggu')
        <div class="dot" id="three" style="background-color: white;">
            <i class="clip-world circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 2</date>
        </div>
        @elseif($cek[0]->status_tahap2 == 'ditutup' && $psrta->status_tahap2 == 'lolos')
        <div class="dot" id="three" style="background-color: green;">
            <i class="clip-world circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 2</date>
        </div>
        @elseif($cek[0]->status_tahap2 == 'ditutup' && $psrta->status_tahap2 == 'tidak lolos')
        <div class="dot" id="three" style="background-color: red;">
            <i class="clip-world circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 2</date>
        </div>
        @elseif($cek[0]->status_tahap2 == 'ditutup' && $psrta->status_tahap2 == 'menunggu')
        <div class="dot" id="three" style="background-color: white;">
            <i class="clip-world circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 2</date>
        </div>
        @elseif($cek[0]->status_tahap2 == 'dibuka' && $psrta->status_tahap2 == 'lolos')
        <div class="dot" id="three" style="background-color: green;">
            <i class="clip-world circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 2</date>
        </div>
        @elseif($cek[0]->status_tahap2 == 'dibuka' && $psrta->status_tahap2 == 'menunggu')
        <div class="dot" id="three" style="background-color: orange;">
            <a href="/peserta/showpengumumantahap2/{{$cek[0]->id}}">
                <i class="clip-world circle-icon" style="left:7px;top:6px;position: absolute;"></i>
                <date>Tahap 2</date>
            </a>
        </div>
        @elseif($cek[0]->status_tahap2 == 'dibuka' && $psrta->status_tahap2 == 'tidak lolos')
        <div class="dot" id="three" style="background-color: red;">
            <i class="clip-world circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 2</date>
        </div>
        @endif
        <!-- End coloring Tahap 2 circle -->
        <!--  -->
        <!-- Start coloring Tahap 3 circle -->
        @if($daftar == 'tdklolos' || $daftar == 'belum' || $psrta->status_tahap2 == 'tidak lolos' ||
        $psrta->status_tahap2 == 'menunggu')
        <div class="dot" id="three" style="background-color: white;">
            <i class="clip-users circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 3</date>
        </div>
        @elseif($cek[0]->status_tahap3 == 'ditutup' && $psrta->status_tahap3 == 'lolos')
        <div class="dot" id="three" style="background-color: green;">
            <i class="clip-users circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 3</date>
        </div>
        @elseif($cek[0]->status_tahap3 == 'ditutup' && $psrta->status_tahap3 == 'tidak lolos')
        <div class="dot" id="three" style="background-color: red;">
            <i class="clip-users circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 3</date>
        </div>
        @elseif($cek[0]->status_tahap3 == 'ditutup' && $psrta->status_tahap3 == 'menunggu')
        <div class="dot" id="three" style="background-color: white;">
            <i class="clip-users circle-icon" style="left:7px;top:6px;position: absolute;"></i>
            <date>Tahap 3</date>
        </div>
        @elseif($cek[0]->status_tahap3 == 'dibuka' && $psrta->status_tahap3 == 'lolos')
        <div class="dot" id="three" style="background-color: green;">
            <a href="/peserta/showpengumumantahap3/{{$cek[0]->id}}">
                <i class="clip-users circle-icon" style="left:7px;top:6px;position: absolute;"></i>
                <date>Tahap 3</date>
            </a>
        </div>
        @elseif($cek[0]->status_tahap3 == 'dibuka' && $psrta->status_tahap3 == 'menunggu')
        <div class="dot" id="three" style="background-color: orange;">
            <a href="">
                <i class="clip-users circle-icon" style="left:7px;top:6px;position: absolute;"></i>
                <date>Tahap 3</date>
            </a>
        </div>
        @elseif($cek[0]->status_tahap3 == 'dibuka' && $psrta->status_tahap3 == 'tidak lolos')
        <div class="dot" id="three" style="background-color: red;">
            <a href="/peserta/showpengumumantahap3tdklolos/{{$cek[0]->id}}">
                <i class="clip-users circle-icon" style="left:7px;top:6px;position: absolute;"></i>
                <date>Tahap 3</date>
            </a>
        </div>
        @endif
        <!-- End coloring Tahap 3 circle -->
        <div class="inside"></div>
    </div>
    <div id="timeline" style="background: #353738;margin-top: 100px">
        <div class="dot" id="one" style="background-color: green; width: 30px;height: 30px;">
            <div class="inside">Lolos</div>
        </div>


    </div>
    <div id="timeline" style="background: #353738;margin-top: 30px">
        <div class="dot" id="one" style="background-color: red; width: 30px;height: 30px;">
            <div class="inside">Tidak Lolos</div>
        </div>

    </div>
    <div id="timeline" style="background: #353738;margin-top: 30px">
        <div class="dot" id="one" style="background-color: white; width: 30px;height: 30px;">
            <div class="inside">Belum Terbuka</div>
        </div>

    </div>
    <div id="timeline" style="background: #353738;margin-top: 30px;margin-bottom: 30px;">
        <div class="dot" id="one" style="background-color: orange; width: 30px;height: 30px;">
            <div class="inside">Sedang Berjalan</div>
        </div>

    </div>

</div>
@elseif($status == 'kelebihan')
<p>asdasd</p>
@else
<p>lkjlkj</p>
@endif


@endsection
@section('js')
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
@endsection
<!-- <div class="row" style="position:relative; width: 10px;background: black;">  
            <div class="col-sm-3">
                <div class="core-box">
                    <div class="heading">
                        <h2>Pendaftaran</h2>
                        <h2>
                            <i class="clip-note circle-icon"></i>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="core-box">
                    <div class="heading">
                        <h2>Tahap 1</h2>
                        <h2>
                            <i class="clip-pencil-3 circle-icon"></i>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="core-box">
                    <div class="heading">
                        <h2>Tahap 2</h2>
                        <h2>
                            <i class="clip-pencil-3 circle-icon"></i>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="core-box">
                    <div class="heading">
                        <h2>Tahap 3</h2>
                        <h2>
                            <i class="clip-users circle-icon"></i>
                        </h2>
                    </div>
                </div>
            </div>
        </div> -->