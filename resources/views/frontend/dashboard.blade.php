@extends('layouts.user')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('title')
    Dashboard Peserta
@endsection

@section('description')
    Home
@endsection

@section('content')
@include('layouts.message')
{{-- start REVOLUTION SLIDERS --}}
<section class="fullwidthbanner-container">
    <div class="fullwidthabnner">
        <ul>
            <!-- start: FIRST SLIDE -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                <img src="{{asset('images/timeline.png')}}" style="background-color:rgb(246, 246, 246)" alt="slidebg1"  data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat">
            </li>
            <!-- end: FIRST SLIDE -->
            <!-- start: SECOND SLIDE -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                <img src=""  style="background-color:rgb(246, 246, 246)" alt="slidebg1"  data-bgfit="contain" data-bgposition="center center" data-bgrepeat="no-repeat">
            </li>
            <!-- end: SECOND SLIDE -->
            <!-- start: THIRD SLIDE -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                <img src="assets/images/sliders/slidebg3.png"  style="background-color:rgb(246, 246, 246)" alt="slidebg1"  data-bgfit="cover" data-bgposition="left bottom" data-bgrepeat="no-repeat">
            </li>
            <!-- end: THIRD SLIDE -->
        </ul>
    </div>
</section>
<!-- end: REVOLUTION SLIDERS -->
@endsection
@section('js')
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
@endsection
