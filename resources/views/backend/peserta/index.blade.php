@extends('layouts.admin')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('plugins/iCheck/skins/all.css')}}">
@endsection

@section('title')
Peserta Recruitment
@endsection

@section('description')
Pengelolaan Peserta Recruitment Asisten
@endsection

@section('content')
@include('layouts.message')
        <div class="row">
            <div class="container">
                <div class="col-md-4">
                    <a href="#form_create" data-toggle="modal" class="demo btn btn-primary"
                        style="margin-bottom: 10px; width: 50%;"><i class="fa fa-plus"></i>&ensp;Add Peserta</a>
                </div>
            </div>
        </div>
        <div class="container profile-page">
            <br>
            <table class="table table-striped table-bordered table-full-width">
                    <thead>
                        <tr>
                            <th><p class="label label-default"><i class="fa fa-male"></i> Male : {{$gender[1]}}</p></th>
                            <th><p class="label label-success" style="background-color: pink; color: black;"><i class="fa fa-female"></i> Female : {{$gender[0]}}</p></th>
                            <th><p class="label label-inverse"><i class="clip-list"></i> Jumlah Peserta : {{ $total}}</p></th>
                        </tr>
                    </thead>
            </table>
            <br>
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                <thead>
                    <tr>
                        <th style="width: 5%">Picture</th>
                        <th>NIM</th>
                        <th>Name</th>
                        <th style="width: 30%">Motivasi</th>
                        <th  style="width: 27%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pesertas as $peserta)
                    <tr>
                        <td>
                            <div class="profile-image"> 
                                <img class="circle-img" style="width: 50px;" src="https://krs.umm.ac.id/Poto/{{$peserta->angkatan}}/{{$peserta->nim}}.JPG" alt=""> 
                            </div>
                        </td>
                        <td>{{$peserta->nim}}</td>
                        <td>{{$peserta->nama}}</td>
                        <td>{{$peserta->motivasi}}</td>
                        <td>
                            <div class="col-md-3">
                                <a onclick="detail('{{$peserta->id}}')" class="btn btn-info"><i class="clip-user-2"></i> 
                                <br>Detail</a>
                            </div> 
                            <div class="col-md-5">
                                @if($peserta->lampiran != NULL)
                                    <a class="btn btn-primary" href="{{asset('').$peserta->lampiran}}"><i class="fa fa-download"></i> 
                                    <br>Attachment</a>
                                @else  
                                    <a class="btn btn-detail" style="cursor: not-allowed;" href="#"><i class="fa fa-download"></i> 
                                    <br>Attachment</a>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <a onclick="hapus('{{$peserta->id}}','{{$peserta->nama}}')" id="hapus_peserta" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i> 
                                <br>Delete</a>  
                            </div>                          
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

{{-- modal create--}}
<div id="form_createD" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Add New Peserta</h4>
    </div>
    <form enctype="multipart/form-data" action="{{route('kelolapeserta.store')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="session" class="control-label">NIM</label>
                        <input type="text" placeholder="Input nim" class="form-control" name="nim">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Motivasi</label>
                        <textarea placeholder="Input motivasi" id="form-field-22" class="form-control" style="height: 3cm" name="motivasi"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Attachment</label>
                        <input type="file" name="lampiran" id="lampiran">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>

{{-- modal detail--}}
<div id="form_detail" class="modal fade" tabindex="-1" data-width="80%" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">View Detail Peserta</h4>
    </div>
    <div class="modal-body">
        <form enctype="multipart/form-data" action="{{route('kelolapeserta.update')}}" method="POST">
            @method('put')
            @csrf
            <div class="row">
                <div class="col-md-4">
                    <input type="hidden" class="form-control" name="id" id="id">
                    <div class="form-group">
                        <label for="session" class="control-label">NIM</label>
                        <input type="text" placeholder="Input nim" class="form-control" id="nim" disabled>
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Angkatan</label>
                        <input type="text" placeholder="Input angkatan" class="form-control" id="angkatan" disabled>
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Name</label>
                        <input type="text" placeholder="Input name" class="form-control" id="name" disabled>
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Email</label>
                        <input type="text" placeholder="Input email" class="form-control" name="email" id="email">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="session" class="control-label">Gender</label>
                        <select class="form-control" id="gender" name="gender">
                            <option value="1">Laki - Laki</option>
                            <option value="0">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Phone</label>
                        <input type="text" placeholder="Input phone" class="form-control" name="phone" id="phone">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Motivasi</label>
                        <textarea placeholder="Input motivasi" id="motivasi" class="form-control" style="height: 3cm" name="motivasi"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Lampiran:</label>
                        <input type="file" name="lampiran" id="lampiran"> <br>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group" style="max-height: 400px; max-width: 300px; overflow: scroll;">
                        <label for="session" class="control-label">Skill: </label>
                        <div id="tabel_skill">
                            <table class="table table-striped table-bordered table-hover table-full-width">
                                <thead>
                                    <tr>
                                        <th>Skill</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($skills as $skill)
                                    <tr>
                                        <td>
                                            {{$skill->JenisSkill}}
                                        </td>
                                        <td>    
                                            <input id="skill_{{$skill->id}}" class="skill_item" name="skill_{{$skill->id}}" type="checkbox" >
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button type="button" class="btn btn-info btn-md pull-right" data-toggle="modal" data-target="#form_tambah_skill"><i class="clip clip-expand" ></i> Add other skill</button>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-light-grey">
            Cancel
        </button>
        <button type="submit" class="btn btn-blue" value="submit">
            <i class="fa fa-share"></i> Update
        </button>
    </div>
    </form>
</div>

{{-- modal delete --}}
<div id="form_delete" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-body">
        <p>
            Are you sure to delete <label id="nama_peserta"></label> as peserta?
        </p>
    </div>
    <div class="modal-footer">
        <form action="{{route('kelolapeserta.delete')}}" method="post">
            @csrf
            @method('delete')
            <input hidden id="id_to_delete" name="id">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                Cancel
            </button>
            <button type="submit" class="btn btn-danger" value="submit">
                Continue Delete
            </button>
        </form>
    </div>
</div>

{{-- modal add skill --}}
<div id="form_tambah_skill" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Add skill for peserta</h4>
    </div>
    <form enctype="multipart/form-data" action="{{route('jenis_skill.add')}}" method="POST">
    <div class="modal-body">
        @csrf
        <div class="form-group">
            <input type="text" name="nama_skill" class="form-control" placeholder="tambahkan jenis skill baru">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default">
            Cancel
        </button>
        <button type="submit" class="btn btn-success" value="submit">
            <i class="clip-plus-circle"></i> Tambahkan
        </button>
    </div>
    </form>
</div>
@endsection

@section('js')
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
<script src="{{asset('js/table-data.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('js/form-elements.js')}}"></script>
<script>
    function hapus(id,nama){
        $('#id_to_delete').val(id);
        $('#nama_peserta').text(nama);
        $('#form_delete').modal('show');
    }

    function detail(id) {
        $.ajax({
            url: "{{url('adm1n/peserta/detail/')}}" + "/" + id,
            dataType: "json",
            success: function(peserta) {
                $('#id').val(peserta.id);
                $('#name').val(peserta.nama);
                $('#gender').val(peserta.gender);
                $('#phone').val(peserta.phone);
                $('#nim').val(peserta.nim);
                $('#angkatan').val(peserta.angkatan);
                $('#email').val(peserta.email);
                $('#motivasi').val(peserta.motivasi);
                $('.skill_item').prop("checked", false);
            },
        });
        
        $.ajax({
            url: "{{url('adm1n/peserta/detail/skill')}}" + "/" + id,
            dataType: "json",
            success: function(skill) {
                if(skill){
                    for(var i = 0; i < skill.length; i++) {
                        var obj = skill[i];
                        $('#skill_'+obj.id).prop('checked', true);
                    }
                }
                $('#form_detail').modal('show');
            }
        });
    }

    jQuery(document).ready(function() {
        Main.init();
        TableData.init();
    });
</script>
@endsection