@extends('layouts.admin')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('title')
    Manage Panitia
@endsection

@section('description')
    kelola user panitia
@endsection

@section('content')
@include('layouts.message')

<div class="row">
    <div class="container">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <a href="#responsive" data-toggle="modal" class="demo btn btn-primary" style="margin-bottom: 5px; width: 95%; float: right;"><i class="fa fa-plus"></i> Tambah Panitia</a>
        </div>
            <p>
<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
    <thead>
        <tr>
            <th>Name</th>
            <th>NIM</th>
            <th>Peran Kerja</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($panitia as $panitia)
        <tr>
                <td>{{$panitia->name}}</td>
                <td>{{$panitia->nim}}</td>
                <td>{{$panitia->job_title}}</td>
                <td style="width: 10%">
                <a onclick="detail('{{$panitia->id}}')" class="btn btn-info"><i class="fa fa-search fa fa-white"></i> Detail</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
</p>
</div>
</div>

{{-- modal create--}}
<div id="responsive" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Tambah Panitia Baru</h4>
        </div>
        <form action="{{route('manage-panitia.store')}}" method="POST">
            @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                        <div class="form-group">
                            <label for="Panitia" class="control-label">NIM Panitia:</label>
                            <input type="text" placeholder="Masukkan NIM panitia" class="form-control" name="nim">
                            <br>
                            <label for="Peran Kerja" class="control-label">Peran Kerja:</label>
                            <br>
                            <select class="form-control" id="job_title" name="job_title">
                                <option value="" selected disabled>Peran / Role</option>
                                <option value="ketua">Ketua</option>
                                <option value="sekretaris">Sekretaris</option>
                                <option value="bendahara">Bendahara</option>
                                <option value="acara">Acara</option>
                                <option value="perlengkapan">Perlengkapan</option>
                                <option value="konsumsi">Konsumsi</option>
                                <option value="pdd">PDD</option>
                            </select>
                        </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
        </form>
</div>

{{-- modal detail--}}
<div id="form_detail" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">View Detail Panitia</h4>
        </div>
        <div class="modal-body">
        <form action="{{route('manage-panitia.update')}}" method="POST">
            @method('put')
                @csrf
            <div class="row">
                <div class="col-md-12">
                        <div class="form-group">
                            <input type="hidden" placeholder="Input your panitia" class="form-control" name="id" id="id">
                            </div>
                        <div class="form-group">
                            <label for="Nama Panitia" class="control-label">Nama Panitia:</label>
                                <input type="text" placeholder="Masukkan nama panitia" class="form-control" name="name" id="name">
                            <br>
                            <label for="Peran Kerja" class="control-label">Peran Kerja:</label>
                            <br>
                            <select class="form-control" id="job_title" name="job_title">
																<option value="" selected disabled>Peran / Role</option>
                                <option value="ketua">Ketua</option>
                                <option value="sekretaris">Sekretaris</option>
                                <option value="bendahara">Bendahara</option>
                                <option value="acara">Acara</option>
                                <option value="perlengkapan">Perlengkapan</option>
                                <option value="konsumsi">Konsumsi</option>
                                <option value="pdd">PDD</option>
                            </select>
                        </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <a data-toggle="modal" class="btn btn-danger" id="delete">
                Delete
            </a>
            <button type="submit" class="btn btn-blue" value="submit">
                Update
            </button>
        </div>
    </form>
</div>

{{-- modal delete --}}
<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
			<div class="modal-body">
				<p>
					Are you sure to delete this panitia?
				</p>
			</div>
			<div class="modal-footer">
                <form action="{{route('manage-panitia.delete')}}" method="post">
                    @csrf
                    @method('delete')
                    <input type="hidden" id="id" name="id">
				<button type="button" data-dismiss="modal" class="btn btn-default">
					Cancel
				</button>
				<button type="submit" class="btn btn-danger" value="submit">
					Continue Delete
                </button>
            </form>
			</div>
		</div>
@endsection
@section('js')
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
<script src="{{asset('js/table-data.js')}}"></script>
<script>
    function detail(id){
        $.ajax({
            url : "{{url('admin/manage-panitia/detail')}}"+'/'+id,
            dataType : "json",
            success : function(json){
                $('#id').val(json[0].id);
                $('#name').val(json[0].name);
                $('#job_title').val(json[0].job_title);
                $('#delete').attr('onclick',"hapus('"+id+"')");
                $('#form_detail').modal('show');
            },
        });
    }

    function hapus(id){
        $.ajax({
            url : "{{url('admin/manage-panitia/detail')}}"+'/'+id,
            dataType : "json",
            success : function(json){
                $('#id').val(json[0].id);
                $('#form_detail').modal('hide');
                $('#static').modal('show');
            },
        });
    }

    jQuery(document).ready(function() {
        Main.init();
        TableData.init();
    });
</script>
@endsection
