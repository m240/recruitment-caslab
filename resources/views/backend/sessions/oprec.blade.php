@extends('layouts.admin')

@section('css')
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
@yield('oprec-css')
@endsection

@section('title')
Timeline {{$sesi->nama_oprec}}
@endsection

@section('description')
<br>
<span class="label label-info justify-content-center">{{$sesi->nama_oprec}}</span>
@endsection

@section('content')
@yield('body')
@endsection


@section('js')
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
@yield('oprec-js')
@endsection