@extends('layouts.admin')
@section('title')
Timeline {{$sesi->nama_oprec}}
@endsection

@section('description')
<br>
<span class="label label-info justify-content-center">{{$sesi->nama_oprec}}</span>
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />


<link href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('fonts/style.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/main-responsive.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css')}}" rel="stylesheet"
type="text/css">
<link href="{{asset('plugins/perfect-scrollbar/src/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/theme_light.css')}}" rel="stylesheet" type="text/css" id="skin_color">
<link href="{{asset('css/print.css')}}" rel="stylesheet" type="text/css" media="print" />

<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('plugins/iCheck/skins/all.css')}}">
@endsection
@section('content')
@include('layouts.message')
<div class="col-sm-12">
    <div class="tabbable">

        <div id="panel_projects" class="tab-pane">
            <table class="table table-striped table-bordered table-full-width">
                <thead>
                    <tr>
                        <th><p class="label label-default"><i class="fa fa-male"></i> Male : {{$gender[1]}}</p></th>
                        <th><p class="label label-success" style="background-color: pink; color: black;"><i class="fa fa-female"></i> Female : {{$gender[0]}}</p></th>
                        <th><p class="label label-inverse"><i class="clip-list"></i> Jumlah Peserta : {{ $total}}</p></th>
                    </tr>
                </thead>
            </table>
            <br>
            <table id="example" class="table table-striped table-bordered">
                <thead>
                    <tr>                     
                        <th class=" center">Foto</th>       
                        <th class=" center">NIM</th>
                        <th class=" center">Nama Peserta</th>
                        <th class=" center"> Email</th>
                        <th class=" center">Tanggal Daftar</th>                         
                        <th class=" center">Action</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($status as $status)  
                 <tr>              
                    <td class=" center">
                        <img class="circle-img" style="width: 50px;" src="https://krs.umm.ac.id/Poto/{{$status->peserta->angkatan}}/{{$status->peserta->nim}}.JPG" alt=""> 
                    </td>              
                    <td  class="center">{{$status->peserta->nim}}</td>
                    <td  class="center">{{$status->peserta->nama}}</td>
                    <td  class="center">{{$status->peserta->email}}</td>

                    <td  class="center">{{$status->peserta->created_at->format('d-M-Y')}}</td>

                    <td class="center">

                        <a onclick="detail('{{$status->id}}')" class="btn btn-info"><i class="clip-user-2"></i> 
                            <br>Detail</a>
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



<div id="form_detail" class="modal fade" tabindex="-1" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Update Status Peserta</h4>
    </div>

    <div class="modal-body">
        <form enctype="multipart/form-data" action="{{route('update.pendaftaran')}}" method="POST">
            @method('put')
            @csrf

            <div class="row">
                <div class="col-md-4">
                    <input type="hidden" class="form-control" name="id" id="id">
                    <div class="form-group">
                        <label for="session" class="control-label">Status Pendaftaran</label>
                        <select class="form-control" id="status_pendaftaran" name="status_pendaftaran">
                            <option value="lolos">Lolos</option>
                            <option value="tidak lolos">Tidak Lolos</option>
                            <option value="menunggu">Menunggu</option>
                        </select>
                    </div>
                </div>
            </div>

            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                <i class="fa fa-share"></i> Update
            </button>
        </form>
    </div>
</div>


    @endsection

    @section('js')
    <script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
    <script src="{{asset('js/ui-modals.js')}}"></script>

    <script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
    <script src="{{asset('js/table-data.js')}}"></script>

    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('js/form-elements.js')}}"></script>





    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <!--<![endif]-->
    <script src="{{asset('plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript">
    </script>
    <script src="{{asset('plugins/blockUI/jquery.blockUI.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/iCheck/jquery.icheck.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/perfect-scrollbar/src/jquery.mousewheel.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/perfect-scrollbar/src/perfect-scrollbar.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/less/less-1.5.0.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/jquery-cookie/jquery.cookie.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js')}}" type="text/javascript">
    </script>
    <script src="{{asset('js/main.js')}}" type="text/javascript"></script>



    <script>

        function detail(id) {
            $.ajax({
                url: "{{url('adm1n/sesi/pendaftaran/detail/')}}" + "/" + id,
                dataType: "json",
                success: function(status) {
                    $('#id').val(status.id);
                    $('#status_pendaftaran').val(status.status_pendaftaran);
                    $('#form_detail').modal('show');
                },
            });
        }




        jQuery(document).ready(function() {
            Main.init();
            TableData.init();
        // FormElements.init();
        // Index.init();
    });
</script>
@endsection