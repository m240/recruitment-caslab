@extends('layouts.admin')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('plugins/datepicker/css/datepicker.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('plugins/iCheck/skins/all.css')}}">
@endsection

@section('title')
Sesi Recruitment
@endsection

@section('description')
Pengelolaan Sesi Recruitment Asisten
@endsection

@section('content')
@include('layouts.message')

<div class="row">
    <div class="container">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <a href="#responsive" data-toggle="modal" class="demo btn btn-primary"
                style="margin-bottom: 5px; width: 80%; float: right;"><i class="fa fa-plus"></i>&ensp;Create Sesi</a>
        </div>
        <p>
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th style="width: 20%">Session</th>
                        <th style="width: 15%">Jumlah Peserta</th>
                        <th>Status</th>
                        <th>Created By</th>
                        <th style="width: 15%">Created At</th>
                        <th style="width: 13%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @foreach ($sessions as $session)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$session->nama_oprec}}</td>
                        <td>
                            {{App\Status::where('oprec_id', $session->id)->count()}}
                        </td>
                        <td>
                            @if ($session->status_dibuka_oprec == 'dibuka')
                            <span class="label label-success"> {{$session->status_dibuka_oprec}}</span>
                            @else
                            <span class="label label-danger"> {{$session->status_dibuka_oprec}}</span>
                            @endif


                        </td>
                        <td>
                            {{$session->admin->username}}
                        </td>
                        <td>{{$session->created_at->format('d - M - Y')}}</td>
                        <td style="text-align: center">
                            <a onclick="detail('{{$session->id}}')" class="btn btn-green btn-sm tooltips"
                                data-placement="top" data-original-title="Edit"><i class="fa fa-edit fa-white"></i></a>
                            <a href="sesi/timeline/{{$session->id}}" class="btn btn-yellow btn-sm tooltips"
                                data-placement="top" data-original-title="Detail"><i class="fa fa-search"></i></a>
                            <a onclick="hapus('{{$session->id}}')" class="btn btn-red btn-sm tooltips"
                                data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </p>
    </div>
</div>

{{-- modal create--}}
<div id="responsive" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Create New Session</h4>
    </div>
    <form action="{{route('sesi.store')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="number" name="admin_id" value="{{$admin_id}}" hidden>
                        <label for="session" class="control-label">Session Recruitment</label>
                        <input type="text" placeholder="Input your session" class="form-control" name="nama_oprec">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Session Status:</label>
                        <div class="radio">
                            <label><input type="radio" name="status_dibuka_oprec" checked value="dibuka">Dibuka</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="status_dibuka_oprec" value="ditutup">Ditutup</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>



{{-- modal detail--}}
<div id="form_detail" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">View Detail Session</h4>
    </div>
    <div class="modal-body">
        <form action="{{route('sesi.update')}}" method="POST">
            @method('put')
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="hidden" placeholder="Input your session" class="form-control" name="id" id="id">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Session Recruitment</label>
                        <input type="text" placeholder="Input your session" class="form-control" name="name"
                            id="nama_oprec">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Session Status:</label>
                        <div class="radio">
                            <label><input id="radio_dibuka" type="radio" name="status_dibuka_oprec"
                                    value="dibuka">Dibuka</label>
                        </div>
                        <div class="radio">
                            <label><input id="radio_ditutup" type="radio" name="status_dibuka_oprec"
                                    value="ditutup">Ditutup</label>
                        </div>
                    </div>
                </div>
            </div>

    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-blue" value="submit">
            Update
        </button>
    </div>
    </form>
</div>

{{-- modal delete --}}
<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-body">
        <p>
            Are you sure to delete session ?
        </p>
    </div>
    <div class="modal-footer">
        <form action="{{route('sesi.delete')}}" method="post">
            @csrf
            @method('delete')
            <input type="hidden" id="id_delete" name="id">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                No
            </button>
            <button type="submit" class="btn btn-danger" value="submit">
                Yes
            </button>
        </form>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
<script src="{{asset('js/table-data.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('js/form-elements.js')}}"></script>
<script>
    function detail(id) {
        $.ajax({
            url: "{{url('adm1n/sesi/detail/')}}" + '/' + id,
            dataType: "json",
            success: function(json) {
                $('#id').val(json.id);
                $('#nama_oprec').val(json.nama_oprec);
                if (json.status_dibuka_oprec === "dibuka") {
                    $('#radio_dibuka').attr('checked', true);
                } else {
                    $('#radio_ditutup').attr('checked', true)
                }
                $('#form_detail').modal('show');
            },
        });
    }

    function hapus(id) {
        $.ajax({
            url: "{{url('adm1n/sesi/detail/')}}" + '/' + id,
            dataType: "json",
            success: function(hapus) {
                $('#id_delete').val(hapus.id);
                $('#static').modal('show');
            },
        });
    }

    jQuery(document).ready(function() {
        Main.init();
        TableData.init();
        // FormElements.init();
        // Index.init();
    });
</script>
@endsection