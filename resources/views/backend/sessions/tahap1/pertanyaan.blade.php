@extends('layouts.admin')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('plugins/datepicker/css/datepicker.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('plugins/iCheck/skins/all.css')}}">
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}">
{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> --}}
@endsection

@section('title')
Tahap 1
@endsection

@section('description')
Kelola Tes Tulis Tahap 1
@endsection

@section('content')
@include('layouts.message')
<div class="row">
    <div class="container">
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-tasks"></i>
                    List Pertanyaan
                </div>
                <div class="panel-body">
                    <div class="col-md-10"></div>
                    <div class="col-md-2">
                        @if ($count_category != 0)
                        <a href="#responsive" data-toggle="modal" class="demo btn btn-primary"
                            style="margin-bottom: 5px;"><i class="fa fa-plus"></i>&ensp;Add</a>
                        @endif
                    </div>
                    <table class="table table-striped table-bordered table-hover table-full-width dataTable"
                        id="sample_1">
                        <thead>
                            <tr>
                                <th style="width: 8%">#</th>
                                <th style="width: 15%">Kategori</th>
                                <th style="width: 50%">Soal</th>
                                <th style="width: 15%">Actions</th>
                            </tr>
                            </th>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach ($questions as $question)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$question->category->judul}}

                                </td>
                                <td>{!! str_limit($question->soal, $limit = 50, $end = '...') !!}</td>
                                <td style="text-align: center">
                                    <a onclick="detail('{{$question->id}}')" class="btn btn-sm btn-info tooltips"
                                        data-placement="top" data-original-title="Detail"><i
                                            class="fa fa-search"></i></a>
                                    <a onclick="hapus('{{$question->id}}')" class="btn btn-sm btn-danger tooltips"
                                        data-placement="top" data-original-title="Delete"><i
                                            class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-book"></i>
                    List Kategori
                </div>
                <div class="panel-body">
                    <div class="col-md-10"></div>
                    <div class="col-md-2">
                        <a href="#category" data-toggle="modal" class="btn btn-primary tooltips"
                            style="margin-bottom: 5px;" data-placement="top" data-original-title="Add Category"><i
                                class="fa fa-edit"></i></a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-full-width dataTable"
                        id="sample_1">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kategori</th>
                                <th>%</th>
                                <th>Time</th>
                                <th>Actions</th>
                            </tr>
                            </th>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach ($categories as $category)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$category->judul}}</td>
                                <td>{{$category->persentase}}</td>
                                <td style="width: 15%">{{$category->time}}</td>
                                <td style="text-align: center; width: 15%">
                                    <a onclick="edit_kategori('{{$category->id}}')"
                                        class="btn btn-xs btn-warning tooltips" data-placement="top"
                                        data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a onclick="hapus_kategori('{{$category->id}}')"
                                        class="btn btn-xs btn-danger tooltips" data-placement="top"
                                        data-original-title="Delete"><i class="fa fa-trash-o"></i></a>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- modal create--}}
<div id="responsive" class="modal fade" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Create New Questions</h4>
    </div>
    <form action="{{route('pertanyaan.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="control-label">
                            Kategori <span class="symbol required"></span>
                        </label>
                        <select name="kategoris_id" id="" class="form-control">
                            @foreach ($categories as $category)
                            <option value="{{$category->id}}">{{$category->judul}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            Questions Text <span class="symbol required"></span>
                        </label>
                        <div>
                            <textarea id="summernote" name="soal"></textarea>
                        </div>
                    </div>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img
                                src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt="">
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail"
                            style="max-width: 600px; max-height: 400px; line-height: 20px;"></div>
                        <div>
                            <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i
                                        class="fa fa-picture-o"></i> Select image</span><span
                                    class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                <input type="file" name="gambar">
                            </span>
                            <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                <i class="fa fa-times"></i> Remove
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Option #1 <span class="symbol required"></span></label>
                            <input type="text" placeholder="" class="form-control" name="option1">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Option #2 <span class="symbol required"></span></label>
                            <input type="text" placeholder="" class="form-control" name="option2">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Key Answer <span class="symbol required"></span></label>
                            {!! Form::select('correct', $correct_collections, old('correct'),['class' =>
                            'form-control'])
                            !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Option #3 <span class="symbol required"></span></label>
                            <input type="text" placeholder="" class="form-control" name="option3">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Option #4 <span class="symbol required"></span></label>
                            <input type="text" placeholder="" class="form-control" name="option4">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Score <span class="symbol required"></span></label>
                            <input type="text" class="form-control" placeholder="" name="score">
                        </div>
                    </div>
                    <label for="">
                        <span class="symbol required"></span> Required Fields
                    </label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-ban"></i>
                Close
            </button>
            <button type="submit" class="btn btn-primary" value="submit"><i class="fa fa-save"></i>
                Save
            </button>
        </div>
    </form>
</div>

{{-- modal detail--}}
<div id="form_detail" class="modal fade" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">View Detail Questions</h4>
    </div>
    <div class="modal-body">
        <form action="{{route('pertanyaan.update')}}" method="POST" enctype="multipart/form-data">
            @method('put')
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="hidden" name="id" id="id_detail">
                            <label for="" class="control-label">
                                Kategori <span class="symbol required"></span>
                            </label>
                            <select name="kategoris_id" id="kategoris_id" class="form-control">
                                @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->judul}}</option>
                                @endforeach
                            </select>
                            </select>
                            <label class="control-label">
                                Questions Text <span class="symbol required"></span>
                            </label>
                            <div>
                                <textarea id="summernote1" name="soal"></textarea>
                            </div>
                        </div>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src=""
                                    alt="" id="gambar">
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail"
                                style="max-width: 600px; max-height: 400px; line-height: 20px;"></div>
                            <div>
                                <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i
                                            class="fa fa-picture-o"></i> Select image</span><span
                                        class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                    <input type="file" name="gambar">
                                </span>
                                <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                    <i class="fa fa-times"></i> Remove
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Option #1 <span class="symbol required"></span></label>
                                <input type="hidden" name="id_opt1" id="id1">
                                <input type="text" placeholder="" class="form-control" name="option1" id="option1">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Option #2</label>
                                <input type="hidden" name="id_opt2" id="id2">
                                <input type="text" placeholder="" class="form-control" name="option2" value=""
                                    id="option2">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Key Answer <span class="symbol required"></span></label>
                                {!! Form::select('correct', $correct_collections, old('correct'),['class' =>
                                'form-control', 'id' => 'correct'])
                                !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Option #3</label>
                                <input type="hidden" name="id_opt3" id="id3">
                                <input type="text" placeholder="" class="form-control" name="option3" value=""
                                    id="option3">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Option #4</label>
                                <input type="hidden" name="id_opt4" id="id4">
                                <input type="text" placeholder="" class="form-control" name="option4" value=""
                                    id="option4">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Score <span class="symbol required"></span></label>
                                <input type="number" placeholder="" class="form-control" name="score" value=""
                                    id="score">
                            </div>
                        </div>
                        <label for="">
                            <span class="symbol required"></span> Required Fields
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-warning"><i class="fa fa-ban"></i>
                    Cancel
                </button>
                <button type="submit" class="btn btn-success" value="submit"> <i class="fa fa-edit"></i>
                    Edit
                </button>
            </div>
        </form>
    </div>
</div>

{{-- modal delete --}}
<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-body">
        <p>
            Are you sure to delete question ?
        </p>
    </div>
    <div class="modal-footer">
        <form action="{{route('pertanyaan.destroy')}}" method="post">
            @csrf
            @method('delete')
            <input type="hidden" id="id_delete" name="id">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                No
            </button>
            <button type="submit" class="btn btn-danger" value="submit">
                Yes
            </button>
        </form>
    </div>
</div>

{{-- modal create category --}}
<div id="category" class="modal fade" tabindex="-1" data-width="400" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Create New Category</h4>
    </div>
    <form action="{{route('category.store')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Title <span class="symbol required"></span></label>
                        <input type="text" placeholder="Your category" class="form-control" name="judul">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Persentase % <span class="symbol required"></span></label>
                        <input type="number" placeholder="%" class="form-control" name="persentase">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Time <span class="symbol required"></span></label>
                        <input type="number" placeholder="In Minute" class="form-control" name="time">
                    </div>
                    <label for="">
                        <span class="symbol required"></span> Required Fields
                    </label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-ban"></i>
                Close
            </button>
            <button type="submit" class="btn btn-primary" value="submit"><i class="fa fa-save"></i>
                Save
            </button>
        </div>
    </form>
</div>

{{-- Modal Edit Category --}}
<div id="edit_category" class="modal fade" tabindex="-1" data-width="400" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Edit Category</h4>
    </div>
    <form action="{{route('category.update')}}" method="POST">
        @csrf
        @method('PUT')
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" id="id_edit_category" name="id">
                    <div class="form-group">
                        <label class="control-label">Title <span class="symbol required"></span></label>
                        <input type="text" placeholder="Your category" class="form-control" name="judul" id="judul">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Persentase % <span class="symbol required"></span></label>
                        <input type="number" placeholder="%" class="form-control" name="persentase" id="persentase">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Time <span class="symbol required"></span></label>
                        <input type="number" placeholder="In Minute" class="form-control" name="time" id="time">
                    </div>
                    <label for="">
                        <span class="symbol required"></span> Required Fields
                    </label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-ban"></i>
                Close
            </button>
            <button type="submit" class="btn btn-primary" value="submit"><i class="fa fa-save"></i>
                Save
            </button>
        </div>
    </form>
</div>

{{-- modal delete category --}}
<div id="delete_category" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false"
    style="display: none;">
    <div class="modal-body">
        <p>
            Are you sure to delete category ?
        </p>
    </div>
    <div class="modal-footer">
        <form action="{{route('category.destroy')}}" method="post">
            @csrf
            @method('delete')
            <input type="hidden" id="id_delete_category" name="id">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                No
            </button>
            <button type="submit" class="btn btn-danger" value="submit">
                Yes
            </button>
        </form>
    </div>
</div>

@endsection

@section('js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<script src="{{asset('plugins/autosize/jquery.autosize.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
<script src="{{asset('plugins/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('js/form-elements.js')}}"></script>
<script src="{{asset('js/table-data.js')}}"></script>
{{-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> --}}

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            tabsize: 5,
            placeholder: 'Write here...',
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
        // $('#summernote1').summernote({
        //     tabsize: 2,
        // });
    });

    function detail(id) {
        $.ajax({
            url: "{{url('/adm1n/sesi/tahap1/pertanyaan-pilihan/detail/')}}" + '/' + id,
            dataType: "json",
            success: function(detail) {
                $('#id_detail').val(detail.question.id);
                $('#kategoris_id').val(detail.category.id);
                $('#gambar').attr('src', "{{asset('storage')}}"+"/"+detail.question.gambar);
                $('#summernote1').summernote('code', detail.question.soal);
                $('#score').val(detail.question.score);
                $('#id1').val(detail.option[0].id);
                $('#option1').val(detail.option[0].isi);
                $('#id2').val(detail.option[1].id);
                $('#option2').val(detail.option[1].isi);
                $('#id3').val(detail.option[2].id);
                $('#option3').val(detail.option[2].isi);
                $('#id4').val(detail.option[3].id);
                $('#option4').val(detail.option[3].isi);
                for(i=0; i<4; i++){
                    if(detail.option[i].status == 1){
                        $('#correct').val('option'+(i+1));
                    }
                }
                $('#form_detail').modal('show');
            },
        });
    }

    function hapus(id) {
        $.ajax({
            url:"{{url('/adm1n/sesi/tahap1/pertanyaan/detail/')}}"+'/'+id,
            dataType:"json",
            success:function(detail){
                $('#id_delete').val(detail.id);
                $('#static').modal('show');
            },
        });
    }

    function hapus_kategori(id) {
        $.ajax({
            url:"{{url('/adm1n/sesi/tahap1/category/detail/')}}"+'/'+id,
            dataType:"json",
            success:function(detail){
                $('#id_delete_category').val(detail.id);
                $('#delete_category').modal('show');
            },
        });
    }

    function edit_kategori(id) {
        $.ajax({
            url:"{{url('/adm1n/sesi/tahap1/category/detail/')}}"+'/'+id,
            dataType:"json",
            success:function(detail){
                $('#id_edit_category').val(detail.id);
                $('#judul').val(detail.judul);
                $('#persentase').val(detail.persentase);
                $('#time').val(detail.time);
                $('#edit_category').modal('show');
            },
        });
    }

    jQuery(document).ready(function() {
        TableData.init();
        FormElements.init();
        Main.init();
    });
</script>
@endsection