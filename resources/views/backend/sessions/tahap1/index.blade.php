@extends('layouts.admin')

@section('css')
{{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css"> --}}
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('title')
List Peserta Tahap 1
@endsection

@section('description')
Kelola Hasil Peserta Tahap 1
@endsection

@section('content')
<table class="table hover row-border cell-border" id="sample_1">
    <thead>
        <tr style="text-align: center">
            <th style="width: 5%">#</th>
            <th style="width: 5%">Image</th>
            <th>NIM Peserta</th>
            <th>Nama Peserta</th>
            <th>Score</th>
            <th>Status</th>
            <th style="width: 9%">Actions</th>
        </tr>
    </thead>
    <tbody>
        @php
        $i = 1;
        @endphp
        @foreach ($pesertas as $peserta)
        <tr>
            <td>{{$i++}}</td>
            <td style="text-align: center"><img class="circle-img" style="width: 50px;"
                    src="https://krs.umm.ac.id/Poto/{{$peserta->peserta->angkatan}}/{{$peserta->peserta->nim}}.JPG"
                    alt="oioiS"></td>
            <td>{{$peserta->peserta->nim}}</td>
            <td>{{$peserta->peserta->nama}}</td>
            <td>{{$peserta->score_total}}</td>
            <td>
                @if ($peserta->peserta->status->first()->status_tahap1 == "menunggu")
                <span class="label label-warning"> Menunggu</span>
                @elseif ($peserta->peserta->status->first()->status_tahap1 == "lolos")
                <span class="label label-success"> Lolos</span>
                @else
                <span class="label label-danger"> Tidak Lolos</span>
                @endif
            </td>
            <td style="text-align: center">
                <a class="btn btn-sm btn-info" onclick="detail('{{$peserta->peserta_id}}')"><i class="fa fa-pencil"></i>
                    Edit</a></td>
        </tr>
        @endforeach
    </tbody>
</table>

<div id="form_detail" class="modal fade" tabindex="-1" data-width="400" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Edit Peserta Tahap 1</h4>
    </div>
    <div class="modal-body">
        <form action="{{route('tahap1.peserta.status')}}" method="POST">
            @method('put')
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="id" id="id">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Status Peserta </label>
                        <div class="radio">
                            <label><input id="radio_menunggu" type="radio" name="status_tahap1"
                                    value="menunggu">Menunggu</label>
                        </div>
                        <div class="radio">
                            <label><input id="radio_lolos" type="radio" name="status_tahap1" value="lolos">Lolos</label>
                        </div>
                        <div class="radio">
                            <label><input id="radio_tidak_lolos" type="radio" name="status_tahap1"
                                    value="tidak lolos">Tidak Lolos</label>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-blue" value="submit">
            Update
        </button>
    </div>
    </form>
</div>
@endsection

@section('js')
<!-- DataTables -->
{{-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script> --}}
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('js/form-elements.js')}}"></script>
<script src="{{asset('js/table-data.js')}}"></script>
<script>
    function detail(id) {
        $.ajax({
            url: "{{url('adm1n/sesi/tahap1/peserta/status')}}" + '/' + id,
            dataType: "json",
            success: function(json) {
                $('#id').val(json.id);
                if (json.status_tahap1 == "menunggu") {
                    $('#radio_menunggu').attr('checked', true);
                }else if(json.status_tahap1 == "lolos"){
                    $('#radio_lolos').attr('checked', true);
                } else {
                    $('#radio_tidak_lolos').attr('checked', true);
                }
                $('#form_detail').modal('show');
            },
        });
    }
</script>
<script>
    // $(document).ready( function () {
    //     $('#pesertas-table').DataTable({
    //         processing:true,
    //         serverSide:true,
    //         ajax:"",
    //         dataType: "json",
    //         type:"POST",
    //         columns:[
    //             // {data:'index', name:'index'},
    //             {data:'DT_RowIndex', name:'DT_RowIndex'},
    //             {data:'peserta.nama', name:'peserta.nama'},
                
    //             {data: 'score_total', name: 'score_total' },
    //             // {data: 'sesi.nama_oprec', name: 'sesi.nama_oprec' },
                
                

    //             {data:'action', name:'action', orderable:false, searchable:false}
    //         ]
    //     });
    // } );

    jQuery(document).ready(function() {
        TableData.init();
        // FormElements.init();
        // Index.init();
    });
</script>
@endsection