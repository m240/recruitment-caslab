@extends('backend.sessions.oprec')
@section('css')
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('fonts/style.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/main-responsive.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('plugins/perfect-scrollbar/src/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/theme_light.css')}}" rel="stylesheet" type="text/css" id="skin_color">
<link href="{{asset('css/print.css')}}" rel="stylesheet" type="text/css" media="print" />

<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('plugins/iCheck/skins/all.css')}}">
@endsection
@section('body')
@include('layouts.message')
<div class="col-sm-12">
    <div class="tabbable">
        <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
            <li class="active">
                <a data-toggle="tab" href="#panel_overview" aria-expanded="true">
                    Timeline
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#panel_projects">
                    List Peserta
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="panel_overview" class="tab-pane active">
                <div id="timeline" class="demo1">
                    <div class="timeline">
                        <div class="spine" style="background-color: purple;"></div>
                        <div class="date_separator">
                            <span>Sesi: {{$sesi->created_at}}</span>
                        </div>
                        <ul class="columns">
                            <li>
                                <div class="timeline_element">
                                    <div class="panel-heading">
                                        <i class="fa fa-external-link-square"></i>
                                        Session Created
                                        <div class="panel-tools">
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        This session has been created at {{$sesi->created_at}} and now status in
                                        {{$sesi->status_dibuka_oprec}}
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <ul class="columns">
                            <div class="date_separator">
                                <span>Pendaftaran:</span>
                            </div>
                            <li>
                                <div class="timeline_element">
                                    <div class="panel-heading">
                                        <i class="fa fa-external-link-square"></i>
                                        Pendaftaran
                                    </div>
                                    <div class="panel-body">
                                        <b>Status Pendaftaran: {{$sesi->status_pendaftaran}}</b> is simply dummy text of
                                        the printing and typesetting
                                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the
                                        1500s, when an unknown printer took a galley of type and scrambled it to make a
                                        type specimen book. It has survived not only five centuries, but also the leap
                                        into electronic typesetting, remaining essentially unchanged.
                                    </div>
                                    <div class="readmore">
                                        <div class="btn-group disabled" style="padding-right: 180px;">
                                            <a class="btn {{$sesi->status_pendaftaran == 'dibuka'  ? 'btn-success active' : 'btn-light-grey'}}" style="{{$sesi->status_pendaftaran == 'ditutup'  ? '' : 'pointer-events: none;'}} " href="{{route('pendaftaran.aktif',$sesi->id)}}">
                                                Dibuka
                                            </a>
                                            <a class="btn {{$sesi->status_pendaftaran == 'ditutup'  ? 'btn-danger active' : 'btn-light-grey'}}" role="button" aria-disabled="true" style="{{$sesi->status_pendaftaran == 'dibuka'  ? '' : 'pointer-events: none;'}} " href="{{route('pendaftaran.tutup',$sesi->id)}}">
                                                Ditutup
                                            </a>
                                        </div>

                                        <a href="{{route('lolos.pendaftaran',$sesi->id)}}" class="btn btn-purple ">
                                            Lihat Peserta <i class="fa fa-arrow-circle-right"></i>
                                        </a>

                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="date_separator">
                            <span>Tahap 1:</span>
                        </div>
                        <ul class="columns">
                            <li>
                                <div class="timeline_element">
                                    <div class="panel-heading">
                                        <i class="fa fa-external-link-square"></i>
                                        Tahap 1
                                        <div class="panel-tools">
                                            <a class="btn btn-xs btn-link panel-config" href="#form_edit_tahap_1" data-toggle="modal">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="btn btn-xs btn-link panel-config" href="{{route('tahap1.pertanyaan')}}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a class="btn btn-xs btn-link panel-config" href="{{route('tahap1.index', $sesi->id)}}">
                                                <i class="fa fa-share"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <b>Status Tahap 1: </b> @if ($sesi->status_tahap1 == 'ditutup')
                                        <span class="label label-danger">
                                            INACATIVE</span>
                                        @else
                                        <span class="label label-success">
                                            ACTIVE</span>
                                        @endif
                                        <p>is simply dummy text of the
                                            printing and typesetting
                                            industry. Lorem Ipsum has been the industry's standard dummy text ever
                                            since
                                            the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a
                                            type specimen book. It has survived not only five centuries, but also
                                            the
                                            leap
                                            into electronic typesetting, remaining essentially unchanged.</p>
                                    </div>
                                    <div class="readmore">
                                        {{-- <a href="{{route('tahap1.pertanyaan')}}" class="btn btn-success btn-sm
                                        tooltips"
                                        data-placement="top" data-original-title="Questions"><i class="fa fa-pencil fa-white"></i></a>
                                        <a class="btn btn-warning btn-sm tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit fa-white"></i></a>
                                        <a href="{{route('tahap1.index')}}" class="btn btn-purple btn-sm tooltips" data-placement="top" data-original-title="More"><i class="fa fa-share fa-white"></i></a> --}}
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <ul class="columns">
                            <div class="date_separator">
                                <span>Tahap 2:</span>
                            </div>
                            <li>
                                <div class="timeline_element">
                                    <div class="panel-heading">
                                        <i class="fa fa-external-link-square"></i>
                                        Tahap 2
                                    </div>
                                    <div class="panel-body">
                                        <b>Status Tahap 2: </b> @if ($sesi->status_tahap2 == 'ditutup')
                                        <span class="label label-danger">
                                            INACATIVE</span>
                                        @else
                                        <span class="label label-success">
                                            ACTIVE</span>
                                        @endif
                                        <p>is simply dummy text of the
                                            printing and typesetting
                                            industry. Lorem Ipsum has been the industry's standard dummy text ever
                                            since
                                            the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a
                                            type specimen book. It has survived not only five centuries, but also
                                            the
                                            leap
                                            into electronic typesetting, remaining essentially unchanged.</p>
                                    </div>
                                    <div class="readmore">
                                        <div class="btn-group disabled" style="padding-right: 180px;">
                                            <a class="btn {{$sesi->status_tahap2 == 'dibuka'  ? 'btn-success active' : 'btn-light-grey'}}" style="{{$sesi->status_tahap2 == 'ditutup'  ? '' : 'pointer-events: none;'}} " href="{{route('aktiftahap.aktif_tahap2',$sesi->id)}}">
                                                Dibuka
                                            </a>
                                            <a class="btn {{$sesi->status_tahap2 == 'ditutup'  ? 'btn-danger active' : 'btn-light-grey'}}" role="button" aria-disabled="true" style="{{$sesi->status_tahap2 == 'dibuka'  ? '' : 'pointer-events: none;'}} " href="{{route('aktiftahap.tutup_tahap2',$sesi->id)}}">
                                                Ditutup
                                            </a>
                                        </div>
                                        <a href="/adm1n/tahap2/detail/{{$sesi->id}}" class="btn btn-purple">
                                            Detail <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                    </div>
                                </div>

                        </ul>
                        <div class="date_separator">
                            <span>Tahap 3:</span>
                        </div>
                        <ul class="columns">
                            <li>
                                <div class="timeline_element">
                                    <div class="panel-heading">
                                        <i class="fa fa-external-link-square"></i>
                                        Tahap 3
                                    </div>
                                    <div class="panel-body">
                                        <b>Status Tahap 3: </b> @if ($sesi->status_tahap3 == 'ditutup')
                                        <span class="label label-danger">
                                            INACATIVE</span>
                                        @else
                                        <span class="label label-success">
                                            ACTIVE</span>
                                        @endif
                                        <p>is simply dummy text of the
                                            printing and typesetting
                                            industry. Lorem Ipsum has been the industry's standard dummy text ever
                                            since
                                            the
                                            1500s, when an unknown printer took a galley of type and scrambled it to
                                            make a
                                            type specimen book. It has survived not only five centuries, but also
                                            the
                                            leap
                                            into electronic typesetting, remaining essentially unchanged.</p>
                                    </div>
                                    <div class="readmore">
                                        <div class="btn-group disabled" style="padding-right: 180px;">
                                            <a class="btn {{$sesi->status_tahap3 == 'dibuka'  ? 'btn-success active' : 'btn-light-grey'}}" style="{{$sesi->status_tahap3 == 'ditutup'  ? '' : 'pointer-events: none;'}} " href="{{route('aktiftahap.aktif_tahap3',$sesi->id)}}">
                                                Dibuka
                                            </a>
                                            <a class="btn {{$sesi->status_tahap3 == 'ditutup'  ? 'btn-danger active' : 'btn-light-grey'}}" role="button" aria-disabled="true" style="{{$sesi->status_tahap3 == 'dibuka'  ? '' : 'pointer-events: none;'}} " href="{{route('aktiftahap.tutup_tahap3',$sesi->id)}}">
                                                Ditutup
                                            </a>
                                        </div>
                                        <a href="/adm1n/tahap3/detail/{{$sesi->id}}" class="btn btn-purple">
                                            Lihat Peserta <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                    </div>

                        </ul>

                    </div>
                </div>
            </div>
            <div id="panel_projects" class="tab-pane">
                <table class="table table-striped table-bordered table-full-width">
                    <thead>
                        <tr>
                            <th>
                                <p class="label label-default"><i class="fa fa-male"></i> Male : {{$gender[1]}}</p>
                            </th>
                            <th>
                                <p class="label label-success" style="background-color: pink; color: black;"><i class="fa fa-female"></i> Female : {{$gender[0]}}</p>
                            </th>
                            <th>
                                <p class="label label-inverse"><i class="clip-list"></i> Jumlah Peserta : {{ $total}}
                                </p>
                            </th>
                        </tr>
                    </thead>

                </table>
                <br>
                <table id="example" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class=" center">Foto</th>
                            <th class=" center">NIM</th>
                            <th class=" center">Nama Peserta</th>
                            <th class=" center">Status Pendaftaran</th>
                            <th class=" center"> Status Tahap 1 </th>
                            <th class=" center"> Status Tahap 2 </th>
                            <th class=" center"> Status Tahap 3 </th>
                            <th class=" center"> Action </th>


                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tampil as $tampil)
                        <tr>
                            <td class=" center">
                                <img class="circle-img" style="width: 50px;" src="https://krs.umm.ac.id/Poto/{{$tampil->peserta->angkatan}}/{{$tampil->peserta->nim}}.JPG" alt="">
                            </td>
                            <td class="center">{{$tampil->peserta->nim}}</td>
                            <td class="center">{{$tampil->peserta->nama}}</td>
                            <td class="center">
                                @if($tampil->status_pendaftaran == 'lolos')
                                <span class="label label-success">Lolos </span>
                                @elseif($tampil->status_pendaftaran == 'tidak lolos')
                                <span class="label label-danger">Tidak Lolos </span>
                                @elseif($tampil->status_pendaftaran == 'menunggu')
                                <span class="label label-default">Menunggu </span>
                                @endif
                            </td>
                            <td class="center">
                                @if($tampil->status_tahap1 == 'lolos')
                                <span class="label label-success">Lolos </span>
                                @elseif($tampil->status_tahap1 == 'tidak lolos')
                                <span class="label label-danger">Tidak Lolos </span>
                                @elseif($tampil->status_tahap1 == 'menunggu')
                                <span class="label label-default">Menunggu </span>
                                @endif
                            </td>
                            <td class="center">
                                @if($tampil->status_tahap2 == 'lolos')
                                <span class="label label-success">Lolos </span>
                                @elseif($tampil->status_tahap2 == 'tidak lolos')
                                <span class="label label-danger">Tidak Lolos </span>
                                @elseif($tampil->status_tahap2 == 'menunggu')
                                <span class="label label-default">Menunggu </span>
                                @endif
                            </td>
                            <td class="center">
                                @if($tampil->status_tahap3 == 'lolos')
                                <span class="label label-success">Lolos </span>
                                @elseif($tampil->status_tahap3 == 'tidak lolos')
                                <span class="label label-danger">Tidak Lolos </span>
                                @elseif($tampil->status_tahap3 == 'menunggu')
                                <span class="label label-default">Menunggu </span>
                                @endif
                            </td>
                            <td class="center">

                                <a onclick="detail('{{$tampil->id}}')" class="btn btn-info"><i class="clip-user-2"></i>
                                    <br>Detail</a>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        </table>
    </div>


    <!-- Detail -->
    <div id="form_detail" class="modal fade" tabindex="-1" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Update Status Peserta</h4>
        </div>

        <div class="modal-body">
            <form enctype="multipart/form-data" action="{{route('listpeserta.update')}}" method="POST">
                @method('put')
                @csrf

                <div class="row">
                    <div class="col-md-4">
                        <input type="hidden" class="form-control" name="id" id="id">
                        <div class="form-group">

                            <label for="session" class="control-label">Status Pendaftaran</label>
                            <select class="form-control" id="status_pendaftaran" name="status_pendaftaran">
                                <option value="lolos">Lolos</option>
                                <option value="tidak lolos">Tidak Lolos</option>
                                <option value="menunggu">Menunggu</option>
                            </select>
                            <label for="session" class="control-label">Status Tahap 1</label>
                            <select class="form-control" id="status_tahap1" name="status_tahap1">
                                <option value="lolos">Lolos</option>
                                <option value="tidak lolos">Tidak Lolos</option>
                                <option value="menunggu">Menunggu</option>
                            </select>
                            <label for="session" class="control-label">Status Tahap 2</label>
                            <select class="form-control" id="status_tahap2" name="status_tahap2">
                                <option value="lolos">Lolos</option>
                                <option value="tidak lolos">Tidak Lolos</option>
                                <option value="menunggu">Menunggu</option>
                            </select>
                            <label for="session" class="control-label">Status Tahap 3</label>
                            <select class="form-control" id="status_tahap3" name="status_tahap3">
                                <option value="lolos">Lolos</option>
                                <option value="tidak lolos">Tidak Lolos</option>
                                <option value="menunggu">Menunggu</option>
                            </select>

                        </div>
                    </div>
                </div>

                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Cancel
                </button>
                <button type="submit" class="btn btn-blue" value="submit">
                    <i class="fa fa-share"></i> Update
                </button>
            </form>
        </div>
    </div>



    <div id="form_edit_tahap_1" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Edit Tahap 1</h4>
        </div>
        <div class="modal-body">
            <form action="{{route('timeline.update_tahap1', $sesi->id)}}" method="POST">
                @method('put')
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="hidden" placeholder="Input your session" class="form-control" name="id" id="id">
                        </div>
                        <div class="form-group">
                            <label for="session" class="control-label">Tahap 1 Status:</label>
                            <div class="radio">
                                <label><input id="radio_dibuka" type="radio" name="status_tahap1" value="dibuka" @if($sesi->status_tahap1 == 'dibuka')
                                    {{'checked'}}
                                    @endif>ACTIVE</label>
                            </div>
                            <div class="radio">
                                <label><input id="radio_ditutup" type="radio" name="status_tahap1" value="ditutup" @if($sesi->status_tahap1 == 'ditutup')
                                    {{'checked'}}
                                    @endif>INACTIVE</label>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-blue" value="submit">
                Update
            </button>
        </div>
        </form>
    </div>


    @endsection

    @section('js')
    <script src="{{asset('js/ui-modals.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
    <script src="{{asset('js/table-data.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('js/form-elements.js')}}"></script>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> --}}
    <!--<![endif]-->
    <script src="{{asset('plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript">
    </script>
    <script src="{{asset('plugins/blockUI/jquery.blockUI.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/iCheck/jquery.icheck.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/perfect-scrollbar/src/jquery.mousewheel.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/perfect-scrollbar/src/perfect-scrollbar.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/less/less-1.5.0.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/jquery-cookie/jquery.cookie.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js')}}" type="text/javascript">
    </script>
    <script src="{{asset('js/main.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>


    <script>
        function detail(id) {
            $.ajax({
                url: "{{url('adm1n/sesi/timeline/detail')}}" + "/" + id,
                dataType: "json",
                success: function(status) {
                    $('#id').val(status.id);
                    $('#status_pendaftaran').val(status.status_pendaftaran);
                    $('#status_tahap1').val(status.status_tahap1);
                    $('#status_tahap2').val(status.status_tahap2);
                    $('#status_tahap3').val(status.status_tahap3);
                    $('#form_detail').modal('show');
                },
            });
        }

        jQuery(document).ready(function() {
            Main.init();
            TableData.init();
            // FormElements.init();
            // Index.init();
        });
    </script>



    @endsection