@extends('backend.sessions.oprec')

@section('oprec-css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('plugins/datepicker/css/datepicker.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('plugins/iCheck/skins/all.css')}}">
@endsection

@section('body')
@include('layouts.message')
@include('layouts.tahap3')
<form action="{{route('tahap3.update-kelulusan')}}" method="POST">
    @csrf
    <table class="table table-striped table-bordered table-full-width">
        <thead>
            <tr>
                <th style="width: 12%">
                    <a href="/adm1n/sesi/timeline/{{$sesi->id}}" class="btn btn-black"><i class="clip-arrow-left"></i> Back to Oprec {{$sesi->nama_oprec}}</a>
                </th>
                <th>
                    <input type=" number" name="oprec_id" value="{{$sesi->id}}" hidden>
                    Check actions:
                    <select id="form-field-select-1" name="status_kelulusan" class="form-control">
                        <option value="lolos">Lolos</option>
                        <option value="tidak lolos">Tidak Lolos</option>
                        <option value="menunggu">Menunggu</option>
                    </select>
                </th>
                <th style="width: 30%">
                    Actions: <br>
                    <button type="submit" class="btn btn-success"><i class="clip-upload"></i> Update Peserta</button>
                </th>
            </tr>
        </thead>
    </table>
    <br>
    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
        <thead>
            <tr>
                <th>No.</th>
                <th style="width: 5%">Picture</th>
                <th>Name</th>
                <th>Tahap 2</th>
                <th>Wawancara 2</th>
                <th>Project</th>
                <th>Magang</th>
                <th>Final Score</th>
                <th>Status Tahap 3</th>
                <th><input type="checkbox" id="selectAll" /></th>
                <th style="width: 30%">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1; ?>
            @foreach ($pesertas as $peserta)
            <tr>
                <td align="center">
                    {{$no}}
                </td>
                <td align="center">
                    <div class="profile-image">
                        <img class="circle-img" style="width: 50px;" src="https://krs.umm.ac.id/Poto/{{$peserta->peserta->angkatan}}/{{$peserta->peserta->nim}}.JPG" alt="">
                    </div>
                </td>
                <td>{{$peserta->peserta->nama}}</td>
                <td align="center">
                    @if(isset($peserta->peserta->tahap2[0]))
                    {{$peserta->peserta->tahap2[0]->score_total_tahap_2}}
                    @endif
                </td>
                <td align="center">
                    @if(isset($peserta->peserta->tahap3[0]))
                    {{($peserta->peserta->tahap3->where('oprec_id','=',$peserta->oprec_id))[0]->score_total_wawancara_2}}
                    @endif
                </td>
                <td align="center">
                    @if(isset($peserta->peserta->tahap3[0]))
                    {{($peserta->peserta->tahap3->where('oprec_id','=',$peserta->oprec_id))[0]->score_total_project}}
                    @endif
                </td>
                <td align="center">
                    @if(isset($peserta->peserta->tahap3[0]))
                    {{($peserta->peserta->tahap3->where('oprec_id','=',$peserta->oprec_id))[0]->score_total_magang}}
                    @endif
                </td>
                <td align="center">
                    @if(isset($peserta->peserta->tahap3[0]))
                    {{($peserta->peserta->tahap3->where('oprec_id','=',$peserta->oprec_id))[0]->score_total_tahap_3}}
                    @endif
                </td>
                <td align="center">
                    @if(isset($peserta->peserta->tahap2[0]))
                    {{$peserta->status_tahap3}}
                    @endif
                </td>
                <td align="center">
                    @if(isset($peserta->peserta->tahap2[0]))
                    <input type="checkbox" name="id[]" value="{{ $peserta->id }}" /></td>
                @endif
                <td align="center">
                    @if(isset($peserta->peserta->tahap2[0]))
                    <div class="col-md-4">
                        <a href="/adm1n/sesi/{{$peserta->oprec_id}}/tahap3/wawancara2/{{$peserta->peserta_id}}" class="btn btn-info"><i class="clip-user-2"></i><br> Wawancara 2</a>
                    </div>
                    <div class="col-md-3">
                        <a href="/adm1n/sesi/{{$peserta->oprec_id}}/tahap3/project/{{$peserta->peserta_id}}" class="btn btn-yellow"><i class="clip-copy"></i><br> Project</a>
                    </div>
                    <div class="col-md-3">
                        <a href="/adm1n/sesi/{{$peserta->oprec_id}}/tahap3/magang/{{$peserta->peserta_id}}" class="btn btn-green"><i class="fa fa-star"></i><br> Magang</a>
                    </div>
                    @else
                    Nilai tahap 2 belum di set
                    @endif
                </td>
            </tr>
            <?php $no++; ?>
            @endforeach
        </tbody>
    </table>
</form>
</div>

<div id="wawancara_modal" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Wawancara 2 : {{$sesi->nama_oprec}}</h4>
    </div>
    <form action="{{route('tahap3.add.kriteria.wawancara2')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
                        <label for="session" class="control-label">Nama Kriteria: </label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Prosentase: </label>
                        <input type="number" min="0" class="form-control" name="prosentase">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>

<div id="project_modal" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Project : {{$sesi->nama_oprec}}</h4>
    </div>
    <form action="{{route('tahap3.add.kriteria.project')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
                        <label for="session" class="control-label">Nama Kriteria: </label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Prosentase: </label>
                        <input type="number" min="0" class="form-control" name="prosentase">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>

<div id="pengumuman_tahap3_modal" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Create Pengumuman : {{$sesi->nama_oprec}}</h4>
    </div>
    <form action="{{route('tahap3.add.pengumumantahap3')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
                        <label for="session" class="control-label">Pengumuman : </label>
                        <textarea rows="5" cols="50" name="namapengumumantahap3"> </textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>

<div id="pengumuman_tahap3tdklolos_modal" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Create Pengumuman : {{$sesi->nama_oprec}}</h4>
    </div>
    <form action="{{route('tahap3.add.pengumumantahap3tdklolos')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
                        <label for="session" class="control-label">Pengumuman : </label>
                        <textarea rows="5" cols="50" name="namapengumumantahap3tdklolos"> </textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>

<div id="magang_modal" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Magang : {{$sesi->nama_oprec}}</h4>
    </div>
    <form action="{{route('tahap3.add.kriteria.magang')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
                        <label for="session" class="control-label">Nama Kriteria: </label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Prosentase: </label>
                        <input type="number" min="0" class="form-control" name="prosentase">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>


{{-- modal delete --}}
<div id="delete_project" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Kriteria Project : {{$sesi->nama_oprec}}</h4>
    </div>
    <div class="modal-body" id="form_delete_project">
        <p>
            Are you sure to delete session ?
        </p>
    </div>
    <div class="modal-footer">
        <form id="delete_project_form" action="{{route('tahap3.delete.kriteria.project')}}" method="post">
            @csrf
            @method('delete')
            <input type="hidden" id="id_delete_project" value="" name="id">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                No
            </button>
            <button type="submit" class="btn btn-danger" value="submit">
                Yes
            </button>
        </form>
    </div>
</div>

{{-- modal delete --}}
<div id="delete_magang" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Kriteria Magang : {{$sesi->nama_oprec}}</h4>
    </div>
    <div class="modal-body" id="form_delete_magang">
        <p>
            Are you sure to delete session ?
        </p>
    </div>
    <div class="modal-footer">
        <form id="delete_magang_form" action="{{route('tahap3.delete.kriteria.magang')}}" method="post">
            @csrf
            @method('delete')
            <input type="hidden" id="id_delete_magang" value="" name="id">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                No
            </button>
            <button type="submit" class="btn btn-danger" value="submit">
                Yes
            </button>
        </form>
    </div>
</div>

{{-- modal delete --}}
<div id="delete_wawancara2" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Kriteria Wawancara 1 : {{$sesi->nama_oprec}}</h4>
    </div>
    <div class="modal-body" id="form_delete_wawancara2">
        <p>
            Are you sure to delete session ?
        </p>
    </div>
    <div class="modal-footer">
        <form action="{{route('tahap3.delete.kriteria.wawancara2')}}" method="post">
            @csrf
            @method('delete')
            <input type="hidden" id="id_delete_wawancara2" value="" name="id">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                No
            </button>
            <button type="submit" class="btn btn-danger" value="submit">
                Yes
            </button>
        </form>
    </div>
</div>
@endsection

@section('oprec-js')
<script type="text/javascript">
    $('#selectAll').click(function(e) {
        var table = $(e.target).closest('table');
        $('td input:checkbox', table).prop('checked', this.checked);
    });

    function hapusProject(id, name) {
        $('#form_delete_project p').text("Are yo sure to delete " + name + " ?");
        $('#id_delete_project').val(id);
        $('#delete_project').modal('show');
    }

    function hapusMagang(id, name) {
        $('#form_delete_magang p').text("Are yo sure to delete " + name + " ?");
        $('#id_delete_magang').val(id);
        $('#delete_magang').modal('show');
    }

    function hapusWawancara2(id, name) {
        $('#form_delete_wawancara2 p').text("Are yo sure to delete " + name + " ?");
        $('#id_delete_wawancara2').val(id);
        $('#delete_wawancara2').modal('show');
    }
</script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
<script src="{{asset('js/table-data.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('js/form-elements.js')}}"></script>
@endsection
