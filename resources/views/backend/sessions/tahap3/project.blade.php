@extends('backend.sessions.oprec')

@section('oprec-css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('plugins/datepicker/css/datepicker.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('plugins/iCheck/skins/all.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css" />
@endsection

@section('body')
<div class="col-md-12">
    <table class="table table-striped table-bordered table-full-width">
        <thead>
                <tr>
                    <th style="width: 30% ; text-align:center">
                        <a href="/adm1n/tahap3/detail/{{$sesi->id}}" class="btn btn-lg"><i class="clip-arrow-left"></i> Back to list peserta tahap 3</a>
                    </th>
                    <th style="text-align:center">{{$peserta[0]->peserta->nama}}
                    </th>
                    <th style="text-align:center">{{$peserta[0]->peserta->nim}}
                    </th>
                    <th style="text-align:center">
                        <div class="profile-image">
                            <img class="circle-img" style="width: 100px;" src="https://krs.umm.ac.id/Poto/{{$peserta[0]->peserta->angkatan}}/{{$peserta[0]->peserta->nim}}.JPG" alt="">
                        </div>
                    </th>
                </tr>
        </thead>
    </table>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- start: FORM VALIDATION 1 PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> Pengisian Nilai Tahap 3
            </div>
            <div class="panel-body">
                <form action="{{route('peserta.tahap3.project.update')}}" method="POST">
                    @csrf
                    <div class="row">
                        @include('layouts.message')
                        <input type="number" name="peserta_id" value="{{$peserta[0]->peserta->id}}" hidden>
                        <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
                        <div class="col-md-12">
                            <h3>Nilai project</h3>
                            @foreach($kt_project as $project)
                            <?php
                            $nilai = App\NilaiProject::where('peserta_id', '=',
                            $peserta[0]->peserta->id)->where('project_id', '=', $project->id)->get()->first();
                            ?>
                            <div class="form-group">
                                <label class="control-label">
                                    {{$project->nama_kriteria}} : {{$project->prosentase}} % <span class="symbol required" aria-required="true"></span>
                                </label>
                                <input name="id_kt_project[]" type="number" value="{{$project->id}}" hidden>
                                @if($nilai!=null)
                                <div class="range-slider">
                                    <input name="kt_project[]" type="number" class="js-range-slider" value="{{$nilai->nilai}}" />
                                </div>
                                @else
                                <div class="range-slider">
                                    <input name="kt_project[]" type="number" class="js-range-slider" value="" />
                                </div>
                                @endif
                            </div>
                            @endforeach
                            <div class="form-group">
                                <label class="control-label">
                                    Catatan project
                                </label>
                                <?php
                                $catatan = App\Tahap3::where('peserta_id', '=', $peserta[0]->peserta->id)->where('oprec_id', '=', $sesi->id)->get()->first();
                                ?>
                                @if($catatan!=null)
                                <textarea name="catatan_project" type="text" class="form-control">{{$catatan->catatan_project}}</textarea>
                                @else
                                <textarea name="catatan_project" type="text" class="form-control"></textarea>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <span class="symbol required" aria-required="true"></span>Required Fields
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="form-group pull-right">
                            <button class="btn btn-info btn-lg" type="submit">
                                    Submit Nilai Project <i class="fa fa-arrow-circle-right"></i>
                            </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- end: FORM VALIDATION 1 PANEL -->
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    jQuery(document).ready(function() {
        Main.init();
        var $range = $(".js-range-slider");
        var values = [0, 50, 100];
        var values_p = ["bad", "good", "best"];
        $range.ionRangeSlider({
            type: "single",
            skin: "round",
            grid: true,
            values: values,
            prettify: function(n) {
                var ind = values.indexOf(n);
                return values_p[ind];
            },
        });
    });
</script>
@endsection

@section('oprec-js')
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
<script src="{{asset('js/table-data.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('js/form-elements.js')}}"></script>
@endsection
