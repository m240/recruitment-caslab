@extends('backend.sessions.oprec')

@section('oprec-css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/DataTables/media/css/DT_bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('plugins/datepicker/css/datepicker.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('plugins/iCheck/skins/all.css')}}">
@endsection

@section('body')
@include('layouts.message')
@include('layouts.tahap2')
<form action="{{route('tahap2.update-kelulusan')}}" method="POST">
    @csrf
    <table class="table table-striped table-bordered table-full-width">
        <thead>
            <tr>
                <th style="width: 15%">
                    <a href="/adm1n/sesi/timeline/{{$sesi->id}}" class="btn btn-black"><i class="clip-arrow-left"></i> Back to {{$sesi->nama_oprec}}</a>
                </th>
                <th>
                    <input type=" number" name="oprec_id" value="{{$sesi->id}}" hidden>
                    Check actions:
                    <select id="form-field-select-1" name="status_kelulusan" class="form-control">
                        <option value="lolos">Lolos</option>
                        <option value="tidak lolos">Tidak Lolos</option>
                        <option value="menunggu">Menunggu</option>
                    </select>
                </th>
                <th style="width: 30%">
                    Actions: <br>
                    <button type="submit" class="btn btn-success"><i class="clip-upload"></i> Update Peserta</button>
                </th>
            </tr>
        </thead>
    </table>
    <br>
    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
        <thead>
            <tr>
                <th>No.</th>
                <th style="width: 5%">Picture</th>
                <th>Name</th>
                <th>Nilai Tahap 1</th>
                <th>Score Microteaching</th>
                <th>Score Wawancara 1</th>
                <th>Score Total Tahap 2</th>
                <th>Status Tahap 2</th>
                <th><input type="checkbox" id="selectAll" /></th>
                <th style="width: 25%">Actions</th>
                <th style="width: 15%">Tema</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1; ?>
            @foreach ($pesertas as $peserta)
            <tr>
                <td>{{$no}}</td>
                <td>
                    <div class="profile-image">
                        <img class="circle-img" style="width: 50px;" src="https://krs.umm.ac.id/Poto/{{$peserta->peserta->angkatan}}/{{$peserta->peserta->nim}}.JPG" alt="">
                    </div>
                </td>
                <td>{{$peserta->peserta->nama}}</td>
                <td>
                    @if(isset($peserta->peserta->tahap1[0]))
                    {{$peserta->peserta->tahap1[0]->score_total}}
                    @endif
                </td>
                <td>
                    @if(isset($peserta->peserta->tahap2[0]))
                    {{($peserta->peserta->tahap2->where('oprec_id','=',$peserta->oprec_id))[0]->score_total_microteaching}}
                    @endif
                </td>
                <td>
                    @if(isset($peserta->peserta->tahap2[0]))
                    {{($peserta->peserta->tahap2->where('oprec_id','=',$peserta->oprec_id))[0]->score_total_wawancara_1}}
                    @endif
                </td>
                <td>
                    @if(isset($peserta->peserta->tahap2[0]))
                    {{($peserta->peserta->tahap2->where('oprec_id','=',$peserta->oprec_id))[0]->score_total_tahap_2}}
                    @endif
                </td>
                <td>
                    @if(isset($peserta->peserta->tahap1[0]))
                    {{$peserta->status_tahap2}}
                    @endif
                </td>
                <td>
                    @if(isset($peserta->peserta->tahap1[0]))
                    <input type="checkbox" name="id[]" value="{{ $peserta->id }}" /></td>
                @endif
                <td>
                    @if(isset($peserta->peserta->tahap1[0]))
                    <div class="col-md-6">
                        <a href="/adm1n/sesi/{{$peserta->oprec_id}}/tahap2/wawancara1/{{$peserta->peserta_id}}" class="btn btn-primary"><i class="clip-user-2"></i><br> Wawancara 1</a>
                    </div>
                    <div class="col-md-6">
                        <a href="/adm1n/sesi/{{$peserta->oprec_id}}/tahap2/microteaching/{{$peserta->peserta_id}}" class="btn btn-purple"><i class="fa fa-star"></i><br> Microteaching</a>
                    </div>
                    @else
                    Nilai tahap 1 belum di set
                    @endif
                </td>
                <td>
                    @if(isset($peserta->peserta->tahap2[0]->tema)!=null)
                    <a onclick="updateTema('{{$peserta->peserta_id}}','{{$peserta->oprec_id}}','{{$peserta->peserta->nama}}','{{$peserta->peserta->tahap2[0]->tema}}')" style="margin: 3px" class="btn btn-black" role="button">
                        {{str_limit($peserta->peserta->tahap2[0]->tema, $limit = 10, $end = '...')}}
                    </a>
                    @else
                    <a onclick="tema('{{$peserta->peserta_id}}','{{$peserta->oprec_id}}','{{$peserta->peserta->nama}}')" style="margin: 3px" class="btn btn-info" role="button">
                        Tema <br> belum diset
                    </a>
                    @endif
                </td>
            </tr>
            <?php $no++; ?>
            @endforeach
        </tbody>
    </table>
</form>
</div>

<div id="tema_modal" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Tema microteaching</h4>
    </div>
    <form action="{{route('tahap2.add.tema')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="number" name="oprec_id" id="oprec_id" value="" hidden>
                        <input type="number" name="peserta_id" id="peserta_id" value="" hidden>
                        <label id="judul_tema" class="control-label">judul tema: </label>
                        <input id="input_tema" type="text" class="form-control" name="judul_tema">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>

<div id="pengumuman_tahap2_modal" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Create Pengumuman : {{$sesi->nama_oprec}}</h4>
    </div>
    <form action="{{route('tahap2.add.pengumumantahap2')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
                        <label for="session" class="control-label">Pengumuman : </label>
                        <textarea rows="5" cols="50" name="namapengumumantahap2"> </textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>

<div id="wawancara_modal" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Kriteria Wawancara 1 : {{$sesi->nama_oprec}}</h4>
    </div>
    <form action="{{route('tahap2.add.kriteria.wawancara1')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
                        <label for="session" class="control-label">Nama Kriteria: </label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Persentase: </label>
                        <input type="number" min="0" class="form-control" name="prosentase">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>

<div id="microteaching_modal" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Kriteria Microteaching : {{$sesi->nama_oprec}}</h4>
    </div>
    <form action="{{route('tahap2.add.kriteria.microteaching')}}" method="POST">
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="number" name="oprec_id" value="{{$sesi->id}}" hidden>
                        <label for="session" class="control-label">Nama Kriteria: </label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label for="session" class="control-label">Persentase: </label>
                        <input type="number" min="0" class="form-control" name="prosentase">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
            <button type="submit" class="btn btn-blue" value="submit">
                Submit
            </button>
        </div>
    </form>
</div>


{{-- modal delete --}}
<div id="delete_microteaching" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Kriteria Microteaching : {{$sesi->nama_oprec}}</h4>
    </div>
    <div class="modal-body" id="form_delete_microteaching">
        <p>
            Are you sure to delete session ?
        </p>
    </div>
    <div class="modal-footer">
        <form id="delete_microteaching_form" action="{{route('tahap2.delete.kriteria.microteaching')}}" method="post">
            @csrf
            @method('delete')
            <input type="hidden" id="id_delete_micro" value="" name="id">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                No
            </button>
            <button type="submit" class="btn btn-danger" value="submit">
                Yes
            </button>
        </form>
    </div>
</div>


{{-- modal delete --}}
<div id="delete_wawancara1" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Kriteria Wawancara 1 : {{$sesi->nama_oprec}}</h4>
    </div>
    <div class="modal-body" id="form_delete_wawancara1">
        <p>
            Are you sure to delete session ?
        </p>
    </div>
    <div class="modal-footer">
        <form action="{{route('tahap2.delete.kriteria.wawancara1')}}" method="post">
            @csrf
            @method('delete')
            <input type="hidden" id="id_delete_wwa" value="" name="id">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                No
            </button>
            <button type="submit" class="btn btn-danger" value="submit">
                Yes
            </button>
        </form>
    </div>
</div>
@endsection

@section('oprec-js')
<script type="text/javascript">
    $('#selectAll').click(function(e) {
        var table = $(e.target).closest('table');
        $('td input:checkbox', table).prop('checked', this.checked);
    });

    function hapusMicroteaching(id, name) {
        $('#form_delete_microteaching p').text("Are yo sure to delete " + name + " ?");
        $('#id_delete_micro').val(id);
        $('#delete_microteaching').modal('show');
    }

    function hapusWawancara1(id, name) {
        $('#form_delete_wawancara1 p').text("Are yo sure to delete " + name + " ?");
        $('#id_delete_wwa').val(id);
        $('#delete_wawancara1').modal('show');
    }

    function tema(peserta, oprec, nama) {
        $('#judul_tema').text("Add tema for " + nama + " : ");
        $('#oprec_id').val(oprec);
        $('#peserta_id').val(peserta);
        $('#input_tema').val("");
        $('#tema_modal').modal('show');
    }

    function updateTema(peserta, oprec, nama, tema) {
        $('#judul_tema').text("Update tema for " + nama + " : ");
        $('#oprec_id').val(oprec);
        $('#peserta_id').val(peserta);
        $('#input_tema').val(tema);
        $('#tema_modal').modal('show');
    }
</script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modal.js')}}"></script>
<script src="{{asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}"></script>
<script src="{{asset('js/ui-modals.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
<script src="{{asset('js/table-data.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('js/form-elements.js')}}"></script>
@endsection