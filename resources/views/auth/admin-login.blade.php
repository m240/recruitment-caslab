<!DOCTYPE html>
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- start: HEAD -->

<head>
	<title>SIRENTA - Sistem Recruitment Asisten Lab.</title>
	<!-- start: META -->
	<meta charset="utf-8" />
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link rel="icon" href="{{asset('images/logo-sirenta.jpg')}}">
	<!-- end: META -->
	<!-- start: MAIN CSS -->
	<link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('fonts/style.css')}}">
	<link rel="stylesheet" href="{{asset('css/main.css')}}">
	<link rel="stylesheet" href="{{asset('css/main-responsive.css')}}">
	<link rel="stylesheet" href="{{asset('plugins/iCheck/skins/all.css')}}">
	<link rel="stylesheet" href="{{asset('plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css')}}">
	<link rel="stylesheet" href="{{asset('plugins/perfect-scrollbar/src/perfect-scrollbar.css')}}">
	<link rel="stylesheet" href="{{asset('css/theme_light.css')}}" type="text/css" id="skin_color">
	<link rel="stylesheet" href="{{asset('css/print.css')}}" type="text/css" media="print" />
	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	<link rel="stylesheet" href="{{asset('css/peserta-login/peserta-login.css')}}" type="text/css"/>
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>
<!-- end: HEAD -->
<!-- start: BODY -->

<body>
	<div class="container">
		<div class="row">
			<div class="col-sm" style="float:left;">
				<img src="{{asset('images/logo-sirenta.png')}}" alt="SIRENTA" class="logo">
				<div class="copyright">
					<center>
						<p>
							&copy; Copyright 2019. Made with
							<span style="color: #e25555;">&#9829;</span>
							by Tim Riset Infotech UMM
						</p>
					</center>
				</div>
				<div class="col-sm login-container" style="border-color: #5E97F6;">
				<ul class="circles">
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
					</ul>
					<div class="box-login">
						<center>
							<img src="{{asset('images/rscLogin/avatar-admin.png')}}" alt="Login" style="width: 20%; height: 20%; margin: 5%;">
							    <form id="form-login" role="form" class="form-login" action="{{route('admin.login.submit')}}" method="POST">
								@csrf
								<fieldset>
                                    @if (Session::has('message'))
                                        <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                    @endif
									<div class="form-group">
										<div class="errorHandler alert alert-danger" style="display: none;" role="warning" id="notif">
										</div>
									</div>
									<div class="form-group {{ $errors->has('username') ? 'has-error' : ''}} input-sm">
										<span class="input-icon">
											<input type="text" class="form-control" id="username" name="username" placeholder="Username">
                                            <i class="fa fa-user"></i>
                                            
                                            @if ($errors->has('username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                            @endif
										</span>
									</div>
									<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}} input-sm">
										<span class="input-icon">
											<input type="password" class="form-control password" id="password" name="password" placeholder="Kata Sandi">
                                            <i class="fa fa-lock"></i>
                                            
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
									</div>
									<div class="form-group">
										<label style="color: white;"> Masuk menggunakan Username dan Password dari akun I-LAB (infotech.umm.ac.id)</label>
									</div>
									<div class="form-actions">
                                        <button type="button" onClick="window.location.href='{{route('login')}}'" class="btn btn-warning" style="float: left">
											<i class="fa fa-arrow-circle-left"></i> PESERTA
										</button>
                                        <button type="button" onClick="sendForm()" class="btn btn-primary" style="float: right">
											MASUK <i class="fa fa-arrow-circle-right"></i>
                                        </button>
									</div>
								</fieldset>
							</form>
						</center>
						<!-- end: LOGIN BOX -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		function sendForm() {
			var username = document.getElementById("username");
			var password = document.getElementById("password");
			var form_login = document.getElementById("form-login");
			var notif = document.getElementById("notif");
			if (username.value == 0 || password.value == 0) {
				notif.innerHTML = "Username atau Kata Sandi tidak boleh kosong";
				notif.style.display = "block";
				notif.alert();
				return;
			}
			notif.style.display = "none";
			form_login.submit();
		}
	</script>
	<!-- start: MAIN JAVASCRIPTS -->
	<!--[if lt IE 9]>
		<script src="{{asset('plugins/respond.min.js')}}"></script>
		<script src="{{asset('plugins/excanvas.min.js')}}"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<![endif]-->
	<!--[if gte IE 9]><!-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<!--<![endif]-->
	<script src="{{asset('plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js')}}"></script>
	<script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('plugins/less/less-1.5.0.min.js')}}"></script>
	<script src="{{asset('plugins/jquery-cookie/jquery.cookie.js')}}"></script>
	<script src="{{asset('js/main.js')}}"></script>
	<!-- end: MAIN JAVASCRIPTS -->
	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="{{asset('plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
	<script src="{{asset('js/login.js')}}"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
		jQuery(document).ready(function() {
			Main.init();
			Login.init();
			FormElements.init();
		});
	</script>
</body>
<!-- end: BODY -->

</html>