@extends('layouts.panitia')
@section('content')
    <style>
    .uper {
        margin-top: 40px;
    }
    </style>
        <div class="uper">
        @if(session()->get('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}  
            </div><br />
        @endif
            <table class="table table-striped">
        <thead>
            <tr>
                <td>Name</td>
                <td>NIM</td>
                <td>Email</td>
                <td>Motivasi</td>
                <td>Ide Kreatif</td>
                <td>Address</td>
                <td>Tempat Lahir</td>
                <td>Tanggal Lahir</td>
                <td colspan="2">Action</td>
            </tr>
        </thead>
    <tbody>
            @foreach($pesertas as $peserta)
            <tr>
                <td>{{$peserta->name}}</td>
                <td>{{$peserta->nim}}</td>
                <td>{{$peserta->email}}</td>
                <td>{{$peserta->motivasi}}</td>
                <td>{{$peserta->ide_kreatif}}</td>
                <td>{{$peserta->address}}</td>
                <td>{{$peserta->tempat_lahir}}</td>
                <td>{{$peserta->tanggal_lahir}}</td>
                

            </tr>
            @endforeach
            </tbody>
        </table>
@endsection
