var options = {
	valueNames: [
		'name',
		'angkatan',
		'motivasi',
		{ data: ['gender']}
	],
	page: 3,
	pagination: true
};
var userList = new List('users', options);

function resetList(){
	userList.search();
	userList.filter();
	userList.update();
	$(".filter-all").prop('checked', true);
	$('.filter').prop('checked', false);
	$('.search').val('');
	//console.log('Reset Successfully!');
};
  
function updateList(){
    var values_gender = $("#name_profile:checked").val();
	var values_motivasi = $("#motivasi_profile:checked").val();
	console.log(values_gender, values_motivasi);

	userList.filter(function (item) {
		var genderFilter = false;
		var motivasiFilter = false;
		
		if(values_gender == "all")
		{ 
			genderFilter = true;
		} else {
			genderFilter = item.values().gender == values_gender;
			
		}
		if(values_motivasi == null)
		{ 
			motivasiFilter = true;
		} else {
			motivasiFilter = item.values().motivasi.indexOf(values_motivasi) >= 0;
		}
		return motivasiFilter && genderFilter
	});
	userList.update();
	//console.log('Filtered: ' + values_gender);
}
                               
$(function(){
  //updateList();
  $("#name_profile").change(updateList);
	$('#motivasi_profile').change(updateList);
	
	userList.on('updated', function (list) {
		if (list.matchingItems.length > 0) {
			$('.no-result').hide()
		} else {
			$('.no-result').show()
		}
	});
});