<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('adm1n/sesi')->group(function() {

	/*--------------------------------- Timeline ---------------------------------*/
	Route::get('timeline/aktif/{id}','PendaftaranController@aktif')->name('pendaftaran.aktif');
	Route::get('timeline/nonaktif/{id}','PendaftaranController@nonaktif')->name('pendaftaran.tutup');
	Route::get('timeline/detail/{id}', 'PendaftaranController@detail');
	Route::put('timeline/update', 'KelolaTimelineController@update')->name('listpeserta.update');
	

	/*--------------------------------- Pendaftaran ---------------------------------*/
	Route::get('pendaftaran/{id}','PendaftaranController@index')->name('lolos.pendaftaran');
	Route::get('pendaftaran/detail/{id}', 'PendaftaranController@detail');
	Route::put('pendaftaran/update', 'PendaftaranController@update')->name('update.pendaftaran');

});

Route::prefix('peserta')->middleware('login')->group(function () {
	Route::get('daftar/detail/skill/{id}', 'KelolaPesertaController@skillPeserta');
	Route::get('daftar/detail/{id}', 'KelolaPesertaController@detail');
	Route::put('daftar/edit', 'KelolaDataController@update')->name('inputskill.update');



});


