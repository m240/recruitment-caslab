<?php

//progress
Route::get('/timeline/tahap2/aktif/{id}', 'AktifTahapController@aktif_tahap2')->name('aktiftahap.aktif_tahap2');
Route::get('/timeline/tahap2/nonaktif/{id}', 'AktifTahapController@nonaktif_tahap2')->name('aktiftahap.tutup_tahap2');
Route::get('/timeline/tahap3/aktif/{id}', 'AktifTahapController@aktif_tahap3')->name('aktiftahap.aktif_tahap3');
Route::get('/timeline/tahap3/nonaktif/{id}', 'AktifTahapController@nonaktif_tahap3')->name('aktiftahap.tutup_tahap3');

Route::get('/peserta/showpengumumantahap2/{id}', 'PengumumanController@showPengumumantahap2');
Route::post('/tahap2/detail/add/pengumumanTahap2', 'PengumumanController@addPengumumanTahap2')->name('tahap2.add.pengumumantahap2');


Route::get('peserta/showpengumumantahap3/{id}', 'PengumumanController@showPengumumantahap3');
Route::get('peserta/showpengumumantahap3tdklolos/{id}', 'PengumumanController@showPengumumantahap3TdkLolos');
Route::post('/tahap3/detail/add/pengumumanTahap3', 'PengumumanController@addPengumumanTahap3')->name('tahap3.add.pengumumantahap3');
Route::post('/tahap3/detail/add/pengumumanTahap3tdklolos', 'PengumumanController@addPengumumanTahap3TdkLolos')->name('tahap3.add.pengumumantahap3tdklolos');
