<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('peserta')->middleware('login')->group(function () {
	Route::get('/kelola-data', 'PendaftaranCaslabController@index')->name('peserta.kelola-data');
	Route::get('/progres','PendaftaranCaslabController@progres')->name('peserta.progres');
});