<?php

Route::prefix('peserta')->middleware('login')->group(function () {
    Route::get('dashboard', 'PesertaController@index')->name('peserta.index');
    Route::get('tahap-1', 'PesertaController@tahapSatu')->name('peserta.tahap1');
    Route::post('tahap-1/create', 'PesertaController@storeJawaban')->name('peserta.store');
    Route::post('tahap-1/final', 'PesertaController@finalScore')->name('peserta.tahap1.final');

});

Route::prefix('adm1n')->group(function () {
    Route::get('sesi', 'KelolaSesiController@index')->name('sesi.index');
    Route::post('sesi/create', 'KelolaSesiController@store')->name('sesi.store');
    Route::put('sesi/update', 'KelolaSesiController@update')->name('sesi.update');
    Route::delete('sesi/delete', 'KelolaSesiController@delete')->name('sesi.delete');

    Route::get('peserta', 'KelolaPesertaController@index')->name('kelolapeserta.index');
    Route::post('peserta/create', 'KelolaPesertaController@store')->name('kelolapeserta.store');
    Route::put('peserta/update', 'KelolaPesertaController@update')->name('kelolapeserta.update');
    Route::delete('peserta/delete', 'KelolaPesertaController@delete')->name('kelolapeserta.delete');

    // route js
    Route::get('peserta/detail/{id}', 'KelolaPesertaController@detail');
    Route::get('sesi/detail/{id}', 'KelolaSesiController@detail');
});

Route::prefix('adm1n/sesi')->group(function()
{
    // timeline
    Route::get('timeline/{id}', 'KelolaTimelineController@index')->name('timeline.index');
    Route::put('timeline/update/{id}', 'KelolaSesiController@updateTahap1')->name('timeline.update_tahap1');

    // Questions
    Route::prefix('tahap1')->group(function () {
        Route::get('hasil/{id}', 'KelolaTahap1Controller@index')->name('tahap1.index');
        Route::get('pertanyaan', 'KelolaTahap1Controller@pertanyaan')->name('tahap1.pertanyaan');
        Route::post('pertanyaan/create', 'KelolaTahap1Controller@store')->name('pertanyaan.store');
        Route::put('pertanyaan/update', 'KelolaTahap1Controller@update')->name('pertanyaan.update');
        Route::delete('pertanyaan/destroy', 'KelolaTahap1Controller@destroy')->name('pertanyaan.destroy');

        Route::post('category/create', 'KelolaTahap1Controller@store_category')->name('category.store');
        Route::delete('category/destroy', 'KelolaTahap1Controller@destroy_category')->name('category.destroy');
        Route::put('category/update', 'KelolaTahap1Controller@update_category')->name('category.update');

        Route::put('peserta/status/edit', 'KelolaTahap1Controller@updateStatusPeserta')->name('tahap1.peserta.status');
        
        // Route JS
        Route::get('pertanyaan-pilihan/detail/{id}', 'KelolaTahap1Controller@detail');
        Route::get('pertanyaan/detail/{id}', 'KelolaTahap1Controller@detailDelete');
        Route::get('hasil-akhir/{id}', 'KelolaTahap1Controller@anyData')->name('hasil-akhir.data');
        Route::get('category/detail/{id}', 'KelolaTahap1Controller@detail_category');
        Route::get('peserta/status/{id}', 'KelolaTahap1Controller@detailPeserta');
        
    });

});
