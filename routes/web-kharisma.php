<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
//Route Admin
Route::prefix('adm1n')->group(function () {
    // dashboard admin
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/index', 'AdminController@index')->name('admin.dashboard');
    Route::get('/', 'AdminController@index');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    // Manage Panitia
    Route::get('manage-panitia', 'KelolaPanitiaController@index')->name('manage-panitia.index');
    Route::post('manage-panitia/create', 'KelolaPanitiaController@store')->name('manage-panitia.store');
    Route::put('manage-panitia/update', 'KelolaPanitiaController@update')->name('manage-panitia.update');
    Route::delete('manage-panitia/delete', 'KelolaPanitiaController@delete')->name('manage-panitia.delete');
    Route::get('manage-panitia/detail/{id}', 'KelolaPanitiaController@detail');
    Route::get('/peserta/detail/skill/{id}', 'KelolaPesertaController@skillPeserta');
    //timeline
    Route::get('sesi/timeline/tambah_pendaftaran/{id}', 'KelolaSesiController@addPendaftaran')->name('timeline.sesi.tambah_pendaftaran');
    Route::get('sesi/timeline/tambah_tahap2/{id}', 'Tahap2Controller@addTahap2')->name('timeline.sesi.tambah_tahap2');
    //timeline
    Route::get('/tahap2/detail/{id}', 'Tahap2Controller@index');
    Route::post('/tahap2/detail/update', 'Tahap2Controller@updateKelulusan')->name('tahap2.update-kelulusan');

    //kriteria tahap 2
    Route::post('/tahap2/detail/add/wawancara1', 'Tahap2Controller@addKriteriaWawancara1')->name('tahap2.add.kriteria.wawancara1');
    Route::post('/tahap2/detail/add/microteaching', 'Tahap2Controller@addMicroteaching')->name('tahap2.add.kriteria.microteaching');
    Route::post('/tahap2/detail/add/tema', 'Tahap2Controller@addTema')->name('tahap2.add.tema');

    Route::delete('/tahap2/detail/delete/wawancara1', 'Tahap2Controller@deleteKriteriaWawancara1')->name('tahap2.delete.kriteria.wawancara1');
    Route::delete('/tahap2/detail/delete/microteaching', 'Tahap2Controller@deleteKriteriaMicroteaching')->name('tahap2.delete.kriteria.microteaching');
    Route::post('/tahap2/detail/set/totalTahap2', 'Tahap2Controller@setTotalTahap2')->name('tahap2.set.total.tahap2');

    //set nilai peserta tahap 2
    Route::get('/sesi/{oprec_id}/tahap2/wawancara1/{peserta_id}', 'Tahap2Controller@detailWawancara1');
    Route::get('/sesi/{oprec_id}/tahap2/microteaching/{peserta_id}', 'Tahap2Controller@detailMicroteaching');

    Route::post('/sesi/tahap2/wawancara1/update', 'Tahap2Controller@updateNilaiWawancara1')->name('peserta.tahap2.wawancara1.update');
    Route::post('/sesi/tahap2/microteaching/update', 'Tahap2Controller@updateNilaiMicroteaching')->name('peserta.tahap2.microteaching.update');
});

//tambah jenis skill
Route::post('skill/tambah_skill', 'KelolaPesertaController@tambah_skill')->name('jenis_skill.add');

//Route daftar dan kelola-data Peserta
Route::prefix('peserta')->middleware('login')->group(function () {
    Route::get('/daftar', 'PendaftaranCaslabController@index')->name('peserta.daftar');
    Route::post('/daftar', 'PendaftaranCaslabController@register')->name('peserta.daftar.submit');
    Route::get('/daftar/contoh-berkas', 'PendaftaranCaslabController@contohBerkas')->name('peserta.daftar.contoh-berkas');
    //Route::get('/kelola-data', 'KelolaDataController@index')->name('peserta.kelola-data');
    //download hasil lampiran
    Route::post('/kelola-data/lampiran', 'KelolaDataController@getLampiran')->name('peserta.kelola-data.lampiran');
    //update data diri
    Route::post('/kelola-data/submit/informasi', 'KelolaDataController@updateInformasi')->name('peserta.kelola-data.submit.informasi');
    //batalkan keikutsertaan
    Route::post('/kelola-data/batal-ikut', 'KelolaDataController@deleteRecord')->name('peserta.kelola-data.batal-ikut');
});
