<?php

Route::prefix('panitia')->group(function () {
    Route::get('session', '@index')->name('session.index');
    Route::post('session/create', '@store')->name('session.store');
    Route::put('session/update', '@update')->name('session.update');
    Route::delete('session/delete', '@delete')->name('session.delete');

    Route::get('peserta', 'PanitiaKelolaPesertaController@index')->name('panitiakelolapeserta.index');
    Route::post('peserta/create', 'PanitiaKelolaPesertaController@store')->name('panitiakelolapeserta.store');
    Route::put('peserta/update', 'PanitiaKelolaPesertaController@update')->name('panitiakelolapeserta.update');
    Route::delete('peserta/delete', 'PanitiaKelolaPesertaController@delete')->name('panitiakelolapeserta.delete');

    // download csv
    Route::get('peserta/download_csv', 'PanitiaKelolaPesertaController@download_csv')->name('panitiakelolapeserta.download_csv');

    // route js
    Route::get('session/detail/{id}', '@detail');
    Route::get('peserta/detail/{id}', 'PanitiaKelolaPesertaController@detail');
});


//  Newest Routing

Route::prefix('adm1n')->group(function () {
    //timeline
    Route::get('/tahap3/detail/{id}', 'Tahap3Controller@index');
    Route::post('/tahap3/detail/update', 'Tahap3Controller@updateKelulusan')->name('tahap3.update-kelulusan');

    //kriteria tahap 3
    Route::post('/tahap3/detail/add/wawancara2', 'Tahap3Controller@addKriteriaWawancara2')->name('tahap3.add.kriteria.wawancara2');
    Route::post('/tahap3/detail/add/magang', 'Tahap3Controller@addKriteriaMagang')->name('tahap3.add.kriteria.magang');
    Route::post('/tahap3/detail/add/project', 'Tahap3Controller@addKriteriaProject')->name('tahap3.add.kriteria.project');

    Route::delete('/tahap3/detail/delete/wawancara2', 'Tahap3Controller@deleteKriteriaWawancara2')->name('tahap3.delete.kriteria.wawancara2');
    Route::delete('/tahap3/detail/delete/magang', 'Tahap3Controller@deleteKriteriaMagang')->name('tahap3.delete.kriteria.magang');
    Route::delete('/tahap3/detail/delete/project', 'Tahap3Controller@deleteKriteriaProject')->name('tahap3.delete.kriteria.project');

    Route::post('/tahap3/detail/set/totalTahap3', 'Tahap3Controller@setTotalTahap3')->name('tahap3.set.total.tahap3');

    //set nilai peserta tahap 3
    Route::get('/sesi/{oprec_id}/tahap3/wawancara2/{peserta_id}', 'Tahap3Controller@detailWawancara2');
    Route::get('/sesi/{oprec_id}/tahap3/magang/{peserta_id}', 'Tahap3Controller@detailMagang');
    Route::get('/sesi/{oprec_id}/tahap3/project/{peserta_id}', 'Tahap3Controller@detailProject');

    Route::post('/sesi/tahap3/wawancara2/update', 'Tahap3Controller@updateNilaiWawancara2')->name('peserta.tahap3.wawancara2.update');
    Route::post('/sesi/tahap3/magang/update', 'Tahap3Controller@updateNilaiMagang')->name('peserta.tahap3.magang.update');
    Route::post('/sesi/tahap3/project/update', 'Tahap3Controller@updateNilaiProject')->name('peserta.tahap3.project.update');
});
