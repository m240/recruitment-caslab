<?php

use Illuminate\Database\Seeder;
use App\Skill;

class TableSeeder extends Seeder
{
    //seed tabel admin dan table role
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create([
            'hak_akses' => "Super Admin"
        ]);
        \App\Role::create([
            'hak_akses' => "Ketua"
        ]);
        \App\Role::create([
            'hak_akses' => "Sekretaris"
        ]);
        \App\Role::create([
            'hak_akses' => "Bendahara"
        ]);
        \App\Role::create([
            'hak_akses' => "Acara"
        ]);
        \App\Role::create([
            'hak_akses' => "Konsumsi"
        ]);
        \App\Role::create([
            'hak_akses' => "Perlengkapan"
        ]);
        \App\Role::create([
            'hak_akses' => "PDD"
        ]);
        \App\Admin::create([
            'name' => "Admin Sirenta",
            'username'  => "admin",
            'password'  =>  bcrypt(sha1(md5("123admin" . "asjkdfh") . "shsadfgkj")),
            'role_id' => '1',
            'email' => "lab.informatika@umm.ac.id",
        ]);
        $skills = array("pemrograman web", "pemrograman mobile", "pemrograman desktop", "desain grafis", "videografi");
        foreach ($skills as $skill) {
            $newskill = new Skill;
            $newskill->JenisSkill = $skill;
            $newskill->save();
        }
    }
}
