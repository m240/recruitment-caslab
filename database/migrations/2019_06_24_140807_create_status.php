<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status_pendaftaran', ['lolos', 'tidak lolos', 'menunggu'])->default('lolos');
            $table->enum('status_tahap1', ['lolos', 'tidak lolos', 'menunggu'])->default('menunggu');
            $table->enum('status_tahap2', ['lolos', 'tidak lolos', 'menunggu'])->default('menunggu');
            $table->enum('status_tahap3', ['lolos', 'tidak lolos', 'menunggu'])->default('menunggu');
            //foreign key
            $table->unsignedInteger('oprec_id');
            $table->unsignedInteger('peserta_id');
            $table->timestamps();
            $table->foreign('oprec_id')->references('id')->on('oprec')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('peserta_id')->references('id')->on('peserta')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status');
    }
}
