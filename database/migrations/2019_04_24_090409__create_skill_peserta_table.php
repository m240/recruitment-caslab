<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillPesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_peserta', function (Blueprint $table) {
            //
            $table->unsignedInteger('peserta_id');
            $table->unsignedInteger('skill_id');
            $table->foreign('peserta_id')->references('id')->on('peserta')
                ->onDelete('cascade');
            $table->foreign('skill_id')->references('id')->on('skill')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_peserta');
    }
}
