<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNilaiWawancara1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_wawancara_1', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('wawancara1_id');
            $table->unsignedInteger('nilai');
            $table->unsignedInteger('peserta_id');
            $table->foreign('peserta_id')->references('id')->on('peserta')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('wawancara1_id')->references('id')->on('kriteria_wawancara_1')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_wawancara_1');
    }
}
