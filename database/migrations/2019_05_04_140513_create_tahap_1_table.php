<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTahap1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahap_1', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('oprec_id');
            $table->integer('score_total')->nullable();
            $table->unsignedInteger('peserta_id');
            $table->timestamps();
            $table->foreign('peserta_id')->references('id')->on('peserta')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('oprec_id')->references('id')->on('oprec')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahap1s');
    }
}
