<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNilaiMagang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_magang', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('magang_id');
            $table->unsignedInteger('nilai');
            $table->unsignedInteger('peserta_id');
            $table->foreign('peserta_id')->references('id')->on('peserta')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('magang_id')->references('id')->on('kriteria_magang')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_magang');
    }
}
