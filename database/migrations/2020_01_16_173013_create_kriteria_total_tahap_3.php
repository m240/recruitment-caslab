<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKriteriaTotalTahap3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kriteria_total_tahap_3', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('prosentase_wawancara_2')->default(0);
            $table->unsignedInteger('prosentase_magang')->default(0);
            $table->unsignedInteger('prosentase_project')->default(0);
            $table->unsignedInteger('oprec_id');
            $table->foreign('oprec_id')->references('id')->on('oprec')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kriteria_total_tahap_3');
    }
}
