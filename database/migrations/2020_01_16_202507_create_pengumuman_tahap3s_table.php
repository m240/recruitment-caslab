<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengumumanTahap3sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengumumantahap3', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('oprec_id');
            $table->text('namapengumumantahap3');
            $table->foreign('oprec_id')->references('id')->on('oprec')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengumuman_tahap3');
    }
}
