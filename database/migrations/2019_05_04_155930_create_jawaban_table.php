<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jawaban', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jawab');
            $table->unsignedInteger('pertanyaan_id');
            $table->unsignedInteger('pilihan_id');
            $table->unsignedInteger('tahap_1_id');
            $table->timestamps();

            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('pilihan_id')->references('id')->on('pilihan')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tahap_1_id')->references('id')->on('tahap_1')->onUpdate('cascade')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jawabans');
    }
}
