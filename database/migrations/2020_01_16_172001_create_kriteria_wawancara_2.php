<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKriteriaWawancara2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kriteria_wawancara_2', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_kriteria');
            $table->unsignedInteger('prosentase');
            $table->unsignedInteger('oprec_id')->nullable();
            $table->foreign('oprec_id')->references('id')->on('oprec')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kriteria_wawancara_2');
    }
}
