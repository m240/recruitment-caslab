<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOprecTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oprec', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_oprec');
            $table->enum('status_dibuka_oprec', ['dibuka', 'ditutup']);
            $table->timestamps();
            $table->enum('status_pendaftaran', ['dibuka', 'ditutup'])->default('ditutup');
            $table->enum('status_tahap1', ['dibuka', 'ditutup'])->default('ditutup');
            $table->enum('status_tahap2', ['dibuka', 'ditutup'])->default('ditutup');
            $table->enum('status_tahap3', ['dibuka', 'ditutup'])->default('ditutup');
            $table->unsignedInteger('admin_id');
            $table->foreign('admin_id')->references('id')->on('admin')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oprec');
    }
}
