<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTahap2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahap_2', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tema')->nullable();
            $table->timestamps();
            $table->text('catatan_wawancara_1')->nullable();
            $table->text('catatan_microteaching')->nullable();
            $table->unsignedInteger('oprec_id')->nullable();
            $table->unsignedInteger('peserta_id')->nullable();
            $table->unsignedInteger('score_total_microteaching')->nullable();
            $table->unsignedInteger('score_total_wawancara_1')->nullable();
            $table->unsignedInteger('score_total_tahap_2')->nullable();
            $table->foreign('oprec_id')->references('id')->on('oprec')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('peserta_id')->references('id')->on('peserta')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahap_2');
    }
}
