<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNilaiMicroteaching extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_microteaching', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('microteaching_id');
            $table->unsignedInteger('nilai');
            $table->unsignedInteger('peserta_id');
            $table->foreign('peserta_id')->references('id')->on('peserta')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('microteaching_id')->references('id')->on('kriteria_microteaching')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_microteaching');
    }
}
